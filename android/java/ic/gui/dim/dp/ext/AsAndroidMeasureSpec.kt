package ic.gui.dim.dp.ext


import android.view.View.MeasureSpec

import ic.base.primitives.int32.Int32
import ic.base.throwables.NotSupportedException
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp


val Dp.asAndroidMeasureSpec : Int32 get() = when (this) {

	ByContent -> MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)

	ByContainer -> throw NotSupportedException.Runtime("Container size not specified")

	else -> MeasureSpec.makeMeasureSpec(this.inPx, MeasureSpec.EXACTLY)

}