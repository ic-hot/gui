package ic.gui.dim.px.ext


import android.view.View.MeasureSpec

import ic.base.primitives.int32.Int32
import ic.base.throwables.NotSupportedException
import ic.gui.dim.px.ByContainer
import ic.gui.dim.px.ByContent
import ic.gui.dim.px.Px


val Px.asAndroidMeasureSpec : Int32 get() = when (this) {

	ByContent -> MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)

	ByContainer -> throw NotSupportedException.Runtime("Container size not specified")

	else -> MeasureSpec.makeMeasureSpec(this, MeasureSpec.EXACTLY)

}