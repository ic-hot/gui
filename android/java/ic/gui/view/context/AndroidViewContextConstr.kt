package ic.gui.view.context


import android.content.Context

import ic.gui.dim.dp.Dp


internal inline fun AndroidViewContext (

	androidContext : Context,

	crossinline getRootMaxWidth  : () -> Dp,
	crossinline getRootMaxHeight : () -> Dp,

	crossinline afterUpdateView : () -> Unit = {}

) : AndroidViewContext {

	return object : AndroidViewContext (

		androidContext = androidContext

	) {

		override val rootMaxWidth  get() = getRootMaxWidth()
		override val rootMaxHeight get() = getRootMaxHeight()

		override fun afterUpdate() {
			afterUpdateView()
		}

	}

}
