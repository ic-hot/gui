package ic.gui.font


import ic.base.primitives.int32.Int32

import android.graphics.Typeface


class AndroidFont (

	val androidTypeface : Typeface?,

	val modifier : Int32 = Typeface.NORMAL

) : Font