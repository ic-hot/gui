package ic.gui.adapters


import android.content.Context

import ic.android.ui.view.scope.AndroidViewScope

import ic.gui.scope.ext.views.AndroidView


class AndroidAndroidViewAdapter (

	override val androidContext: Context

) : AndroidViewAdapter<AndroidView, android.view.View>() {


	private val androidViewScope = AndroidViewScope(
		context = androidContext
	)


	override fun initAndroidView (view: AndroidView) : android.view.View {
		return view.callInitAndroidView(androidViewScope)
	}


	override fun AndroidView.updateAndroidViewSelf (androidView: android.view.View) {}


}