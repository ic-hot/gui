package ic.gui.adapters.text


import android.app.Activity
import android.content.Context
import android.text.TextUtils

import ic.android.ui.activity.ext.keyboard.hideKeyboard
import ic.android.ui.view.ext.requestFocusAndKeyboard
import ic.android.ui.view.text.ext.toStrikeThrough
import ic.android.ui.view.text.ext.toUnderscore
import ic.base.primitives.int32.ext.asFloat32

import ic.gui.adapters.AndroidViewAdapter
import ic.gui.adapters.text.impl.AndroidTextViewImplEditable
import ic.gui.adapters.text.impl.AndroidTextViewImplNonEditable
import ic.gui.adapters.text.impl.updateInputIfNeeded
import ic.gui.adapters.text.impl.updateTextHorizontalAlignIfNeeded
import ic.gui.adapters.text.impl.updateTextSizeIfNeeded
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.inPx
import ic.gui.dim.px.ext.inDp
import ic.gui.font.AndroidFont
import ic.gui.view.simple.text.TextView
import ic.gui.view.simple.text.impl.adapter.TextViewAdapter


internal class AndroidTextViewAdapter
	(
		override val androidContext: Context
	)
	: AndroidViewAdapter<TextView, android.widget.TextView>()
	, TextViewAdapter<android.widget.TextView>
{


	override fun initAndroidView (view: TextView) = (
		if (view.input == null) {
			AndroidTextViewImplNonEditable(view, androidContext)
		} else {
			AndroidTextViewImplEditable(view, androidContext)
		}
	)

	override fun TextView.updateAndroidViewSelf (androidView: android.widget.TextView) {
		val font = this.font as AndroidFont
		androidView.setTypeface(
			font.androidTypeface,
			font.modifier
		)
		androidView.setTextColor(color.argb)
		androidView.setLineSpacing(lineSpacingExtra.inPx.asFloat32, 1F)
		androidView.toUnderscore = toUnderscore
		androidView.toStrikeThrough = toStrikeThrough
		when (androidView) {
			is AndroidTextViewImplNonEditable -> {
				androidView.isTextChangedByUserInput = false
				androidView.updateTextSizeIfNeeded()
				androidView.updateTextHorizontalAlignIfNeeded()
				androidView.setMaxLength(maxLength)
				androidView.maxLines = maxLinesCount
				if (this.toEllipsize) {
					androidView.ellipsize = TextUtils.TruncateAt.END
				}
				androidView.setTextAndSpans(text, spans)
				androidView.isTextChangedByUserInput = true
			}
			is AndroidTextViewImplEditable -> {
				androidView.isTextChangedByUserInput = false
				androidView.updateTextSizeIfNeeded()
				androidView.updateTextHorizontalAlignIfNeeded()
				androidView.setMaxLength(maxLength)
				androidView.updateInputIfNeeded()
				androidView.setTextAndSpans(text, spans)
				androidView.isTextChangedByUserInput = true
			}
		}
	}

	override fun focus (impl: android.widget.TextView) {
		if (impl.isEnabled) {
			impl.requestFocusAndKeyboard()
		}
	}

	override fun unfocus (impl: android.widget.TextView) {
		impl.clearFocus()
		(impl.context as? Activity)?.hideKeyboard()
	}


	override fun getIntrinsicMinWidth (view: TextView, impl: android.widget.TextView): Dp {
		if (impl is android.widget.EditText) {
			val layout = impl.layout ?: return super.getIntrinsicMinWidth(view, impl)
			return layout.width.inDp
		} else {
			return super.getIntrinsicMinWidth(view, impl)
		}
	}

	override fun getIntrinsicMinHeight (view: TextView, impl: android.widget.TextView) : Dp {
		if (impl is android.widget.EditText) {
			val layout = impl.layout ?: return super.getIntrinsicMinHeight(view, impl)
			return layout.height.inDp
		} else {
			return super.getIntrinsicMinHeight(view, impl)
		}
	}


}