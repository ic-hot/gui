package ic.gui.adapters.text.impl


import android.util.TypedValue


internal fun AndroidTextViewImplNonEditable.updateTextSizeIfNeeded() {

	val viewTextSize = view.size

	if (viewTextSize == cachedTextSize) return

	cachedTextSize = viewTextSize

	setTextSize(TypedValue.COMPLEX_UNIT_SP, viewTextSize)

}

internal fun AndroidTextViewImplEditable.updateTextSizeIfNeeded() {

	val viewTextSize = view.size

	if (viewTextSize == cachedTextSize) return

	cachedTextSize = viewTextSize

	setTextSize(TypedValue.COMPLEX_UNIT_SP, viewTextSize)

}