package ic.gui.adapters.text.impl


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Rect
import android.os.Build
import android.text.Editable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo

import ic.android.ui.activity.ext.keyboard.hideKeyboard
import ic.android.ui.view.ext.activity
import ic.android.ui.view.text.ext.maxLength
import ic.base.primitives.int32.Int32
import ic.ifaces.cancelable.Cancelable
import ic.math.ext.clamp
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.length.isEmpty
import ic.struct.list.ext.reduce.find.atLeastOne

import ic.graphics.color.ColorArgb
import ic.gui.align.HorizontalAlign
import ic.gui.dim.dp.Dp

import ic.gui.view.simple.text.OnOkayAction
import ic.gui.view.simple.text.TextInput
import ic.gui.view.simple.text.TextView
import ic.gui.view.simple.text.span.Link
import ic.gui.view.simple.text.span.Span


@SuppressLint("ViewConstructor")
internal class AndroidTextViewImplEditable (

	internal val view: TextView,

	androidContext : Context

) : android.widget.EditText(androidContext) {

	private inline val thisImpl get() = this

	internal val defaultKeyListener = keyListener

	internal val defaultTransformationMethod = transformationMethod

	init {
		clipToOutline = false
		setPadding(0, 0, 0, 0)
		setBackgroundColor(ColorArgb(0x00000000))
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
			textCursorDrawable = null
		}
		setOnEditorActionListener { _, actionId, _ ->
			when (actionId) {
				EditorInfo.IME_ACTION_DONE,
				EditorInfo.IME_ACTION_NEXT,
				EditorInfo.IME_ACTION_GO,
				EditorInfo.IME_ACTION_SEARCH,
				EditorInfo.IME_ACTION_SEND -> {
					when (val onOkayAction = cachedInput?.onOkayAction) {
						null                         -> activity.hideKeyboard()
						is OnOkayAction.HideKeyboard -> activity.hideKeyboard()
						else                         -> onOkayAction.onOkay()
					}
					true
				}
				else -> false
			}
		}
		addTextChangedListener(
			object : TextWatcher {
				override fun beforeTextChanged (p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
				override fun onTextChanged (p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
				override fun afterTextChanged (editable: Editable?) {
					val text = editable.toString()
					val input = view.input
					if (input == null) {
						if (isTextChangedByUserInput) {
							implSetTextAndSpans(thisImpl.text, spans)
						} else {
							thisImpl.text = text
						}
					} else {
						thisImpl.text = text
						if (isTextChangedByUserInput) {
							input.onTextChange(text)
						}
					}
				}
			}
		)
	}

	private var maxLength : Int32 = Int32.MAX_VALUE
	fun setMaxLength (maxLength: Int32) {
		if (maxLength == this.maxLength) return
		(this as android.widget.EditText).maxLength = maxLength
	}

	var isTextChangedByUserInput : Boolean = true

	private var text : String = ""
	private var spans : List<Span> = List()
	private fun implSetTextAndSpans (text: String, spans: List<Span>) {
		if (spans.isEmpty) {
			setText(text)
		} else {
			val spannableString = SpannableString.valueOf(text)
			spans.forEach { span ->
				val startCharIndex = span.startCharIndex
				val endCharIndex   = span.endCharIndex
				val color = span.color
				if (color != null) {
					spannableString.setSpan(
						ForegroundColorSpan(color.argb),
						startCharIndex, endCharIndex,
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
					)
				}
				if (span is Link) {
					spannableString.setSpan(
						object : ClickableSpan() {
							override fun onClick (widget: View) {
								span.onClick()
							}
							override fun updateDrawState (drawState: TextPaint) {
								drawState.isUnderlineText = false
							}
						},
						startCharIndex, endCharIndex,
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
					)
				}
			}
			setText(spannableString)
			if (spans.atLeastOne { it is Link }) {
				movementMethod = LinkMovementMethod.getInstance()
			} else {
				movementMethod = null
			}
		}
	}

	fun setTextAndSpans (text: String, spans: List<Span>) {
		if (text == this.text && spans == this.spans) return
		val oldText = this.text
		this.text = text
		this.spans = spans
		val oldCursorPosition = selectionEnd
		val newCursorPosition = (
			if (text.length > oldText.length) {
				oldCursorPosition + (text.length - oldText.length)
			} else {
				oldCursorPosition
			}
		).clamp(0, text.length + 1)
		implSetTextAndSpans(text, spans)
		setSelection(newCursorPosition, newCursorPosition)
	}

	internal var cachedTextSize : Dp = Dp.NaN
	internal var cachedInput : TextInput? = null
	internal var cachedInputType : TextInput.Type? = null
	internal var cachedMaxLinesCount : Int32 = -1
	internal var cachedIsSelectable : Boolean? = null
	internal var cachedTextHorizontalAlign : HorizontalAlign = Float.NaN

	override fun dispatchTouchEvent (event: MotionEvent?) : Boolean {
		if (
			view.input != null || view.isSelectable || view.spans.atLeastOne { it is Link }
		) {
			return super.dispatchTouchEvent(event)
		} else {
			return false
		}
	}

	internal var requestFocusDelayTask : Cancelable? = null

	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		requestFocusIfNeeded()
	}

	override fun onDetachedFromWindow() {
		requestFocusDelayTask?.cancel()
		requestFocusDelayTask = null
		super.onDetachedFromWindow()
	}

	override fun onFocusChanged (
		focused: Boolean, direction: Int, previouslyFocusedRect: Rect?
	) {
		super.onFocusChanged(focused, direction, previouslyFocusedRect)
		if (focused) {
			view.notifyFocus()
		} else {
			view.notifyUnfocus()
		}
	}


}