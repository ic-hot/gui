package ic.gui.adapters.text.impl


import android.text.method.PasswordTransformationMethod
import android.view.inputmethod.EditorInfo

import ic.gui.view.simple.text.OnOkayAction
import ic.gui.view.simple.text.TextInput


internal fun AndroidTextViewImplEditable.updateInputIfNeeded() {

	val input = view.input
	val maxLinesCount = view.maxLinesCount
	val isSelectable = view.isSelectable

	val inputType = input?.type

	if (
		input         == cachedInput &&
		inputType     == cachedInputType &&
		maxLinesCount == cachedMaxLinesCount &&
		isSelectable  == cachedIsSelectable
	) return

	val oldToFocusByDefault : Boolean = this.cachedInput?.toFocusByDefault ?: false

	this.cachedInput = input
	this.cachedInputType = inputType
	this.cachedMaxLinesCount = maxLinesCount
	this.cachedIsSelectable = isSelectable

	if (inputType != null || isSelectable) {
		isFocusable = true
		isClickable = false
	} else {
		isFocusable = false
		isClickable = true
	}

	if (inputType == null) {
		keyListener = null
		transformationMethod = null
		this.inputType = EditorInfo.TYPE_NULL
	} else {
		keyListener = defaultKeyListener
		var androidInputType = when (inputType) {
			is TextInput.Type.CommonText -> EditorInfo.TYPE_CLASS_TEXT
			is TextInput.Type.Password -> EditorInfo.TYPE_CLASS_TEXT or EditorInfo.TYPE_TEXT_VARIATION_PASSWORD
			is TextInput.Type.Integer -> EditorInfo.TYPE_CLASS_NUMBER
			is TextInput.Type.Decimal -> EditorInfo.TYPE_CLASS_NUMBER or EditorInfo.TYPE_NUMBER_FLAG_DECIMAL
			is TextInput.Type.PhoneNumber -> EditorInfo.TYPE_CLASS_PHONE
		}
		if (maxLinesCount != 1) {
			androidInputType = androidInputType or EditorInfo.TYPE_TEXT_FLAG_MULTI_LINE
		}
		if (input.isAllCaps) {
			androidInputType = androidInputType or EditorInfo.TYPE_TEXT_FLAG_CAP_CHARACTERS
		}
		if (!view.toSpellCheck) {
			androidInputType = androidInputType or EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS
		}
		this.inputType = androidInputType
		imeOptions = when (input.onOkayAction) {
			is OnOkayAction.HideKeyboard -> (
				if (maxLinesCount == 1) {
					EditorInfo.IME_ACTION_DONE
				} else {
					EditorInfo.IME_ACTION_NONE
				}
			)
			is OnOkayAction.Done -> EditorInfo.IME_ACTION_DONE
			is OnOkayAction.Send -> EditorInfo.IME_ACTION_SEND
		}
		if (inputType == TextInput.Type.Password) {
			transformationMethod = PasswordTransformationMethod()
		} else {
			transformationMethod = defaultTransformationMethod
		}
	}
	isSingleLine = (
		if (input == null) {
			maxLinesCount == 1
		} else {
			if (input.onOkayAction == OnOkayAction.HideKeyboard) {
				maxLinesCount == 1
			} else {
				true
			}
		}
	)
	maxLines = maxLinesCount
	if (!oldToFocusByDefault && isAttachedToWindow) {
		requestFocusIfNeeded()
	}

}