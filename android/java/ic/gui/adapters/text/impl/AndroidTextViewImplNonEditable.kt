package ic.gui.adapters.text.impl


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Rect
import android.os.Build
import android.text.Editable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.MotionEvent
import android.view.View

import ic.android.ui.view.text.ext.maxLength
import ic.base.primitives.int32.Int32
import ic.math.ext.clamp
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.length.isEmpty
import ic.struct.list.ext.reduce.find.atLeastOne

import ic.gui.align.HorizontalAlign
import ic.gui.dim.dp.Dp

import ic.gui.view.simple.text.TextInput
import ic.gui.view.simple.text.TextView
import ic.gui.view.simple.text.span.Link
import ic.gui.view.simple.text.span.Span


@SuppressLint("ViewConstructor")
internal class AndroidTextViewImplNonEditable (

	internal val view: TextView,

	androidContext : Context

) : android.widget.TextView(androidContext) {

	private inline val thisImpl get() = this

	internal val defaultKeyListener = keyListener

	internal val defaultTransformationMethod = transformationMethod

	init {
		clipToOutline = false
	}

	private var maxLength : Int32 = Int32.MAX_VALUE
	fun setMaxLength (maxLength: Int32) {
		if (maxLength == this.maxLength) return
		(this as android.widget.EditText).maxLength = maxLength
	}

	var isTextChangedByUserInput : Boolean = true

	private var text : String = ""
	private var spans : List<Span> = List()
	private fun implSetTextAndSpans (text: String, spans: List<Span>) {
		if (spans.isEmpty) {
			setText(text)
		} else {
			val spannableString = SpannableString.valueOf(text)
			spans.forEach { span ->
				val startCharIndex = span.startCharIndex
				val endCharIndex   = span.endCharIndex
				val color = span.color
				if (color != null) {
					spannableString.setSpan(
						ForegroundColorSpan(color.argb),
						startCharIndex, endCharIndex,
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
					)
				}
				if (span is Link) {
					spannableString.setSpan(
						object : ClickableSpan() {
							override fun onClick (widget: View) {
								span.onClick()
							}
							override fun updateDrawState (drawState: TextPaint) {
								drawState.isUnderlineText = false
							}
						},
						startCharIndex, endCharIndex,
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
					)
				}
			}
			setText(spannableString)
			if (spans.atLeastOne { it is Link }) {
				movementMethod = LinkMovementMethod.getInstance()
			} else {
				movementMethod = null
			}
		}
	}

	fun setTextAndSpans (text: String, spans: List<Span>) {
		if (text == this.text && spans == this.spans) return
		val oldText = this.text
		this.text = text
		this.spans = spans
		val oldCursorPosition = selectionEnd
		val newCursorPosition = (
			if (text.length > oldText.length) {
				oldCursorPosition + (text.length - oldText.length)
			} else {
				oldCursorPosition
			}
		).clamp(0, text.length + 1)
		implSetTextAndSpans(text, spans)
	}

	internal var cachedTextSize : Dp = Dp.NaN
	internal var cachedInput : TextInput? = null
	internal var cachedInputType : TextInput.Type? = null
	internal var cachedMaxLinesCount : Int32 = -1
	internal var cachedIsSelectable : Boolean? = null
	internal var cachedTextHorizontalAlign : HorizontalAlign = Float.NaN

	override fun dispatchTouchEvent (event: MotionEvent?) : Boolean {
		if (
			view.input != null || view.isSelectable || view.spans.atLeastOne { it is Link }
		) {
			return super.dispatchTouchEvent(event)
		} else {
			return false
		}
	}


}