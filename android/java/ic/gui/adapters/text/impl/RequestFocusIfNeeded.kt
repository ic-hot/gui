package ic.gui.adapters.text.impl


import ic.android.ui.view.ext.requestFocusAndKeyboard
import ic.android.util.handler.postDelayed


internal fun AndroidTextViewImplEditable.requestFocusIfNeeded() {

	if (requestFocusDelayTask != null) return

	if (cachedInput?.toFocusByDefault == true) {

		requestFocusDelayTask = postDelayed(384) {
			requestFocusDelayTask = null
			requestFocusAndKeyboard()
		}

	}

}