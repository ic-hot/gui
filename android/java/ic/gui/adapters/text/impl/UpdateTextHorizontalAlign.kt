package ic.gui.adapters.text.impl


import android.view.Gravity

import ic.gui.align.Left
import ic.gui.align.Right


internal fun AndroidTextViewImplEditable.updateTextHorizontalAlignIfNeeded() {

	val viewTextHorizontalAlign = view.textHorizontalAlign

	if (viewTextHorizontalAlign == cachedTextHorizontalAlign) return

	cachedTextHorizontalAlign = viewTextHorizontalAlign

	gravity = when (viewTextHorizontalAlign) {
		Left  -> Gravity.LEFT
		Right -> Gravity.RIGHT
		else  -> Gravity.CENTER_HORIZONTAL
	}

}


internal fun AndroidTextViewImplNonEditable.updateTextHorizontalAlignIfNeeded() {

	val viewTextHorizontalAlign = view.textHorizontalAlign

	if (viewTextHorizontalAlign == cachedTextHorizontalAlign) return

	cachedTextHorizontalAlign = viewTextHorizontalAlign

	gravity = when (viewTextHorizontalAlign) {
		Left  -> Gravity.LEFT
		Right -> Gravity.RIGHT
		else  -> Gravity.CENTER_HORIZONTAL
	}

}