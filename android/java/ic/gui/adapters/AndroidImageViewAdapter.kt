package ic.gui.adapters


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.DrawFilter
import android.graphics.Paint
import android.graphics.PaintFlagsDrawFilter

import ic.android.ui.view.image.ext.clearImage
import ic.android.ui.view.image.ext.setTintColor

import ic.graphics.color.Color
import ic.graphics.image.Image
import ic.graphics.image.ext.asAndroidBitmap

import ic.gui.view.simple.image.ImageView
import ic.gui.view.simple.image.ScaleMode


internal class AndroidImageViewAdapter
	(
		override val androidContext: Context
	)
	: AndroidViewAdapter<ImageView, AndroidImageViewAdapter.Impl>()
{


	@SuppressLint("ViewConstructor")
	inner class Impl : android.widget.ImageView(androidContext) {

		var image : Image? = null
			set(value) {
				if (value == field) return
				field = value
				val bitmap = image?.asAndroidBitmap
				if (bitmap == null) {
					clearImage()
				} else {
					setImageBitmap(bitmap)
				}
			}
		;

		private var drawFilter : DrawFilter? = null

		override fun onDraw (canvas: Canvas) {
			val oldDrawFilter = canvas.drawFilter
			canvas.drawFilter = drawFilter
			super.onDraw(canvas)
			canvas.drawFilter = oldDrawFilter
		}

		var toUseAntialiasing : Boolean = true
			set(value) {
				field = value
				drawFilter = if (value) {
					null
				} else {
					PaintFlagsDrawFilter(Paint.FILTER_BITMAP_FLAG, 0)
				}
			}
		;

		var scaleMode : ScaleMode = ScaleMode.Fit
			set(value) {
				field = value
				scaleType = when (value) {
					ScaleMode.Crop    -> ScaleType.CENTER_CROP
					ScaleMode.Fit     -> ScaleType.FIT_CENTER
					ScaleMode.Stretch -> ScaleType.FIT_XY
				}
			}
		;

		var tintColor : Color? = null
			set(value) {
				if (value == field) return
				field = value
				setTintColor(value?.argb)
			}
		;

	}


	override fun initAndroidView (view: ImageView) = Impl()


	override fun ImageView.updateAndroidViewSelf (androidView: Impl) {
		androidView.image = image
		androidView.toUseAntialiasing = toUseAntialiasing
		androidView.scaleMode = scaleMode
		androidView.tintColor = tint
	}


}