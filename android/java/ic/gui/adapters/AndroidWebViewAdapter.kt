package ic.gui.adapters


import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import android.widget.FrameLayout

import ic.android.ui.view.web.ext.loadPage
import ic.base.escape.skip.skippable
import ic.base.primitives.int32.Int32
import ic.base.strings.ext.parse.parseFloat32
import ic.base.strings.ext.parse.parseFloat32OrNull

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.simple.web.WebView


internal class AndroidWebViewAdapter (

	override val androidContext: Context

) : AndroidViewAdapter<WebView, AndroidWebViewAdapter.Impl>() {


	inner class Impl (private val view: WebView) : FrameLayout(androidContext) {

		var loadedUrl      : String? = null
		var loadedPageHtml : String? = null

		var measuredHeightDp : Dp = 0.dp

		val webView = object : android.webkit.WebView(androidContext) {

			init {

				overScrollMode = OVER_SCROLL_NEVER

				isVerticalScrollBarEnabled = false
				isHorizontalScrollBarEnabled = false

				isScrollContainer = false

				webViewClient = object : WebViewClient() {

					@Suppress("OVERRIDE_DEPRECATION")
					override fun shouldOverrideUrlLoading(
						androidWebView : android.webkit.WebView,
						url : String
					) : Boolean {
						skippable(
							onSkip = {
								if (view.toOpenLinksInBrowser) {
									context.startActivity(
										Intent(
											Intent.ACTION_VIEW,
											Uri.parse(url)
										)
									)
									return true
								} else {
									@Suppress("DEPRECATION")
									return super.shouldOverrideUrlLoading(androidWebView, url)
								}
							}
						) {
							view.callOnRedirectOrSkip(url)
							return true
						}
					}

					override fun onPageFinished (
						androidWebView : android.webkit.WebView,
						urlString : String?
					) {
						super.onPageFinished(androidWebView, urlString)
						view.javascriptToRun?.let { evaluateJavascript(it) {} }
						fun updateHeight (attemptsLeft: Int32) {
							evaluateJavascript("document.body.scrollHeight") { heightString ->
								val height = heightString.parseFloat32OrNull()
								if (height != null) {
									measuredHeightDp = height
									post {
										view.updateAllViews()
										if (attemptsLeft > 1) {
											postDelayed({ updateHeight(attemptsLeft - 1) }, 1024)
										}
									}
								}
							}
						}
						updateHeight(attemptsLeft = 4)
					}

				}

				webChromeClient = WebChromeClient()

			}

		}.also { addView(it, MATCH_PARENT, MATCH_PARENT) }

	}


	override fun initAndroidView (view: WebView) = Impl(view)


	override fun WebView.updateAndroidViewSelf (androidView: Impl) {

		val settings = androidView.webView.settings
		settings.javaScriptEnabled = toEnableJavaScript
		settings.useWideViewPort = !toUseMobileSiteVersion

		val urlToLoad      = this.pageUrl
		val pageHtmlToLoad = this.pageHtml

		if (
			urlToLoad      != androidView.loadedUrl ||
			pageHtmlToLoad != androidView.loadedPageHtml
		) {
			androidView.loadedUrl = urlToLoad
			androidView.loadedPageHtml = pageHtmlToLoad
			if (pageHtmlToLoad == null) {
				androidView.webView.loadUrl(urlToLoad ?: return)
			} else {
				androidView.webView.loadPage(
					pageHtml = pageHtmlToLoad,
					toPreventScroll = true,
					innerStyleCss = innerStyleCss
				)
			}
		}

	}


	override fun getIntrinsicMinWidth (view: WebView, impl: Impl): Dp {
		return 0.dp
	}

	override fun getIntrinsicMinHeight (view: WebView, impl: Impl) : Dp {
		return impl.measuredHeightDp
	}


}