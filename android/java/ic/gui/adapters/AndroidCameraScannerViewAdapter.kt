package ic.gui.adapters


import android.content.Context

import me.dm7.barcodescanner.zxing.ZXingScannerView

import ic.gui.view.simple.camera.CameraScannerView


internal class AndroidCameraScannerViewAdapter
	(
		override val androidContext: Context
	)
	: AndroidViewAdapter<CameraScannerView, ZXingScannerView>()
{

	override fun initAndroidView (view: CameraScannerView) : ZXingScannerView {
		return object : ZXingScannerView(androidContext) {
			init {
				setResultHandler { result ->
					view.callOnScanned(result.text)
				}
			}
			override fun onAttachedToWindow() {
				super.onAttachedToWindow()
				startCamera()
			}
			override fun onDetachedFromWindow() {
				super.onDetachedFromWindow()
				stopCamera()
			}
		}
	}

	override fun CameraScannerView.updateAndroidViewSelf (androidView: ZXingScannerView) {}

}