package ic.gui.adapters.impl


import android.view.MotionEvent

import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.int32.ext.asInt64

import ic.gui.view.context.ViewContext
import ic.gui.dim.px.ext.inDp
import ic.gui.touch.TouchDirective.*
import ic.gui.touch.TouchEvent
import ic.gui.view.View


internal inline fun implDispatchTouchEvent (

	view : View,

	motionEvent : MotionEvent,

	superDispatchTouchEvent : (MotionEvent) -> Boolean

) : Boolean {

	if (view.isOpen && view.container is ViewContext) {

		val touchHandler = view.touchHandler

		val touchDirective = when (motionEvent.action) {
			MotionEvent.ACTION_DOWN -> {
				touchHandler.handle(
					TouchEvent.Down(
						touchId = motionEvent.getPointerId(0).asInt64,
						globalX = motionEvent.x.asInt32.inDp,
						globalY = motionEvent.y.asInt32.inDp
					)
				)
			}
			MotionEvent.ACTION_MOVE -> {
				touchHandler.handle(
					TouchEvent.Move(
						touchId = motionEvent.getPointerId(0).asInt64,
						globalX = motionEvent.x.asInt32.inDp,
						globalY = motionEvent.y.asInt32.inDp
					)
				)
			}
			MotionEvent.ACTION_UP -> {
				touchHandler.handle(
					TouchEvent.Up(
						touchId = motionEvent.getPointerId(0).asInt64
					)
				)
			}
			MotionEvent.ACTION_CANCEL -> {
				touchHandler.handle(
					TouchEvent.Cancel(
						touchId = motionEvent.getPointerId(0).asInt64
					)
				)
				Forget
			}
			else -> {
				touchHandler.handle(
					TouchEvent.Cancel(
						touchId = motionEvent.getPointerId(0).asInt64
					)
				)
				Forget
			}
		}

		/*
		if (
			motionEvent.action == MotionEvent.ACTION_DOWN &&
			touchDirective == Capture
		) {
			return true
		}
		 */

		val guiResult = when (touchDirective) {
			Forget    -> false
			Watch     -> true
			Capture   -> true
			is Screen -> true
		}

		val androidResult = superDispatchTouchEvent(motionEvent)

		return guiResult or androidResult

	} else {

		return superDispatchTouchEvent(motionEvent)

	}

}
