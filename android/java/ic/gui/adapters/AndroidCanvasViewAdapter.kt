package ic.gui.adapters


import android.content.Context

import ic.graphics.canvas.LateInitAndroidCanvas

import ic.gui.view.simple.canvas.CanvasView


internal class AndroidCanvasViewAdapter (

	override val androidContext: Context

) : AndroidViewAdapter<CanvasView, android.view.View>() {


	override fun initAndroidView (view: CanvasView) : android.view.View {

		return object : android.view.View(androidContext) {

			private val canvas = LateInitAndroidCanvas()

			override fun onDraw (canvas: android.graphics.Canvas) {
				super.onDraw(canvas)
				this.canvas.androidCanvas = canvas
				view.callOnDraw(this.canvas)
				if (view.toRedrawContinuously) postInvalidate()
			}
			
		}

	}


	override fun CanvasView.updateAndroidViewSelf (androidView : android.view.View) {}


}