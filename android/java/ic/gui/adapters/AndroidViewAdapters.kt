package ic.gui.adapters


import android.content.Context

import ic.gui.adapters.text.AndroidTextViewAdapter


internal class AndroidViewAdapters (

	internal val androidContext : Context

) : ViewAdapters<android.view.View> {


	override val cameraScannerViewAdapter = AndroidCameraScannerViewAdapter(androidContext)

	override val canvasViewAdapter = AndroidCanvasViewAdapter(androidContext)

	override val cardViewAdapter = AndroidCardViewAdapter(androidContext)

	override val containerViewAdapter = AndroidContainerViewAdapter(androidContext)

	override val datePickerViewAdapter = AndroidDatePickerViewAdapter(androidContext)

	override val imageViewAdapter = AndroidImageViewAdapter(androidContext)

	override val linearGradientViewAdapter = AndroidLinearGradientViewAdapter(androidContext)

	override val pdfViewAdapter = AndroidPdfViewAdapter(androidContext)

	override val textViewAdapter = AndroidTextViewAdapter(androidContext)

	override val webViewAdapter = AndroidWebViewAdapter(androidContext)


	val androidViewAdapter = AndroidAndroidViewAdapter(androidContext)


}