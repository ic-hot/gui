package ic.gui.adapters


import android.content.Context
import android.view.MotionEvent

import ic.gui.adapters.impl.implDispatchTouchEvent
import ic.gui.view.container.ContainerView


internal class AndroidContainerViewAdapter (

	override val androidContext : Context

) : AndroidAContainerViewAdapter<ContainerView, AndroidContainerViewAdapter.Impl>() {


	inner class Impl (

		private val view: ContainerView

	) : android.widget.FrameLayout(androidContext) {

		init {
			clipToPadding = false
			clipChildren = false
		}

		override fun dispatchTouchEvent (motionEvent: MotionEvent) = implDispatchTouchEvent(
			superDispatchTouchEvent = { super.dispatchTouchEvent(it) },
			view = view,
			motionEvent = motionEvent
		)

	}


	override fun initAndroidView (view: ContainerView) = Impl(view)

	override val Impl.subviewsContainer get() = this


}