package ic.gui.adapters


import android.annotation.SuppressLint
import android.content.Context
import android.view.View.MeasureSpec
import android.widget.EditText
import android.widget.FrameLayout

import ic.base.primitives.int32.ext.asFloat32
import ic.util.log.logD

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.inPx
import ic.gui.dim.px.ext.inDp
import ic.gui.view.View
import ic.gui.view.ext.width


abstract class AndroidViewAdapter
	<V: View, AndroidView: android.view.View>
	: ViewAdapter<V, AndroidView>
{


	protected abstract val androidContext : Context


	override fun V.updateImplLocalPosition (impl: AndroidView, localX: Dp, localY: Dp) {
		impl.translationX = localX.inPx.asFloat32
		impl.translationY = localY.inPx.asFloat32
		impl.pivotX = 0F
		impl.pivotY = 0F
		impl.scaleX = this.scaleX
		impl.scaleY = this.scaleY
	}


	override fun updateImplSize (icView: V, impl: AndroidView, width: Dp, height: Dp) {
		impl.layoutParams = (
			impl.layoutParams?.also { it.width = width.inPx; it.height = height.inPx }
			?: FrameLayout.LayoutParams(width.inPx, height.inPx)
		)
	}


	protected abstract fun initAndroidView (view: V) : AndroidView

	@SuppressLint("ClickableViewAccessibility")
	final override fun V.initImpl() : AndroidView {
		return initAndroidView(this)
	}


	protected abstract fun V.updateAndroidViewSelf (androidView: AndroidView)

	final override fun V.updateImplSelf (impl: AndroidView) {
		impl.alpha = opacity
		impl.rotation = rotationDeg
		impl.elevation = z.asFloat32
		val debugColor = this.debugColor
		if (debugColor != null) {
			impl.setBackgroundColor(debugColor.argb)
		}
		updateAndroidViewSelf(impl)
	}


	override fun getIntrinsicMinWidth (view: V, impl: AndroidView): Dp {
		impl.measure(
			MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
			MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
		)
		return (impl.measuredWidth + 1).inDp.also {
			val debugName = view.debugName
			if (debugName != null) {
				if (impl is EditText) {
					logD("gui") { "$debugName impl.measuredWidth: $it text: ${ impl.text }" }
				} else {
					logD("gui") { "$debugName impl.measuredWidth: $it" }
				}
			}
		} // Without extra pixel TextView tends to wrap
	}

	override fun getIntrinsicMinHeight (view: V, impl: AndroidView) : Dp {
		impl.measure(
			MeasureSpec.makeMeasureSpec(view.width.inPx, MeasureSpec.EXACTLY),
			MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
		)
		return impl.measuredHeight.inDp.also {
			val debugName = view.debugName
			if (debugName != null) {
				if (impl is EditText) {
					logD("gui") { "$debugName impl.measuredHeight: $it text: ${ impl.text }" }
				} else {
					logD("gui") { "$debugName impl.measuredHeight: $it" }
				}
			}
		}
	}


}