package ic.gui.adapters


import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.graphics.drawable.GradientDrawable
import android.view.MotionEvent
import android.view.ViewGroup.LayoutParams.MATCH_PARENT

import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32

import ic.graphics.color.Color

import ic.gui.adapters.impl.implDispatchTouchEvent
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.dim.dp.ext.inPx
import ic.gui.view.card.CardView


internal class AndroidCardViewAdapter (

	override val androidContext : Context

) : AndroidAContainerViewAdapter<CardView, android.view.View>() {


	inner class BackgroundView (
		private val view : CardView
	) : android.widget.FrameLayout(androidContext)

	inner class ContainerView (
		private val view : CardView
	) : android.widget.FrameLayout(androidContext) {

		init {
			clipToPadding = true
			clipChildren = false
			outlineProvider = null
		}

		var insetRadii : FloatArray? = null

		var insetPx : Int32 = 0

		override fun dispatchDraw (canvas: Canvas) {
			val insetRadii = insetRadii ?: return super.dispatchDraw(canvas)
			val clipPath = Path()
			clipPath.addRoundRect(
				RectF(
					canvas.clipBounds.apply {
						inset(insetPx, insetPx)
					}
				),
				insetRadii,
				Path.Direction.CW
			)
			canvas.clipPath(clipPath)
			super.dispatchDraw(canvas)
		}

	}

	inner class ForegroundView (
		private val view : CardView
	) : android.widget.FrameLayout(androidContext)


	inner class Impl (
		private val view : CardView
	) : android.widget.FrameLayout(androidContext) {

		val backgroundView = BackgroundView(view).also { addView(it, MATCH_PARENT, MATCH_PARENT) }

		val containerView = ContainerView(view).also { addView(it, MATCH_PARENT, MATCH_PARENT) }

		var strokeView = ForegroundView(view).also { addView(it, MATCH_PARENT, MATCH_PARENT) }

		init {
			clipToPadding = false
			clipChildren = false
		}

		// Cached parameters:
		var backgroundColor : Color = Color.Transparent
		var shadowOpacity : Float32 = 1F
		var topLeftCornerRadius     : Dp = 0.dp
		var topRightCornerRadius    : Dp = 0.dp
		var bottomLeftCornerRadius  : Dp = 0.dp
		var bottomRightCornerRadius : Dp = 0.dp
		var outlineThickness : Dp = 0.dp
		var outlineColor : Color? = null
		var outlineDashLength : Dp = 0.dp

		override fun dispatchTouchEvent (motionEvent: MotionEvent) = implDispatchTouchEvent(
			superDispatchTouchEvent = { super.dispatchTouchEvent(it) },
			view = view,
			motionEvent = motionEvent
		)

	}


	override fun initAndroidView (view: CardView) : android.view.View {
		if (view.subview == null) {
			return android.view.View(androidContext)
		} else {
			return Impl(view)
		}
	}

	override val android.view.View.subviewsContainer : android.view.ViewGroup? get() {
		if (this is Impl) {
			return containerView
		} else {
			return null
		}
	}

	override fun updateAndroidViewGroup (view: CardView, impl: android.view.View) {

		val backgroundColor = view.backgroundColor

		when (impl) {

			is Impl -> {

				val shadowOpacity = view.shadowOpacity
				val topLeftCornerRadius = view.topLeftCornerRadius
				val topRightCornerRadius = view.topRightCornerRadius
				val bottomLeftCornerRadius = view.bottomLeftCornerRadius
				val bottomRightCornerRadius = view.bottomRightCornerRadius
				val outlineThickness = view.outlineThickness
				val outlineColor = view.outlineColor
				val outlineDashLength = view.outlineDashLength
				val areCardParametersTheSame = (
					topLeftCornerRadius  == impl.topLeftCornerRadius  &&
						topRightCornerRadius == impl.topRightCornerRadius &&
						bottomLeftCornerRadius == impl.bottomLeftCornerRadius &&
						bottomRightCornerRadius == impl.bottomRightCornerRadius &&
						outlineThickness == impl.outlineThickness &&
						outlineColor == impl.outlineColor &&
						outlineDashLength == impl.outlineDashLength &&
						backgroundColor == impl.backgroundColor &&
						shadowOpacity == impl.shadowOpacity
					)
				if (areCardParametersTheSame) return
				impl.backgroundColor = backgroundColor
				impl.shadowOpacity = shadowOpacity
				impl.topLeftCornerRadius = topLeftCornerRadius
				impl.topRightCornerRadius = topRightCornerRadius
				impl.bottomLeftCornerRadius = bottomLeftCornerRadius
				impl.bottomRightCornerRadius = bottomRightCornerRadius
				impl.outlineThickness = outlineThickness
				impl.outlineColor = outlineColor
				impl.outlineDashLength = outlineDashLength

				val outlineThicknessPx = outlineThickness.inPx
				val inset = view.inset
				val insetPx = view.inset.inPx
				val halfInsetPx = (inset / 2).inPx
				val topLeftCornerRadiusPx     = topLeftCornerRadius.inPx
				val topRightCornerRadiusPx    = topRightCornerRadius.inPx
				val bottomLeftCornerRadiusPx  = bottomLeftCornerRadius.inPx
				val bottomRightCornerRadiusPx = bottomRightCornerRadius.inPx
				val outlineDashLengthPx = outlineDashLength.inPx
				val radii : FloatArray?
				val insetRadii : FloatArray?
				val halfPaddedRadii : FloatArray?
				if (
					topLeftCornerRadiusPx == 0 &&
					topRightCornerRadiusPx == 0 &&
					bottomLeftCornerRadiusPx == 0 &&
					bottomRightCornerRadiusPx == 0
				) {
					radii = null
					insetRadii = null
					halfPaddedRadii = null
				} else {
					radii = floatArrayOf(
						topLeftCornerRadiusPx.asFloat32,     topLeftCornerRadiusPx.asFloat32,
						topRightCornerRadiusPx.asFloat32,    topRightCornerRadiusPx.asFloat32,
						bottomRightCornerRadiusPx.asFloat32, bottomRightCornerRadiusPx.asFloat32,
						bottomLeftCornerRadiusPx.asFloat32,  bottomLeftCornerRadiusPx.asFloat32
					)
					insetRadii = floatArrayOf(
						(topLeftCornerRadiusPx     - insetPx).asFloat32,
						(topLeftCornerRadiusPx     - insetPx).asFloat32,
						(topRightCornerRadiusPx    - insetPx).asFloat32,
						(topRightCornerRadiusPx    - insetPx).asFloat32,
						(bottomRightCornerRadiusPx - insetPx).asFloat32,
						(bottomRightCornerRadiusPx - insetPx).asFloat32,
						(bottomLeftCornerRadiusPx  - insetPx).asFloat32,
						(bottomLeftCornerRadiusPx  - insetPx).asFloat32
					)
					halfPaddedRadii = floatArrayOf(
						(topLeftCornerRadiusPx     - halfInsetPx).asFloat32,
						(topLeftCornerRadiusPx     - halfInsetPx).asFloat32,
						(topRightCornerRadiusPx    - halfInsetPx).asFloat32,
						(topRightCornerRadiusPx    - halfInsetPx).asFloat32,
						(bottomRightCornerRadiusPx - halfInsetPx).asFloat32,
						(bottomRightCornerRadiusPx - halfInsetPx).asFloat32,
						(bottomLeftCornerRadiusPx  - halfInsetPx).asFloat32,
						(bottomLeftCornerRadiusPx  - halfInsetPx).asFloat32
					)
				}

				val elevationInPx = view.elevation.inPx.asFloat32

				impl.backgroundView.background = GradientDrawable().apply {
					shape = GradientDrawable.RECTANGLE
					cornerRadii = radii
					setColor(backgroundColor.argb)
				}
				impl.backgroundView.elevation = elevationInPx
				impl.backgroundView.alpha = shadowOpacity

				impl.containerView.insetRadii = insetRadii
				impl.containerView.insetPx = insetPx
				impl.containerView.background = GradientDrawable().apply {
					shape = GradientDrawable.RECTANGLE
					cornerRadii = radii
					setColor(backgroundColor.argb)
				}
				impl.containerView.elevation = elevationInPx

				impl.strokeView.background = GradientDrawable().apply {
					shape = GradientDrawable.RECTANGLE
					cornerRadii = halfPaddedRadii
					if (outlineThicknessPx > 0) {
						if (outlineDashLengthPx > 0) {
							setStroke(
								outlineThicknessPx, outlineColor.argb,
								outlineDashLengthPx.asFloat32, outlineDashLengthPx.asFloat32
							)
						} else {
							setStroke(
								outlineThicknessPx, outlineColor.argb
							)
						}
					}
				}
				impl.strokeView.elevation = elevationInPx
			}

			else -> {
				impl.setBackgroundColor(backgroundColor.argb)
			}

		}

	}


}