package ic.gui.adapters


import ic.android.ui.view.group.ext.filterChildren
import ic.android.ui.view.group.ext.forEachChild
import ic.android.ui.view.group.ext.noChild
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.reduce.find.atLeastOne

import ic.gui.view.container.ContainerView
import ic.gui.view.container.empty.EmptyView
import ic.gui.view.ext.updateImpl
import ic.util.log.logD


abstract class AndroidAContainerViewAdapter

	<View: ContainerView, Impl: android.view.View>

	: AndroidViewAdapter<View, Impl>()

{


	protected abstract val Impl.subviewsContainer : android.view.ViewGroup?


	protected open fun updateAndroidViewGroup (view: View, impl: Impl) {}


	final override fun View.updateAndroidViewSelf (androidView: Impl) {
		if (this.debugName != null) {
			logD("gui") { "$debugName updateAndroidViewSelf androidView: $androidView" }
		}
		updateAndroidViewGroup(this, androidView)
		val androidSubviewsContainer = androidView.subviewsContainer
		if (androidSubviewsContainer != null) {
			if (this.debugName != null) {
				logD("gui") { "$debugName updateAndroidViewSelf androidSubviewsContainer != null" }
			}
			androidSubviewsContainer.filterChildren { childAndroidView ->
				preparedSubviews.atLeastOne { it.impl === childAndroidView }
			}
			preparedSubviews.forEach { child ->
				if (child !is EmptyView || child.debugColor != null) {
					val childAndroidView = child.impl as android.view.View
					if (
						androidSubviewsContainer.noChild { it === childAndroidView }
					) {
						androidSubviewsContainer.addView(childAndroidView)
					}
					child.updateImpl()
				}
			}
			if (this.debugName != null) {
				androidSubviewsContainer.forEachChild { childAndroidView ->
					logD("gui") { "$debugName updateAndroidViewSelf childAndroidView: $childAndroidView" }
				}
			}
		}
	}


}