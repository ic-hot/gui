package ic.gui.adapters


import android.annotation.SuppressLint
import android.content.Context
import android.view.ContextThemeWrapper

import ic.util.time.Time
import ic.util.time.ext.dayOfMonth
import ic.util.time.ext.month
import ic.util.time.ext.year
import ic.util.time.funs.now

import ic.gui.R
import ic.gui.view.simple.datepicker.DatePickerView


internal class AndroidDatePickerViewAdapter (

	override val androidContext: Context

) : AndroidViewAdapter<DatePickerView, AndroidDatePickerViewAdapter.Impl>() {


	@SuppressLint("ViewConstructor")
	class Impl
		(
			androidContext : Context,
			style : DatePickerView.Style?
		)
		: android.widget.DatePicker (
			ContextThemeWrapper(
				androidContext,
				when (style) {
					null                          -> R.style.DatePickerDefault
					DatePickerView.Style.Spinners -> R.style.DatePickerSpinners
					DatePickerView.Style.Calendar -> R.style.DatePickerCalendar
				}
			)
		)
	{

		var isChangedByUserInput : Boolean = true

		var value : Time = now

		init {

			if (style == DatePickerView.Style.Spinners) {
				@Suppress("DEPRECATION")
				calendarViewShown = false
			}

		}

	}


	override fun initAndroidView (view: DatePickerView) : Impl {

		return Impl(androidContext = androidContext, style = view.style).apply {

			init(value.year, value.month, value.dayOfMonth) { _, year, month, day ->

				value = Time(
					initialTime = value,
					year = year,
					month = month,
					dayOfMonth = day
				)

				if (isChangedByUserInput) {
					view.callOnValueChanged(value)
				}

			}

		}

	}


	override fun DatePickerView.updateAndroidViewSelf (androidView: Impl) {

		val value = this.value

		if (androidView.value != value) {

			if (value != null) {

				androidView.value = value

				val year       = value.year
				val month      = value.month
				val dayOfMonth = value.dayOfMonth

				if (
					androidView.year       != year ||
					androidView.month      != month ||
					androidView.dayOfMonth != dayOfMonth
				) {
					androidView.isChangedByUserInput = false
					androidView.updateDate(year, month, dayOfMonth)
					androidView.isChangedByUserInput = true
				}

			}

		}

	}


}