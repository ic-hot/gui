package ic.gui.adapters


import android.content.Context

import ic.base.primitives.float32.Float32

import ic.graphics.color.Color

import ic.android.ui.view.ext.setBackgroundLinearGradient
import ic.gui.view.simple.gradient.LinearGradientView


internal class AndroidLinearGradientViewAdapter (

	override val androidContext: Context

) : AndroidViewAdapter<LinearGradientView, AndroidLinearGradientViewAdapter.Impl>() {


	class Impl (androidContext: Context) : android.view.View(androidContext) {

		private var startColor : Color? = null
		private var endColor   : Color? = null
		private var directionX : Float32 = 0F
		private var directionY : Float32 = 0F

		fun setGradient (
			startColor : Color, endColor : Color,
			directionX : Float32, directionY : Float32
		) {
			if (
				startColor == this.startColor && endColor == this.endColor &&
				directionX == this.directionX && directionY == this.directionY
			) {
				return
			}
			this.startColor = startColor
			this.endColor   = endColor
			this.directionX = directionX
			this.directionY = directionY
			setBackgroundLinearGradient(
				startColor = startColor,
				endColor = endColor,
				directionX = directionX,
				directionY = directionY
			)
		}

	}


	override fun initAndroidView (view: LinearGradientView) : Impl {
		return Impl(androidContext)
	}


	override fun LinearGradientView.updateAndroidViewSelf (androidView: Impl) {
		androidView.setGradient(
			startColor = startColor, endColor = endColor,
			directionX = directionX, directionY = directionY
		)
	}


}