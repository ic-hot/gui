package ic.gui.adapters


import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater

import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toByteArray

import ic.gui.R
import ic.gui.view.simple.pdf.PdfView


internal class AndroidPdfViewAdapter (

	override val androidContext : Context

) : AndroidViewAdapter<PdfView, AndroidPdfViewAdapter.Impl>() {


	class Impl
		(context: Context, set: AttributeSet)
		: com.github.barteksc.pdfviewer.PDFView(context, set)
	{
		var pdf : ByteSequence? = null
	}


	@SuppressLint("InflateParams")
	override fun initAndroidView (view: PdfView) : Impl {
		val layoutInflater = LayoutInflater.from(androidContext)
		val androidLayout = layoutInflater.inflate(R.layout.pdf, null, false)
		val androidView : Impl = androidLayout.findViewById(R.id.pdfView)
		return androidView
	}


	override fun PdfView.updateAndroidViewSelf (androidView: Impl) {
		val pdf = this.pdf
		if (androidView.pdf != pdf) {
			androidView.pdf = pdf
			if (pdf == null) {
				androidView.alpha = 0F
			} else {
				androidView.alpha = 1F
				val pdfByteArray = pdf.toByteArray()
				androidView.fromBytes(pdfByteArray).load()
			}
		}
	}


}