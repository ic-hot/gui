package ic.gui.scope.impl


import android.app.Activity

import ic.gui.scope.AndroidViewScope


inline val AndroidViewScope.activity get() = androidContext as Activity