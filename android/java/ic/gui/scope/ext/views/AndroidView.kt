package ic.gui.scope.ext.views


import ic.android.ui.view.scope.AndroidViewScope

import ic.gui.adapters.AndroidViewAdapters
import ic.gui.adapters.ViewAdapter
import ic.gui.adapters.ViewAdapters
import ic.gui.view.View


abstract class AndroidView : View() {


	protected abstract fun AndroidViewScope.initAndroidView() : android.view.View

	internal fun callInitAndroidView (androidViewScope: AndroidViewScope) : android.view.View {
		return androidViewScope.initAndroidView()
	}


	override val ViewAdapters<*>.adapter : ViewAdapter<*, *> get() {
		this as AndroidViewAdapters
		return androidViewAdapter
	}


}