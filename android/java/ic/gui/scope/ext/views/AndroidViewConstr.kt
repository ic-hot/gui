package ic.gui.scope.ext.views


import ic.android.ui.view.scope.AndroidViewScope
import ic.base.primitives.float32.Float32

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.Dp
import ic.gui.view.View


inline fun AndroidView (

	width : Dp, height : Dp,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,
	opacity : Float32 = 1F,

	crossinline createAndroidView : AndroidViewScope.() -> android.view.View

) : View {

	return object : AndroidView() {

		override fun AndroidViewScope.initAndroidView() : android.view.View {
			return createAndroidView(this)
		}

		override val layoutWidth  get() = width
		override val layoutHeight get() = height
		override val layoutWeight get() = weight
		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign
		override val opacity get() = opacity

	}

}