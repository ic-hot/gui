package ic.gui.scope


import android.content.Context

import ic.android.ui.activity.ext.*
import ic.android.ui.activity.ext.keyboard.hideKeyboard
import ic.android.ui.activity.ext.keyboard.showKeyboard
import ic.android.ui.activity.ext.nav.external.goToMapsNavigator
import ic.android.ui.activity.ext.nav.external.openImageViewer
import ic.android.ui.activity.ext.nav.external.openPhoneDialer
import ic.android.ui.activity.ext.nav.external.openEmailSender
import ic.base.primitives.float32.Float32
import ic.util.geo.Location

import ic.graphics.image.Image
import ic.graphics.image.ext.asAndroidBitmap

import ic.gui.scope.impl.activity


interface AndroidViewScope : ViewScope {


	val androidContext : Context


	override fun goToMapsNavigator (destination: Location) {
		activity.goToMapsNavigator(destination)
	}

	override fun openImageViewer (image: Image) {
		activity.openImageViewer(image.asAndroidBitmap)
	}

	override fun openImageViewer (imageUrl: String) {
		activity.openImageViewer(imageUrl = imageUrl)
	}

	override fun showKeyboard() = activity.showKeyboard()
	override fun hideKeyboard() = activity.hideKeyboard()


	override fun copyToClipboard (text: String) {
		ic.android.util.clipboard.copyToClipboard(text)
	}

	override fun getStringFromClipboard() : String? {
		return ic.android.util.clipboard.getStringFromClipboard()
	}


	override fun setScreenBrightness (brightness: Float32) {
		activity.setScreenBrightness(brightness)
	}

	override fun setAppLanguage(languageCode: String?) {
		ic.android.util.locale.setAppLanguage(languageCode = languageCode)
	}


	override fun openEmailSender(email: String) {
		activity.openEmailSender(email)
	}

	override fun openPhoneDialer(phoneNumber: String) {
		activity.openPhoneDialer(phoneNumber)
	}


}