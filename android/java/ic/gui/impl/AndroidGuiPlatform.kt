package ic.gui.impl


import ic.android.app.thisApp
import ic.android.context.ext.sendBroadcast
import ic.stream.buffer.ByteBuffer
import ic.stream.output.ByteOutput

import ic.gui.app.context.AndroidGuiAppContext

import ic.storage.res.impl.AndroidGuiResources


internal object AndroidGuiPlatform : GuiPlatform {


	override val guiAppContext get() = AndroidGuiAppContext

	override val resources get() = AndroidGuiResources


	override fun getScreenDensityFactor() = ic.android.system.screenDensityFactor


	override fun openBroadcastOutput() : ByteOutput {
		return ByteBuffer(
			onClose = {
				thisApp.sendBroadcast(
					action = "ic.gui.broadcast",
					"data" to this.toByteArray()
				)
			}
		)
	}

}