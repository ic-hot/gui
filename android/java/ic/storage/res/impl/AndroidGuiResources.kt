package ic.storage.res.impl


import android.graphics.Typeface

import ic.android.storage.assets.getFontFromAssetsOrNull
import ic.android.storage.res.getResFontOrNull

import ic.gui.font.AndroidFont
import ic.gui.font.Font


object AndroidGuiResources : GuiResources {


	override fun getDefaultFont() = AndroidFont(
		androidTypeface = null,
		modifier = Typeface.NORMAL
	)

	override fun getDefaultBoldFont() = AndroidFont(
		androidTypeface = null,
		modifier = Typeface.BOLD
	)

	override fun getDefaultMonospaceFont() = AndroidFont(
		androidTypeface = Typeface.MONOSPACE,
		modifier = Typeface.NORMAL
	)


	override fun getFontOrNull (path: String, fontName: String) : Font? {
		return (
			getResFontOrNull(path) ?: getFontFromAssetsOrNull(path)
		)?.let { AndroidFont(it) }
	}


}