package ic.android.ui.view.group.ext


import android.view.ViewGroup


fun ViewGroup.setSingleChildView (child: android.view.View?) {

	if (child == null) {
		if (childCount > 0) {
			removeAllViews()
		}
	} else {
		when (childCount) {
			0 -> {
				addView(child)
			}
			1 -> {
				if (getChildAt(0) !== child) {
					removeAllViews()
					addView(child)
				}
			}
			else -> {
				removeAllViews()
				addView(child)
			}
		}
	}

}