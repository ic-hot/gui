package ic.android.ui.view.group.ext


import android.view.ViewGroup

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.dim.px.ext.inDp
import ic.gui.view.context.AndroidViewContext
import ic.gui.scope.ViewScope
import ic.gui.view.View


fun ViewGroup.setSingleChildView (createView: ViewScope.() -> View) {

	var containerWidth  : Dp = 0.dp
	var containerHeight : Dp = 0.dp

	lateinit var view : View

	val viewContext = AndroidViewContext(
		androidContext = context,
		getRootMaxWidth  = { containerWidth  },
		getRootMaxHeight = { containerHeight }
	)

	view = viewContext.run { createView() }

	viewContext.open(view)

	addOnLayoutChangeListener { _, left, top, right, bottom, _, _, _, _ ->

		containerWidth  = (right - left).inDp
		containerHeight = (bottom - top).inDp

		viewContext.updateAllViews()

	}

	setSingleChildView(view.impl as android.view.View)

}