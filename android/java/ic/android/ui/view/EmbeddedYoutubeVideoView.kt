package ic.android.ui.view


import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup.LayoutParams
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout

import ic.android.ui.view.ext.activity
import ic.android.ui.view.web.ext.loadPage


@Suppress("DEPRECATION")
@SuppressLint("SetJavaScriptEnabled")


class EmbeddedYoutubeVideoView : WebView {


	private var fullScreenView: View? = null

	private var mCustomViewCallback: WebChromeClient.CustomViewCallback? = null
	private var mOriginalOrientation = 0
	private var mOriginalSystemUiVisibility = 0


	var onFullScreenStateChanged : ((isFullScreen: Boolean) -> Unit)? = null

	private fun onExitFullScreen() {
		(activity.window.decorView as FrameLayout).removeView(fullScreenView)
		fullScreenView = null
		activity.window.decorView.systemUiVisibility = mOriginalSystemUiVisibility
		activity.requestedOrientation = mOriginalOrientation
		mCustomViewCallback!!.onCustomViewHidden()
		mCustomViewCallback = null
		onFullScreenStateChanged?.invoke(false)
	}


	init {

		webViewClient = object : WebViewClient() {
			@Deprecated("Deprecated in Java")
			override fun shouldOverrideUrlLoading (view: WebView, url: String) = false
		}

		webChromeClient = object : WebChromeClient() {
			override fun onShowCustomView (view: View, callback: CustomViewCallback) {
				if (fullScreenView != null) {
					onHideCustomView()
					return
				}
				fullScreenView = view
				mOriginalSystemUiVisibility = activity.window.decorView.systemUiVisibility
				mOriginalOrientation = activity.requestedOrientation
				activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_USER
				mCustomViewCallback = callback
				(activity.window.decorView as FrameLayout).addView(fullScreenView, LayoutParams(-1, -1))
				activity.window.decorView.systemUiVisibility = 3846
				onFullScreenStateChanged?.invoke(true)
			}
			override fun onHideCustomView() {
				onExitFullScreen()
			}
		}

		settings.javaScriptEnabled = true
		settings.loadWithOverviewMode = true
		settings.useWideViewPort = true

	}


	val isFullScreen : Boolean get() {
		return fullScreenView != null
	}

	fun exitFullScreen() {
		goBack()
		if (isFullScreen) {
			onExitFullScreen()
		}
	}


	fun setVideoId (videoId: String) {
		loadPage(
			"<html>" +
				"<meta name='viewport' content='width=384 height=220'>" +
				"<style>" +
					"body {" +
						"margin: 0;" +
						"padding: 0;" +
					"}" +
				"</style>" +
				"<body>" +
					"<iframe " +
						"width='384' " +
						"height='220' " +
						"src='https://www.youtube.com/embed/$videoId' " +
						"frameborder='0' " +
						"allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' " +
						"allowfullscreen" +
					"></iframe>" +
				"</body" +
			"</html>"
		)
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr)


}