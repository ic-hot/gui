package ic.android.ui.view.ext


import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import android.widget.LinearLayout.VERTICAL

import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32

import ic.gui.dim.px.ByContainer
import ic.gui.dim.px.ByContent
import ic.gui.dim.px.Px


inline var View.layoutHeightPx : Px

	get() {

		val layoutParams = this.layoutParams!!
		val layoutParam = layoutParams.height

		return when {

			layoutParam == MATCH_PARENT -> ByContainer
			layoutParam == WRAP_CONTENT -> ByContent

			(

				layoutParams is LinearLayout.LayoutParams &&
				layoutParams.weight != 0F &&
				(parent as LinearLayout).orientation == VERTICAL

			) -> ByContainer

			else -> layoutParam

		}

	}

	set(value) = setLayoutHeight(value, weight = 1F)

;


@Suppress("NOTHING_TO_INLINE")
inline fun View.setLayoutHeight (layoutHeight: Px, weight: Float32) {

	val layoutParams = this.layoutParams ?: return

	if (layoutParams is LinearLayout.LayoutParams) {
		val parent = this.parent as LinearLayout
		if (parent.orientation == VERTICAL) {
			val newHeight : Int32
			val newWeight : Float32
			when (layoutHeight) {
				ByContainer -> {
					newHeight = 0
					newWeight = weight
				}
				ByContent -> {
					newHeight = WRAP_CONTENT
					newWeight = 0F
				}
				else -> {
					newHeight = layoutHeight
					newWeight = 0F
				}
			}
			if (newHeight == layoutParams.height && newWeight == layoutParams.weight) return
			layoutParams.height = newHeight
			layoutParams.weight = newWeight
			this.layoutParams = layoutParams
			return
		}
	}

	val newHeight = when (layoutHeight) {
		ByContainer -> MATCH_PARENT
		ByContent   -> WRAP_CONTENT
		else        -> layoutHeight
	}
	if (newHeight == layoutParams.height) return
	layoutParams.height = newHeight
	this.layoutParams = layoutParams

}