package ic.android.ui.view.ext


import android.annotation.SuppressLint
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.RelativeLayout.*

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.Left
import ic.gui.align.Right


inline var View.layoutHorizontalAlign : HorizontalAlign

	@SuppressLint("RtlHardcoded")
	get() {

		val lp = this.layoutParams!!

		return when (lp) {

			is FrameLayout.LayoutParams -> when (lp.gravity and Gravity.HORIZONTAL_GRAVITY_MASK) {
				Gravity.LEFT 	-> Left
				Gravity.RIGHT 	-> Right
				else 			-> Center
			}

			is LinearLayout.LayoutParams -> when (lp.gravity and Gravity.HORIZONTAL_GRAVITY_MASK) {
				Gravity.LEFT 	-> Left
				Gravity.RIGHT 	-> Right
				else 			-> Center
			}

			is RelativeLayout.LayoutParams -> when {
				lp.rules[ALIGN_PARENT_LEFT] != 0 	-> Left
				lp.rules[ALIGN_PARENT_RIGHT] != 0 -> Right
				else -> Center
			}

			else -> Center

		}

	}

	@SuppressLint("RtlHardcoded")
	set(value) {

		val lp = this.layoutParams ?: return

		when (lp) {

			is FrameLayout.LayoutParams -> {
				val newGravity = when (value) {
					Left -> {
						Gravity.LEFT or (lp.gravity and Gravity.VERTICAL_GRAVITY_MASK)
					}
					Right -> {
						Gravity.RIGHT or (lp.gravity and Gravity.VERTICAL_GRAVITY_MASK)
					}
					else -> {
						Gravity.CENTER_HORIZONTAL or (lp.gravity and Gravity.VERTICAL_GRAVITY_MASK)
					}
				}
				if (newGravity == lp.gravity) return
				lp.gravity = newGravity
				this.layoutParams = lp
			}

			is LinearLayout.LayoutParams -> {
				val newGravity = when (value) {
					Left -> {
						Gravity.LEFT or (lp.gravity and Gravity.VERTICAL_GRAVITY_MASK)
					}
					Right -> {
						Gravity.RIGHT or (lp.gravity and Gravity.VERTICAL_GRAVITY_MASK)
					}
					else -> {
						Gravity.CENTER_HORIZONTAL or (lp.gravity and Gravity.VERTICAL_GRAVITY_MASK)
					}
				}
				if (newGravity == lp.gravity) return
				lp.gravity = newGravity
				this.layoutParams = lp
			}

			is RelativeLayout.LayoutParams -> {
				when (value) {
					Left  -> {
						lp.rules[ALIGN_PARENT_LEFT]  = TRUE
						lp.rules[CENTER_HORIZONTAL]  = 0
						lp.rules[ALIGN_PARENT_RIGHT] = 0
					}
					Right -> {
						lp.rules[ALIGN_PARENT_LEFT]  = 0
						lp.rules[CENTER_HORIZONTAL]  = 0
						lp.rules[ALIGN_PARENT_RIGHT] = TRUE
					}
					else -> {
						lp.rules[ALIGN_PARENT_LEFT]  = 0
						lp.rules[CENTER_HORIZONTAL]  = TRUE
						lp.rules[ALIGN_PARENT_RIGHT] = 0
					}
				}
				this.layoutParams = lp
			}

		}

	}

;