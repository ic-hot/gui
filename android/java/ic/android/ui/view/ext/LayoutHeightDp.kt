package ic.android.ui.view.ext


import android.view.View

import ic.base.primitives.float32.Float32

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.inPx
import ic.gui.dim.px.ext.inDp


inline var View.layoutHeightDp : Dp
	get() {
		return layoutHeightPx.inDp
	}
	set(value) {
		layoutHeightPx = value.inPx
	}
;


@Suppress("NOTHING_TO_INLINE")
inline fun View.setLayoutHeight (layoutHeight: Dp, weight: Float32) {
	setLayoutHeight(layoutHeight.inPx, weight = weight)
}