package ic.android.ui.view.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32

import android.view.View

import ic.android.util.units.pxToDp
import ic.android.util.units.dpToPx


inline var View.elevationPx : Int32
	get() = elevation.asInt32
	set(value) { elevation = value.asFloat32 }
;


inline var View.elevationDp : Float32
	get() = pxToDp(elevationPx)
	set(value) { elevationPx = dpToPx(value) }
;