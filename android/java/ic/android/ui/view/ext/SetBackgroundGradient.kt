package ic.android.ui.view.ext


import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.GradientDrawable.Orientation.*

import ic.base.arrays.Int32Array
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.ext.asFloat32

import ic.graphics.color.Color
import ic.graphics.color.ColorArgb

import ic.gui.dim.px.Px


fun android.view.View.setBackgroundLinearGradient (

	startColor : ColorArgb,
	endColor   : ColorArgb,

	directionX : Float32,
	directionY : Float32,

	cornersRadius : Px = 0

) {

	val orientation = when {
		directionX >  0F && directionY == 0F -> LEFT_RIGHT
		directionX <  0F && directionY == 0F -> RIGHT_LEFT
		directionX >  0F && directionY >  0F -> TL_BR
		directionX == 0F && directionY >  0F -> TOP_BOTTOM
		directionX == 0F && directionY <  0F -> BOTTOM_TOP
		else -> throw NotImplementedError()
	}

	val gradientDrawable = GradientDrawable(
		orientation,
		Int32Array(startColor, endColor)
	)

	gradientDrawable.cornerRadius = cornersRadius.asFloat32

	background = gradientDrawable

}


fun android.view.View.setBackgroundLinearGradient (
	startColor : Color, endColor : Color,
	directionX : Float32, directionY : Float32,
	cornersRadius : Px = 0
) {
	setBackgroundLinearGradient(
		startColor = startColor.argb, endColor = endColor.argb,
		directionX = directionX, directionY = directionY,
		cornersRadius = cornersRadius
	)
}