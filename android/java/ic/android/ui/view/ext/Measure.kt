package ic.android.ui.view.ext


import android.view.View

import ic.android.ui.view.funs.layoutParamToDp
import ic.android.ui.view.funs.layoutParamToPx

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.asAndroidMeasureSpec
import ic.gui.dim.px.Px
import ic.gui.dim.px.ext.asAndroidMeasureSpec


fun View.measureInPx (
	width  : Px = layoutParamToPx(layoutParams.width),
	height : Px = layoutParamToPx(layoutParams.height)
) {
	measure(
		width.asAndroidMeasureSpec,
		height.asAndroidMeasureSpec
	)
}


fun View.measure (
	width  : Dp = layoutParamToDp(layoutParams.width),
	height : Dp = layoutParamToDp(layoutParams.height)
) {
	measure(
		width.asAndroidMeasureSpec,
		height.asAndroidMeasureSpec
	)
}