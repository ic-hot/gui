package ic.android.ui.view.ext


import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import android.widget.LinearLayout.HORIZONTAL
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32

import ic.gui.dim.px.ByContainer
import ic.gui.dim.px.ByContent
import ic.gui.dim.px.Px


var View.layoutWidthPx : Px

	get() {

		val layoutParams = this.layoutParams!!
		val layoutParam = layoutParams.width

		return when {

			layoutParam == MATCH_PARENT -> ByContainer
			layoutParam == WRAP_CONTENT -> ByContent

			(
				layoutParams is LinearLayout.LayoutParams &&
				layoutParams.weight != 0F &&
				(parent as LinearLayout).orientation == HORIZONTAL
			) -> ByContainer

			else -> layoutParam

		}

	}

	set(value) = setLayoutWidth(value, weight = 1F)

;


fun View.setLayoutWidth (layoutWidth: Px, weight: Float32) {

	val layoutParams = this.layoutParams ?: return

	if (layoutParams is LinearLayout.LayoutParams) {
		val parent = this.parent as LinearLayout
		if (parent.orientation == HORIZONTAL) {
			val newWidth : Int32
			val newWeight : Float32
			when (layoutWidth) {
				ByContainer -> {
					newWidth = 0
					newWeight = weight
				}
				ByContent -> {
					newWidth = WRAP_CONTENT
					newWeight = 0F
				}
				else -> {
					newWidth = layoutWidth
					newWeight = 0F
				}
			}
			if (newWidth == layoutParams.width && newWeight == layoutParams.weight) return
			layoutParams.width = newWidth
			layoutParams.weight = newWeight
			this.layoutParams = layoutParams
			return
		}
	}

	if (layoutParams is RecyclerView.LayoutParams) {
		val parent = this.parent
		if (parent is RecyclerView) {
			val layoutManager = parent.layoutManager
			if (layoutManager is LinearLayoutManager) {
				if (layoutManager.orientation == LinearLayoutManager.HORIZONTAL) {
					val newWidth = when (layoutWidth) {
						ByContainer -> parent.measuredWidthPx
						ByContent   -> WRAP_CONTENT
						else        -> layoutWidth
					}
					if (newWidth == layoutParams.width) return
					layoutParams.width = newWidth
					this.layoutParams = layoutParams
					return
				}
			}
		}
	}

	val newWidth = when (layoutWidth) {
		ByContainer -> MATCH_PARENT
		ByContent   -> WRAP_CONTENT
		else        -> layoutWidth
	}
	if (newWidth == layoutParams.width) return
	layoutParams.width = newWidth
	this.layoutParams = layoutParams

}