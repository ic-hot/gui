package ic.android.ui.view.ext


import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.RelativeLayout.*

import ic.gui.align.*


inline var View.layoutVerticalAlign : VerticalAlign

	get() {

		val layoutParams = this.layoutParams!!

		return when (layoutParams) {

			is FrameLayout.LayoutParams -> when (layoutParams.gravity and Gravity.VERTICAL_GRAVITY_MASK) {
				Gravity.TOP 	-> Top
				Gravity.BOTTOM 	-> Bottom
				else 			-> Center
			}

			is LinearLayout.LayoutParams -> when (layoutParams.gravity and Gravity.VERTICAL_GRAVITY_MASK) {
				Gravity.TOP 	-> Top
				Gravity.BOTTOM 	-> Bottom
				else 			-> Center
			}

			is RelativeLayout.LayoutParams -> when {
				layoutParams.rules[ALIGN_PARENT_TOP]    != 0 -> Top
				layoutParams.rules[ALIGN_PARENT_BOTTOM] != 0 -> Bottom
				else -> Center
			}

			else -> Center

		}

	}

	set(value) {

		val lp = this.layoutParams ?: return

		when (lp) {

			is FrameLayout.LayoutParams -> {
				val newGravity = when (value) {
					Top -> {
						Gravity.TOP or (lp.gravity and Gravity.HORIZONTAL_GRAVITY_MASK)
					}
					Bottom -> {
						Gravity.BOTTOM or (lp.gravity and Gravity.HORIZONTAL_GRAVITY_MASK)
					}
					else -> {
						Gravity.CENTER_VERTICAL or (lp.gravity and Gravity.HORIZONTAL_GRAVITY_MASK)
					}
				}
				if (newGravity == lp.gravity) return
				lp.gravity = newGravity
				this.layoutParams = lp
			}

			is LinearLayout.LayoutParams -> {
				val newGravity = when (value) {
					Top -> {
						Gravity.TOP or (lp.gravity and Gravity.HORIZONTAL_GRAVITY_MASK)
					}
					Bottom -> {
						Gravity.BOTTOM or (lp.gravity and Gravity.HORIZONTAL_GRAVITY_MASK)
					}
					else -> {
						Gravity.CENTER_VERTICAL or (lp.gravity and Gravity.HORIZONTAL_GRAVITY_MASK)
					}
				}
				if (newGravity == lp.gravity) return
				lp.gravity = newGravity
				this.layoutParams = lp
			}

			is RelativeLayout.LayoutParams -> {
				when (value) {
					Top  -> {
						lp.rules[ALIGN_PARENT_TOP]    = TRUE
						lp.rules[CENTER_VERTICAL]     = 0
						lp.rules[ALIGN_PARENT_BOTTOM] = 0
					}
					Bottom -> {
						lp.rules[ALIGN_PARENT_TOP]    = 0
						lp.rules[CENTER_VERTICAL]     = 0
						lp.rules[ALIGN_PARENT_BOTTOM] = TRUE
					}
					else -> {
						lp.rules[ALIGN_PARENT_TOP]    = 0
						lp.rules[CENTER_VERTICAL]     = TRUE
						lp.rules[ALIGN_PARENT_BOTTOM] = 0
					}
				}
				this.layoutParams = lp
			}

		}

	}

;