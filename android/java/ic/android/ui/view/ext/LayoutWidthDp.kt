package ic.android.ui.view.ext


import android.view.View

import ic.base.primitives.float32.Float32

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.inPx
import ic.gui.dim.px.ext.inDp


inline var View.layoutWidthDp : Dp
	get() {
		return layoutWidthPx.inDp
	}
	set(value) {
		layoutWidthPx = value.inPx
	}
;


@Suppress("NOTHING_TO_INLINE")
inline fun View.setLayoutWidth (layoutWidth: Dp, weight: Float32) {
	setLayoutWidth(layoutWidth.inPx, weight = weight)
}