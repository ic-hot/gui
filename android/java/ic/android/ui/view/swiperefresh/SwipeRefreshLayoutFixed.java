/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ic.android.ui.view.swiperefresh;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.NestedScrollingChild;
import androidx.core.view.NestedScrollingChild2;
import androidx.core.view.NestedScrollingChild3;
import androidx.core.view.NestedScrollingChildHelper;
import androidx.core.view.NestedScrollingParent;
import androidx.core.view.NestedScrollingParent2;
import androidx.core.view.NestedScrollingParent3;
import androidx.core.view.NestedScrollingParentHelper;
import androidx.core.view.ViewCompat;
import androidx.core.widget.ListViewCompat;

/**
 * The SwipeRefreshLayout should be used whenever the user can refresh the
 * contents of a view via a vertical swipe gesture. The activity that
 * instantiates this view should add an OnRefreshListener to be notified
 * whenever the swipe to refresh gesture is completed. The SwipeRefreshLayout
 * will notify the listener each and every time the gesture is completed again;
 * the listener is responsible for correctly determining when to actually
 * initiate a refresh of its content. If the listener determines there should
 * not be a refresh, it must call setRefreshing(false) to cancel any visual
 * indication of a refresh. If an activity wishes to show just the progress
 * animation, it should call setRefreshing(true). To disable the gesture and
 * progress animation, call setEnabled(false) on the view.
 * <p>
 * This layout should be made the parent of the view that will be refreshed as a
 * result of the gesture and can only support one direct child. This view will
 * also be made the target of the gesture and will be forced to match both the
 * width and the height supplied in this layout. The SwipeRefreshLayout does not
 * provide accessibility events; instead, a menu item must be provided to allow
 * refresh of the content wherever this gesture is used.
 * </p>
 */
public abstract class SwipeRefreshLayoutFixed extends ViewGroup implements NestedScrollingParent3,
	NestedScrollingParent2, NestedScrollingChild3, NestedScrollingChild2, NestedScrollingParent,
	NestedScrollingChild {

	private static final String LOG_TAG = SwipeRefreshLayoutFixed.class.getSimpleName();

	private static final int INVALID_POINTER = -1;

	// Default offset in dips from the top of the view to where the progress spinner should stop
	private static final int DEFAULT_CIRCLE_TARGET = 64;

	private View mTarget; // the target of the gesture
	protected boolean mIsRefreshing = false;
	private int mTouchSlop;
	private float mTotalDragDistance = -1;

	// If nested scrolling is enabled, the total amount that needed to be
	// consumed by this as the nested scrolling parent is used in place of the
	// overscroll determined by MOVE events in the onTouch handler
	private float mTotalUnconsumed;
	private final NestedScrollingParentHelper mNestedScrollingParentHelper;
	private final NestedScrollingChildHelper mNestedScrollingChildHelper;
	private final int[] mParentScrollConsumed = new int[2];
	private final int[] mParentOffsetInWindow = new int[2];

	// Used for calls from old versions of onNestedScroll to v3 version of onNestedScroll. This only
	// exists to prevent GC costs that are present before API 21.
	private final int[] mNestedScrollingV2ConsumedCompat = new int[2];
	private boolean mNestedScrollInProgress;

	private float mInitialMotionY;
	private float mInitialDownY;
	private boolean mIsBeingDragged;
	private int mActivePointerId = INVALID_POINTER;

	// Target is returning to its start offset because it was cancelled or a
	// refresh was triggered.
	private boolean mReturningToStart;
	private static final int[] LAYOUT_ATTRS = new int[] {
		android.R.attr.enabled
	};

	int mSpinnerOffsetEnd;

	// Whether the client has set a custom starting position;
	boolean mUsingCustomStart;

	private OnChildScrollUpCallback mChildScrollUpCallback;

	/** @see #setLegacyRequestDisallowInterceptTouchEventEnabled */
	private boolean mEnableLegacyRequestDisallowInterceptTouch;

	/**
	 * Simple constructor to use when creating a SwipeRefreshLayout from code.
	 *
	 * @param context
	 */
	public SwipeRefreshLayoutFixed(@NonNull Context context) {
		this(context, null);
	}

	/**
	 * Constructor that is called when inflating SwipeRefreshLayout from XML.
	 *
	 * @param context
	 * @param attrs
	 */
	public SwipeRefreshLayoutFixed(@NonNull Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);

		mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();

		setWillNotDraw(false);

		final DisplayMetrics metrics = getResources().getDisplayMetrics();

		setChildrenDrawingOrderEnabled(true);
		// the absolute offset has to take into account that the circle starts at an offset
		mSpinnerOffsetEnd = (int) (DEFAULT_CIRCLE_TARGET * metrics.density);
		mTotalDragDistance = mSpinnerOffsetEnd;
		mNestedScrollingParentHelper = new NestedScrollingParentHelper(this);

		mNestedScrollingChildHelper = new NestedScrollingChildHelper(this);
		setNestedScrollingEnabled(true);

		final TypedArray a = context.obtainStyledAttributes(attrs, LAYOUT_ATTRS);
		setEnabled(a.getBoolean(0, true));
		a.recycle();
	}

	private void ensureTarget() {
		// Don't bother getting the parent height if the parent hasn't been laid
		// out yet.
		if (mTarget == null) {
			for (int i = 0; i < getChildCount(); i++) {
				View child = getChildAt(i);
				//if (!child.equals(mCircleView)) {
					mTarget = child;
					break;
				//}
			}
		}
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		final int width = getMeasuredWidth();
		final int height = getMeasuredHeight();
		if (getChildCount() == 0) {
			return;
		}
		if (mTarget == null) {
			ensureTarget();
		}
		if (mTarget == null) {
			return;
		}
		final View child = mTarget;
		final int childLeft = getPaddingLeft();
		final int childTop = getPaddingTop();
		final int childWidth = width - getPaddingLeft() - getPaddingRight();
		final int childHeight = height - getPaddingTop() - getPaddingBottom();
		child.layout(childLeft, childTop, childLeft + childWidth, childTop + childHeight);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (mTarget == null) {
			ensureTarget();
		}
		if (mTarget == null) {
			return;
		}
		mTarget.measure(MeasureSpec.makeMeasureSpec(
			getMeasuredWidth() - getPaddingLeft() - getPaddingRight(),
			MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(
			getMeasuredHeight() - getPaddingTop() - getPaddingBottom(), MeasureSpec.EXACTLY));
	}

	/**
	 * @return Whether it is possible for the child view of this layout to
	 *         scroll up. Override this if the child view is a custom view.
	 */
	public boolean canChildScrollUp() {
		if (mChildScrollUpCallback != null) {
			return mChildScrollUpCallback.canChildScrollUp(this, mTarget);
		}
		if (mTarget instanceof ListView) {
			return ListViewCompat.canScrollList((ListView) mTarget, -1);
		}
		return mTarget.canScrollVertically(-1);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		ensureTarget();

		final int action = ev.getActionMasked();
		int pointerIndex;

		if (mReturningToStart && action == MotionEvent.ACTION_DOWN) {
			mReturningToStart = false;
		}

		if (!isEnabled() || mReturningToStart || canChildScrollUp()
			|| mIsRefreshing || mNestedScrollInProgress) {
			// Fail fast if we're not in a state where a swipe is possible
			return false;
		}

		switch (action) {
			case MotionEvent.ACTION_DOWN:
				//setTargetOffsetTopAndBottom(mOriginalOffsetTop - mCircleView.getTop());
				mActivePointerId = ev.getPointerId(0);
				mIsBeingDragged = false;

				pointerIndex = ev.findPointerIndex(mActivePointerId);
				if (pointerIndex < 0) {
					return false;
				}
				mInitialDownY = ev.getY(pointerIndex);
				break;

			case MotionEvent.ACTION_MOVE:
				if (mActivePointerId == INVALID_POINTER) {
					Log.e(LOG_TAG, "Got ACTION_MOVE event but don't have an active pointer id.");
					return false;
				}

				pointerIndex = ev.findPointerIndex(mActivePointerId);
				if (pointerIndex < 0) {
					return false;
				}
				final float y = ev.getY(pointerIndex);
				startDragging(y);
				break;

			case MotionEvent.ACTION_POINTER_UP:
				onSecondaryPointerUp(ev);
				break;

			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
				mIsBeingDragged = false;
				mActivePointerId = INVALID_POINTER;
				break;
		}

		return mIsBeingDragged;
	}

	/**
	 * Enables the legacy behavior of {@link #requestDisallowInterceptTouchEvent} from before
	 * 1.1.0-alpha03, where the request is not propagated up to its parents in either of the
	 * following two cases:
	 * <ul>
	 *     <li>The child as an {@link AbsListView} and the runtime is API < 21</li>
	 *     <li>The child has nested scrolling disabled</li>
	 * </ul>
	 * Use this method <em>only</em> if your application:
	 * <ul>
	 *     <li>is upgrading SwipeRefreshLayout from &lt; 1.1.0-alpha03 to &gt;= 1.1.0-alpha03</li>
	 *     <li>relies on a parent of SwipeRefreshLayout to intercept touch events and that
	 *     parent no longer responds to touch events</li>
	 *     <li>setting this method to {@code true} fixes that issue</li>
	 * </ul>
	 *
	 * @param enabled {@code true} to enable the legacy behavior, {@code false} for default behavior
	 * @deprecated Only use this method if the changes introduced in
	 *             {@link #requestDisallowInterceptTouchEvent} in version 1.1.0-alpha03 are breaking
	 *             your application.
	 */
	@Deprecated
	public void setLegacyRequestDisallowInterceptTouchEventEnabled(boolean enabled) {
		mEnableLegacyRequestDisallowInterceptTouch = enabled;
	}

	@Override
	public void requestDisallowInterceptTouchEvent(boolean b) {
		// if this is a List < L or another view that doesn't support nested
		// scrolling, ignore this request so that the vertical scroll event
		// isn't stolen
		if ((android.os.Build.VERSION.SDK_INT < 21 && mTarget instanceof AbsListView)
			|| (mTarget != null && !ViewCompat.isNestedScrollingEnabled(mTarget))) {
			if (mEnableLegacyRequestDisallowInterceptTouch) {
				// Nope.
			} else {
				// Ignore here, but pass it up to our parent
				ViewParent parent = getParent();
				if (parent != null) {
					parent.requestDisallowInterceptTouchEvent(b);
				}
			}
		} else {
			super.requestDisallowInterceptTouchEvent(b);
		}
	}

	// NestedScrollingParent 3

	@Override
	public void onNestedScroll(@NonNull View target, int dxConsumed, int dyConsumed,
	                           int dxUnconsumed, int dyUnconsumed, @ViewCompat.NestedScrollType int type,
	                           @NonNull int[] consumed) {
		if (type != ViewCompat.TYPE_TOUCH) {
			return;
		}

		// This is a bit of a hack. onNestedScroll is typically called up the hierarchy of nested
		// scrolling parents/children, where each consumes distances before passing the remainder
		// to parents.  In our case, we want to try to run after children, and after parents, so we
		// first pass scroll distances to parents and consume after everything else has.
		int consumedBeforeParents = consumed[1];
		dispatchNestedScroll(dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed,
			mParentOffsetInWindow, type, consumed);
		int consumedByParents = consumed[1] - consumedBeforeParents;
		int unconsumedAfterParents = dyUnconsumed - consumedByParents;

		// There are two reasons why scroll distance may be totally consumed.  1) All of the nested
		// scrolling parents up the hierarchy implement NestedScrolling3 and consumed all of the
		// distance or 2) at least 1 nested scrolling parent doesn't implement NestedScrolling3 and
		// for comparability reasons, we are supposed to act like they have.
		//
		// We must assume 2) is the case because we have no way of determining that it isn't, and
		// therefore must fallback to a previous hack that was done before nested scrolling 3
		// existed.
		int remainingDistanceToScroll;
		if (unconsumedAfterParents == 0) {
			// The previously implemented hack is to see how far we were offset and assume that that
			// distance is equal to how much all of our parents consumed.
			remainingDistanceToScroll = dyUnconsumed + mParentOffsetInWindow[1];
		} else {
			remainingDistanceToScroll = unconsumedAfterParents;
		}

		// Not sure why we have to make sure the child can't scroll up... but seems dangerous to
		// remove.
		if (remainingDistanceToScroll < 0 && !canChildScrollUp()) {
			mTotalUnconsumed += Math.abs(remainingDistanceToScroll);
			onOverscroll(mTotalUnconsumed);
			// If we've gotten here, we need to consume whatever is left to consume, which at this
			// point is either equal to 0, or remainingDistanceToScroll.
			consumed[1] += unconsumedAfterParents;
		}
	}

	// NestedScrollingParent 2

	@Override
	public boolean onStartNestedScroll(View child, View target, int axes, int type) {
		if (type == ViewCompat.TYPE_TOUCH) {
			return onStartNestedScroll(child, target, axes);
		} else {
			return false;
		}
	}

	@Override
	public void onNestedScrollAccepted(View child, View target, int axes, int type) {
		// Should always be true because onStartNestedScroll returns false for all type !=
		// ViewCompat.TYPE_TOUCH, but check just in case.
		if (type == ViewCompat.TYPE_TOUCH) {
			onNestedScrollAccepted(child, target, axes);
		}
	}

	@Override
	public void onStopNestedScroll(View target, int type) {
		// Should always be true because onStartNestedScroll returns false for all type !=
		// ViewCompat.TYPE_TOUCH, but check just in case.
		if (type == ViewCompat.TYPE_TOUCH) {
			onStopNestedScroll(target);
		}
	}

	@Override
	public void onNestedScroll(View target, int dxConsumed, int dyConsumed, int dxUnconsumed,
	                           int dyUnconsumed, int type) {
		onNestedScroll(target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, type,
			mNestedScrollingV2ConsumedCompat);
	}

	@Override
	public void onNestedPreScroll(View target, int dx, int dy, int[] consumed, int type) {
		// Should always be true because onStartNestedScroll returns false for all type !=
		// ViewCompat.TYPE_TOUCH, but check just in case.
		if (type == ViewCompat.TYPE_TOUCH) {
			onNestedPreScroll(target, dx, dy, consumed);
		}
	}

	// NestedScrollingParent 1

	@Override
	public boolean onStartNestedScroll(View child, View target, int nestedScrollAxes) {
		return isEnabled() && !mReturningToStart && !mIsRefreshing
			&& (nestedScrollAxes & ViewCompat.SCROLL_AXIS_VERTICAL) != 0;
	}

	@Override
	public void onNestedScrollAccepted(View child, View target, int axes) {
		// Reset the counter of how much leftover scroll needs to be consumed.
		mNestedScrollingParentHelper.onNestedScrollAccepted(child, target, axes);
		// Dispatch up to the nested parent
		startNestedScroll(axes & ViewCompat.SCROLL_AXIS_VERTICAL);
		mTotalUnconsumed = 0;
		mNestedScrollInProgress = true;
	}

	@Override
	public void onNestedPreScroll(View target, int dx, int dy, int[] consumed) {
		// If we are in the middle of consuming, a scroll, then we want to move the spinner back up
		// before allowing the list to scroll
		if (dy > 0 && mTotalUnconsumed > 0) {
			if (dy > mTotalUnconsumed) {
				consumed[1] = (int) mTotalUnconsumed;
				mTotalUnconsumed = 0;
			} else {
				mTotalUnconsumed -= dy;
				consumed[1] = dy;
			}
			onOverscroll(mTotalUnconsumed);
		}

		// If a client layout is using a custom start position for the circle
		// view, they mean to hide it again before scrolling the child view
		// If we get back to mTotalUnconsumed == 0 and there is more to go, hide
		// the circle so it isn't exposed if its blocking content is moved
		if (mUsingCustomStart && dy > 0 && mTotalUnconsumed == 0
			&& Math.abs(dy - consumed[1]) > 0) {
			//mCircleView.setVisibility(View.GONE);
		}

		// Now let our nested parent consume the leftovers
		final int[] parentConsumed = mParentScrollConsumed;
		if (dispatchNestedPreScroll(dx - consumed[0], dy - consumed[1], parentConsumed, null)) {
			consumed[0] += parentConsumed[0];
			consumed[1] += parentConsumed[1];
		}
	}

	@Override
	public int getNestedScrollAxes() {
		return mNestedScrollingParentHelper.getNestedScrollAxes();
	}

	@Override
	public void onStopNestedScroll(View target) {
		mNestedScrollingParentHelper.onStopNestedScroll(target);
		mNestedScrollInProgress = false;
		// Finish the spinner for nested scrolling if we ever consumed any
		// unconsumed nested scroll
		if (mTotalUnconsumed > 0) {
			finishSpinner(mTotalUnconsumed);
			mTotalUnconsumed = 0;
		}
		// Dispatch up our nested parent
		stopNestedScroll();
	}

	@Override
	public void onNestedScroll(final View target, final int dxConsumed, final int dyConsumed,
	                           final int dxUnconsumed, final int dyUnconsumed) {
		onNestedScroll(target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed,
			ViewCompat.TYPE_TOUCH, mNestedScrollingV2ConsumedCompat);
	}

	@Override
	public boolean onNestedPreFling(View target, float velocityX,
	                                float velocityY) {
		return dispatchNestedPreFling(velocityX, velocityY);
	}

	@Override
	public boolean onNestedFling(View target, float velocityX, float velocityY,
	                             boolean consumed) {
		return dispatchNestedFling(velocityX, velocityY, consumed);
	}

	// NestedScrollingChild 3

	@Override
	public void dispatchNestedScroll(int dxConsumed, int dyConsumed, int dxUnconsumed,
	                                 int dyUnconsumed, @Nullable int[] offsetInWindow, @ViewCompat.NestedScrollType int type,
	                                 @NonNull int[] consumed) {
		if (type == ViewCompat.TYPE_TOUCH) {
			mNestedScrollingChildHelper.dispatchNestedScroll(dxConsumed, dyConsumed, dxUnconsumed,
				dyUnconsumed, offsetInWindow, type, consumed);
		}
	}

	// NestedScrollingChild 2

	@Override
	public boolean startNestedScroll(int axes, int type) {
		return type == ViewCompat.TYPE_TOUCH && startNestedScroll(axes);
	}

	@Override
	public void stopNestedScroll(int type) {
		if (type == ViewCompat.TYPE_TOUCH) {
			stopNestedScroll();
		}
	}

	@Override
	public boolean hasNestedScrollingParent(int type) {
		return type == ViewCompat.TYPE_TOUCH && hasNestedScrollingParent();
	}

	@Override
	public boolean dispatchNestedScroll(int dxConsumed, int dyConsumed, int dxUnconsumed,
	                                    int dyUnconsumed, int[] offsetInWindow, int type) {
		return type == ViewCompat.TYPE_TOUCH && mNestedScrollingChildHelper.dispatchNestedScroll(
			dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, offsetInWindow, type);
	}

	@Override
	public boolean dispatchNestedPreScroll(int dx, int dy, int[] consumed, int[] offsetInWindow,
	                                       int type) {
		return type == ViewCompat.TYPE_TOUCH && dispatchNestedPreScroll(dx, dy, consumed,
			offsetInWindow);
	}

	// NestedScrollingChild 1

	@Override
	public void setNestedScrollingEnabled(boolean enabled) {
		mNestedScrollingChildHelper.setNestedScrollingEnabled(enabled);
	}

	@Override
	public boolean isNestedScrollingEnabled() {
		return mNestedScrollingChildHelper.isNestedScrollingEnabled();
	}

	@Override
	public boolean startNestedScroll(int axes) {
		return mNestedScrollingChildHelper.startNestedScroll(axes);
	}

	@Override
	public void stopNestedScroll() {
		mNestedScrollingChildHelper.stopNestedScroll();
	}

	@Override
	public boolean hasNestedScrollingParent() {
		return mNestedScrollingChildHelper.hasNestedScrollingParent();
	}

	@Override
	public boolean dispatchNestedScroll(int dxConsumed, int dyConsumed, int dxUnconsumed,
	                                    int dyUnconsumed, int[] offsetInWindow) {
		return mNestedScrollingChildHelper.dispatchNestedScroll(dxConsumed, dyConsumed,
			dxUnconsumed, dyUnconsumed, offsetInWindow);
	}

	@Override
	public boolean dispatchNestedPreScroll(int dx, int dy, int[] consumed, int[] offsetInWindow) {
		return mNestedScrollingChildHelper.dispatchNestedPreScroll(
			dx, dy, consumed, offsetInWindow);
	}

	@Override
	public boolean dispatchNestedFling(float velocityX, float velocityY, boolean consumed) {
		return mNestedScrollingChildHelper.dispatchNestedFling(velocityX, velocityY, consumed);
	}

	@Override
	public boolean dispatchNestedPreFling(float velocityX, float velocityY) {
		return mNestedScrollingChildHelper.dispatchNestedPreFling(velocityX, velocityY);
	}


	protected abstract void onOverscroll(float overscroll);

	protected abstract void onPullSuccess();

	protected abstract void onPullCancel();

	private void finishSpinner(float overscrollTop) {
		if (overscrollTop > mTotalDragDistance) {
			onPullSuccess();
		} else {
			onPullCancel();
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		final int action = ev.getActionMasked();
		int pointerIndex = -1;

		if (mReturningToStart && action == MotionEvent.ACTION_DOWN) {
			mReturningToStart = false;
		}

		if (!isEnabled() || mReturningToStart || canChildScrollUp()
			|| mIsRefreshing || mNestedScrollInProgress) {
			// Fail fast if we're not in a state where a swipe is possible
			return false;
		}

		switch (action) {
			case MotionEvent.ACTION_DOWN:
				mActivePointerId = ev.getPointerId(0);
				mIsBeingDragged = false;
				break;

			case MotionEvent.ACTION_MOVE: {
				pointerIndex = ev.findPointerIndex(mActivePointerId);
				if (pointerIndex < 0) {
					Log.e(LOG_TAG, "Got ACTION_MOVE event but have an invalid active pointer id.");
					return false;
				}

				final float y = ev.getY(pointerIndex);
				startDragging(y);

				if (mIsBeingDragged) {
					final float overscrollTop = (y - mInitialMotionY);
					if (overscrollTop > 0) {
						// While the spinner is being dragged down, our parent shouldn't try
						// to intercept touch events. It will stop the drag gesture abruptly.
						getParent().requestDisallowInterceptTouchEvent(true);
						onOverscroll(overscrollTop);
					} else {
						return false;
					}
				}
				break;
			}
			case MotionEvent.ACTION_POINTER_DOWN: {
				pointerIndex = ev.getActionIndex();
				if (pointerIndex < 0) {
					Log.e(LOG_TAG,
						"Got ACTION_POINTER_DOWN event but have an invalid action index.");
					return false;
				}
				mActivePointerId = ev.getPointerId(pointerIndex);
				break;
			}

			case MotionEvent.ACTION_POINTER_UP:
				onSecondaryPointerUp(ev);
				break;

			case MotionEvent.ACTION_UP: {
				pointerIndex = ev.findPointerIndex(mActivePointerId);
				if (pointerIndex < 0) {
					Log.e(LOG_TAG, "Got ACTION_UP event but don't have an active pointer id.");
					return false;
				}

				if (mIsBeingDragged) {
					final float y = ev.getY(pointerIndex);
					final float overscrollTop = (y - mInitialMotionY);
					mIsBeingDragged = false;
					finishSpinner(overscrollTop);
				}
				mActivePointerId = INVALID_POINTER;
				return false;
			}
			case MotionEvent.ACTION_CANCEL:
				return false;
		}

		return true;
	}

	private void startDragging(float y) {
		final float yDiff = y - mInitialDownY;
		if (yDiff > mTouchSlop && !mIsBeingDragged) {
			mInitialMotionY = mInitialDownY + mTouchSlop;
			mIsBeingDragged = true;
			//mProgress.setAlpha(STARTING_PROGRESS_ALPHA);
		}
	}

	private void onSecondaryPointerUp(MotionEvent ev) {
		final int pointerIndex = ev.getActionIndex();
		final int pointerId = ev.getPointerId(pointerIndex);
		if (pointerId == mActivePointerId) {
			// This was our active pointer going up. Choose a new
			// active pointer and adjust accordingly.
			final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
			mActivePointerId = ev.getPointerId(newPointerIndex);
		}
	}

	/**
	 * Classes that wish to override {@link SwipeRefreshLayoutFixed#canChildScrollUp()} method
	 * behavior should implement this interface.
	 */
	public interface OnChildScrollUpCallback {
		/**
		 * Callback that will be called when {@link SwipeRefreshLayoutFixed#canChildScrollUp()} method
		 * is called to allow the implementer to override its behavior.
		 *
		 * @param parent SwipeRefreshLayout that this callback is overriding.
		 * @param child The child view of SwipeRefreshLayout.
		 *
		 * @return Whether it is possible for the child view of parent layout to scroll up.
		 */
		boolean canChildScrollUp(@NonNull SwipeRefreshLayoutFixed parent, @Nullable View child);
	}

}
