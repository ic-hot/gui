package ic.android.ui.view.web.ext


import android.webkit.WebView

import ic.base.strings.ext.isNotBlank
import ic.util.log.logI
import ic.util.mimetype.MimeType
import ic.util.text.charset.Charset


fun WebView.loadPage (

	pageHtml : String,

	toPreventScroll : Boolean = false,

	innerStyleCss : String = "",

	fontResFileName : String? = null

) {

	var modifiedHtml : String = pageHtml

	var style : String = ""

	if (toPreventScroll) style += (
		"body {" +
			"overflow: hidden;" +
		"}"
	)

	style += innerStyleCss

	if (fontResFileName != null) style += (
		"@font-face {" +
			"font-family: 'res_font_${ fontResFileName.replace('.', '_') }';" +
			"src: url('font/$fontResFileName');" +
		"}" +
		"* {" +
			"font-family: 'res_font_${ fontResFileName.replace('.', '_') }' !important;" +
		"}"
	)

	if (style.isNotBlank) {
		if (modifiedHtml.contains("<html>", ignoreCase = true)) {
			if (modifiedHtml.contains("<style>", ignoreCase = true)) {
				modifiedHtml = modifiedHtml.replace(
					"<style>", "<style>$style", ignoreCase = true
				)
			} else {
				modifiedHtml = modifiedHtml.replace(
					"<html>", "<html><style>$style</style>", ignoreCase = true
				)
			}
		} else {
			modifiedHtml = "<html><style>$style</style><body>$pageHtml</body></html>"
		}
	}

	logI("WebView") { "loadPage modifiedHtml: $modifiedHtml" }

	loadDataWithBaseURL(
		if (fontResFileName == null) null else "file:///android_res/",
		modifiedHtml,
		MimeType.Html.name,
		Charset.defaultHttp.jvmName,
		null
	)

}