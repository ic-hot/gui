package ic.android.ui.view.web.ext


import android.webkit.WebView


fun WebView.runJavascript (script: String) {

	loadUrl(

		"javascript:(function() { $script })()"

	)

}