package ic.android.ui.view.web.ext


import android.webkit.CookieManager
import android.webkit.WebView


fun WebView.clearCookies() {

	val cookieManager = CookieManager.getInstance()

	@Suppress("DEPRECATION")
	cookieManager.removeAllCookie()

}