package ic.android.ui.view.material


import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.GradientDrawable.RECTANGLE
import android.util.AttributeSet
import android.widget.FrameLayout

import ic.base.R
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.int64.ext.asInt32

import ic.graphics.color.ColorArgb

import ic.gui.dim.px.Px


@Deprecated("To remove")
open class CardView : FrameLayout {


	init {
		clipToPadding = true
	}


	private var radii : FloatArray? = null

	private var paddedRadii : FloatArray? = null

	private var outlineThicknessPx : Px = 0


	fun setMaterialParameters (
		topLeftCornerRadiusPx     : Px,
		topRightCornerRadiusPx    : Px,
		bottomLeftCornerRadiusPx  : Px,
		bottomRightCornerRadiusPx : Px,
		colorArgb : ColorArgb,
		outlineThicknessPx : Px = 0,
		outlineColorArgb : ColorArgb = ColorArgb(0x00000000),
		outlineDashLengthPx : Px = 0
	) {

		this.outlineThicknessPx = outlineThicknessPx

		if (
			topLeftCornerRadiusPx == 0 &&
			topRightCornerRadiusPx == 0 &&
			bottomLeftCornerRadiusPx == 0 &&
			bottomRightCornerRadiusPx == 0 &&
			outlineThicknessPx == 0
		) {

			radii = null
			paddedRadii = null

		} else {

			radii = floatArrayOf(
				topLeftCornerRadiusPx.asFloat32,     topLeftCornerRadiusPx.asFloat32,
				topRightCornerRadiusPx.asFloat32,    topRightCornerRadiusPx.asFloat32,
				bottomRightCornerRadiusPx.asFloat32, bottomRightCornerRadiusPx.asFloat32,
				bottomLeftCornerRadiusPx.asFloat32,  bottomLeftCornerRadiusPx.asFloat32
			)

			paddedRadii = floatArrayOf(
				(topLeftCornerRadiusPx     - outlineThicknessPx).asFloat32,
				(topLeftCornerRadiusPx     - outlineThicknessPx).asFloat32,
				(topRightCornerRadiusPx    - outlineThicknessPx).asFloat32,
				(topRightCornerRadiusPx    - outlineThicknessPx).asFloat32,
				(bottomRightCornerRadiusPx - outlineThicknessPx).asFloat32,
				(bottomRightCornerRadiusPx - outlineThicknessPx).asFloat32,
				(bottomLeftCornerRadiusPx  - outlineThicknessPx).asFloat32,
				(bottomLeftCornerRadiusPx  - outlineThicknessPx).asFloat32
			)

		}

		background = GradientDrawable().apply {
			shape = RECTANGLE
			cornerRadii = radii
			setColor(colorArgb)
			if (outlineThicknessPx > 0) {
				if (outlineDashLengthPx > 0) {
					setStroke(
						outlineThicknessPx,
						outlineColorArgb,
						outlineDashLengthPx.asFloat32,
						outlineDashLengthPx.asFloat32
					)
				} else {
					setStroke(outlineThicknessPx, outlineColorArgb)
				}
			}
		}

		setPadding(
			outlineThicknessPx, outlineThicknessPx, outlineThicknessPx, outlineThicknessPx
		)

	}


	override fun dispatchDraw (canvas: Canvas) {
		val paddedRadii = this.paddedRadii ?: return super.dispatchDraw(canvas)
		val clipPath = Path()
		clipPath.addRoundRect(
			RectF(
				canvas.clipBounds.apply {
					inset(outlineThicknessPx, outlineThicknessPx)
				}
			),
			paddedRadii,
			Path.Direction.CW
		)
		canvas.clipPath(clipPath)
		super.dispatchDraw(canvas)
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	{

		if (attrs == null) return

		val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.MaterialView)

		val cornersRadiusPx = styledAttributes.getDimensionPixelSize(R.styleable.MaterialView_cornersRadius, 0)

		setMaterialParameters(
			topLeftCornerRadiusPx = cornersRadiusPx, topRightCornerRadiusPx = cornersRadiusPx,
			bottomLeftCornerRadiusPx = cornersRadiusPx, bottomRightCornerRadiusPx = cornersRadiusPx,
			colorArgb = styledAttributes.getColor(R.styleable.MaterialView_color, 0xffffffff.asInt32),
			outlineThicknessPx = styledAttributes.getDimensionPixelSize(R.styleable.MaterialView_outlineThickness, 0),
			outlineColorArgb = styledAttributes.getColor(R.styleable.MaterialView_outlineColor, 0),
			outlineDashLengthPx = styledAttributes.getDimensionPixelSize(R.styleable.MaterialView_outlineDashLength, 0)
		)

		styledAttributes.recycle()

	}


}