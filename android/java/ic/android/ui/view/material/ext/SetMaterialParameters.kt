package ic.android.ui.view.material.ext


import ic.graphics.color.ColorArgb

import ic.android.ui.view.material.CardView
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.inPx


@Suppress("NOTHING_TO_INLINE")
inline fun CardView.setMaterialParameters (

	cornersRadius : Dp,

	color : ColorArgb

) {

	val cornersRadiusInPx = cornersRadius.inPx

	setMaterialParameters(
		topLeftCornerRadiusPx = cornersRadiusInPx,
		topRightCornerRadiusPx = cornersRadiusInPx,
		bottomLeftCornerRadiusPx = cornersRadiusInPx,
		bottomRightCornerRadiusPx = cornersRadiusInPx,
		colorArgb = color
	)

}