package ic.android.ui.view.funs


import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT

import ic.base.primitives.int32.Int32

import ic.gui.dim.px.ByContainer
import ic.gui.dim.px.ByContent
import ic.gui.dim.px.Px


fun layoutParamToPx (layoutParam: Int32) : Px {

	return when (layoutParam) {

		MATCH_PARENT -> ByContainer

		WRAP_CONTENT -> ByContent

		else -> layoutParam

	}

}