package ic.android.ui.view.funs


import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT

import ic.base.primitives.int32.Int32

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.px.ext.inDp


@Deprecated("Do not use")
fun layoutParamToDp (layoutParam: Int32) : Dp {

	return when (layoutParam) {

		MATCH_PARENT -> ByContainer

		WRAP_CONTENT -> ByContent

		else -> layoutParam.inDp

	}

}