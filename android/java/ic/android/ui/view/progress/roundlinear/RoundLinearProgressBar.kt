package ic.android.ui.view.progress.roundlinear


import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup

import ic.android.ui.view.ext.measuredWidthPx
import ic.base.R
import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.int32.Int32
import ic.android.ui.view.ext.setBackgroundRoundCorners
import ic.base.primitives.int32.ext.asFloat32
import ic.math.ext.clamp


open class RoundLinearProgressBar : ViewGroup {


	private var remainingColorField : Int = Int32(0x80ffffff)

	private var doneColorField : Int = Int32(0xffffffff)

	private var progressField : Float32 = Float32(0)


	private var progressView = View(context)

	init {
		@Suppress("LeakingThis")
		addView(progressView)
	}


	override fun onMeasure (widthMeasureSpec: Int, heightMeasureSpec: Int) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec)
		progressView.measure(
			MeasureSpec.makeMeasureSpec(
				(MeasureSpec.getSize(widthMeasureSpec).asFloat32 * progressField).asInt32,
				MeasureSpec.EXACTLY
			),
			MeasureSpec.makeMeasureSpec(
				MeasureSpec.getSize(heightMeasureSpec),
				MeasureSpec.EXACTLY
			)
		)
	}


	override fun onLayout (changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
		val heightPx = b - t
		progressView.layout(0, 0, progressView.measuredWidthPx, heightPx)
	}


	var remainingColorArgb : Int
		get() = remainingColorField
		set(value) {
			remainingColorField = value
			setBackgroundRoundCorners(
				color = value,
				cornersRadius = Float32(64)
			)
		}
	;

	var doneColorArgb : Int
		get() = doneColorField
		set(value) {
			doneColorField = value
			progressView.setBackgroundRoundCorners(
				color = value,
				cornersRadius = Float32(64)
			)
		}
	;

	var progress : Float32
		get() = progressField
		set(value) {
			progressField = value.clamp()
			requestLayout()
		}
	;


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr) {

		if (attrs != null) {
			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.RoundLinearProgressBar)
			remainingColorArgb 	= styledAttributes.getColor(R.styleable.RoundLinearProgressBar_remainingColor, remainingColorField)
			doneColorArgb 		= styledAttributes.getColor(R.styleable.RoundLinearProgressBar_doneColor, doneColorField)
			progressField	= styledAttributes.getFloat(R.styleable.RoundLinearProgressBar_progress, progressField)
			styledAttributes.recycle()
		}

	}


}