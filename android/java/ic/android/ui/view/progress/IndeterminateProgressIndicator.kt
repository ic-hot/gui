package ic.android.ui.view.progress


import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet

import ic.graphics.color.ColorArgb

import ic.gui.R


@Deprecated("Probably move to some legacy library or remove entirely")
open class IndeterminateProgressIndicator : android.widget.ProgressBar {


	var colorArgb : ColorArgb = ColorArgb(0x00000000)
		set(value) {
			field = value
			indeterminateTintList = ColorStateList.valueOf(colorArgb)
		}
	;


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null) : super(context, attrs) {
		if (attrs == null) return
		val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.IndeterminateProgressIndicator)
		colorArgb = styledAttributes.getColor(R.styleable.IndeterminateProgressIndicator_color, colorArgb)
		styledAttributes.recycle()
	}


}