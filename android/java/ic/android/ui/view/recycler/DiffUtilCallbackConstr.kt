package ic.android.ui.view.recycler


import ic.struct.list.List


@Suppress("FunctionName")
inline fun <Item> DiffUtilCallback (

	crossinline getOldList : () -> List<Item>,

	crossinline getNewList : () -> List<Item>

) : DiffUtilCallback<Item> {

	return object : DiffUtilCallback<Item>() {

		override val oldList get() = getOldList()

		override val newList get() = getNewList()

	}

}