package ic.android.ui.view.recycler


import androidx.recyclerview.widget.DiffUtil

import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.ext.asInt32
import ic.ifaces.id.HasInt64Id
import ic.struct.list.List


abstract class DiffUtilCallback<Item> : DiffUtil.Callback() {


	protected abstract val oldList : List<Item>

	protected abstract val newList : List<Item>


	override fun getOldListSize() : Int32 {
		return oldList.length.asInt32
	}

	override fun getNewListSize() : Int32 {
		return newList.length.asInt32
	}


	override fun areItemsTheSame (oldItemPosition: Int32, newItemPosition: Int32) : Boolean {
		val oldItem = oldList[oldItemPosition]
		val newItem = newList[newItemPosition]
		if (oldItem is HasInt64Id && newItem is HasInt64Id) {
			return oldItem.id == newItem.id
		} else {
			return oldItem == newItem
		}
	}


	override fun areContentsTheSame (oldItemPosition: Int32, newItemPosition: Int32) : Boolean {
		val oldItem = oldList[oldItemPosition]
		val newItem = newList[newItemPosition]
		return oldItem == newItem
	}


	override fun getChangePayload (oldItemPosition: Int, newItemPosition: Int) : Any? {
		return Unit
	}


}