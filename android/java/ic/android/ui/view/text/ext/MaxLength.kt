package ic.android.ui.view.text.ext


import android.text.InputFilter
import android.widget.EditText

import ic.base.arrays.Array
import ic.base.primitives.int32.Int32


inline var EditText.maxLength : Int32

	get() {
		throw NotImplementedError()
	}

	set(value) {
		if (value == Int32.MAX_VALUE) {
			filters = Array()
		} else {
			filters = Array(
				InputFilter.LengthFilter(value)
			)
		}
	}

;