package ic.android.ui.view.text.ext


import ic.android.storage.res.getResString
import ic.base.primitives.int32.Int32
import ic.ifaces.action.Action
import ic.struct.list.List
import ic.util.spannable.generateSpannableStringWithLinks

import android.text.method.LinkMovementMethod
import android.widget.TextView


fun TextView.setValueFromResource (

	resId : Int32,

	linkStartMark : String = "$(linkStart)",
	linkEndMark   : String = "$(linkEnd)",

	linkColorArgb : Int32,

	linksActions : List<Action>

) {

	text = generateSpannableStringWithLinks(
		markedString = getResString(resId),
		linkStartMark = linkStartMark,
		linkEndMark = linkEndMark,
		linkColorArgb = linkColorArgb,
		linksActions = linksActions
	)

	movementMethod = LinkMovementMethod.getInstance()

}