package ic.android.ui.view.text.ext


import android.widget.TextView

import ic.gui.font.AndroidFont
import ic.gui.font.Font


inline var TextView.font : Font?

	get() = throw NotImplementedError()

	set(value) {
		if (value == null) {
			typeface = null
		} else {
			value as AndroidFont
			setTypeface(
				value.androidTypeface,
				value.modifier
			)
		}
	}

;