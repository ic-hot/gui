@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.text.ext


import ic.base.primitives.int32.Int32

import android.widget.TextView

import ic.android.storage.res.getResFontOrNull


inline fun TextView.setFontFromResource (fontResId: Int32) {
	typeface = getResFontOrNull(fontResId)
}