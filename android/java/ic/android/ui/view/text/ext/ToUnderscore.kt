package ic.android.ui.view.text.ext


import android.graphics.Paint
import android.text.TextUtils
import android.widget.TextView


inline var TextView.toUnderscore : Boolean

	get() {
		throw NotImplementedError()
	}

	set(value) {
		if (value) {
			paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
		}
	}

;