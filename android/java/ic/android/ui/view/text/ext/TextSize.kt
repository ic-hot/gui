package ic.android.ui.view.text.ext


import ic.android.util.units.pxToDp

import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32
import ic.base.primitives.float32.Float32

import android.util.TypedValue
import android.widget.TextView


inline var TextView.textSizePx : Int32

	get() = textSize.asInt32

	set(value) {
		setTextSize(TypedValue.COMPLEX_UNIT_PX, value.asFloat32)
	}

;


inline var TextView.textSizeSp : Float32

	get() = pxToDp(textSize)

	set(value) {
		setTextSize(TypedValue.COMPLEX_UNIT_SP, value)
	}

;