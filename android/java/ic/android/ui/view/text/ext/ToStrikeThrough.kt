package ic.android.ui.view.text.ext


import android.graphics.Paint
import android.widget.TextView


inline var TextView.toStrikeThrough : Boolean

	get() {
		throw NotImplementedError()
	}

	set(value) {
		if (value) {
			paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
		} else {
			paintFlags = paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
		}
	}

;