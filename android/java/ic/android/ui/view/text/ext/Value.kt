package ic.android.ui.view.text.ext


import android.widget.TextView


inline var TextView.value : String
	get() = text.toString()
	set(value) {
		setText(value)
	}
;