@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.text.ext


import android.text.Html
import android.widget.TextView


inline fun TextView.setHtml (html: String) {

	@Suppress("DEPRECATION")
	setText(
		Html.fromHtml(html)
	)

}