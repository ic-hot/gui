package ic.android.ui.view.text.ext


import android.text.TextUtils
import android.widget.TextView


inline var TextView.toEllipsize : Boolean

	get() {
		throw NotImplementedError()
	}

	set(value) {
		if (value) {
			ellipsize = TextUtils.TruncateAt.END
		}
	}

;