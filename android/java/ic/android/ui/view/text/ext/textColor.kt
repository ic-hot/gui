package ic.android.ui.view.text.ext


import ic.base.primitives.int32.Int32

import android.widget.TextView


inline var TextView.textColorArgb : Int32

	get() = currentTextColor

	set(value) { setTextColor(value) }

;