package ic.android.ui.view.text.ext


import android.view.Gravity
import android.widget.TextView


import ic.gui.align.HorizontalAlign
import ic.gui.align.Left
import ic.gui.align.Right


inline var TextView.textHorizontalAlign : HorizontalAlign

	get() = throw NotImplementedError()

	set(value) {
		gravity = when (value) {
			Left  -> Gravity.LEFT
			Right -> Gravity.RIGHT
			else  -> Gravity.CENTER_HORIZONTAL
		}
	}

;