@file:Suppress("NOTHING_TO_INLINE")


package ic.android.ui.view.text.ext


import android.widget.TextView

import ic.android.storage.assets.getFontFromAssetsOrNull


inline fun TextView.setFontFromAssets (fontName: String) {
	typeface = getFontFromAssetsOrNull(fontName)
}