package ic.android.ui.view.text.ext


import android.widget.TextView

import ic.base.primitives.int32.Int32


@Deprecated("To remove")
inline var TextView.maxLinesCount : Int32

	get() {
		return maxLines
	}

	set(value) {
		isSingleLine = value == 1
		minLines = 1
		maxLines = value
	}

;