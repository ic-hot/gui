package ic.android.ui.activity


import android.app.Activity
import android.os.Bundle

import ic.android.ui.activity.ext.extras
import ic.android.ui.activity.ext.height
import ic.android.ui.activity.ext.insets.bottomInsetDp
import ic.android.ui.activity.ext.insets.topInsetDp
import ic.android.ui.activity.ext.keyboard.isKeyboardShown
import ic.android.ui.activity.ext.listenBackButtonBlocking
import ic.android.ui.activity.ext.thisActivity
import ic.android.ui.activity.ext.width
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.editable.ext.removeOne

import ic.gui.dim.dp.ByContainer
import ic.gui.vc.ViewController
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.context.AndroidViewContext
import ic.gui.scope.ViewScope
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.proxy.ProxyViewScope
import ic.gui.view.View
import ic.gui.window.scope.WindowScope


abstract class ViewControllerActivity : FullScreenActivity(), WindowScope, ProxyViewScope {


	protected abstract val viewController : ViewController

	protected open fun readDataFromExtras (extras: Bundle) {}

	protected open fun readDataFromStateBundle (stateBundle: Bundle) {}

	protected open fun writeDataToStateBundle (stateBundle: Bundle) {}


	private val viewContext = AndroidViewContext(
		androidContext = this,
		getRootMaxWidth  = { width  },
		getRootMaxHeight = { height },
		afterUpdateView = { afterUpdateView() }
	)

	override val sourceViewScope : ViewScope get() = viewContext

	final override var topDecorHeight    : Dp = 0.dp; private set
	final override var bottomDecorHeight : Dp = 0.dp; private set

	final override var windowWidth  : Dp = 0.dp; private set
	final override var windowHeight : Dp = 0.dp; private set

	override val isKeyboardShown get() = (this as Activity).isKeyboardShown


	protected open fun beforeCreate (stateBundle: Bundle?) {}

	val overlayViews = EditableList<View>()

	private var view : View? = null

	final override fun onCreate (stateBundle: Bundle?) {
		super.onCreate(stateBundle)
		beforeCreate(stateBundle = stateBundle)
		readDataFromExtras(extras)
		if (stateBundle != null) {
			readDataFromStateBundle(stateBundle)
		}
		view = Stack(
			width = ByContainer, height = ByContainer,
			children = List(
				viewController.open(viewScope = viewContext),
				Stack(
					width = ByContainer, height = ByContainer,
					children = overlayViews
				)
			)
		)
		viewContext.open(view!!)
		setContentView(view!!.impl as android.view.View)
		viewController.updateAllViews()
	}

	final override fun onInsetsChanged() {
		topDecorHeight    = topInsetDp
		bottomDecorHeight = bottomInsetDp
		viewController.updateAllViews()
	}

	override fun onSizeChanged() {
		viewController.updateAllViews()
	}


	protected open fun afterUpdateView() {}


	override fun saveStateToBundle (stateBundle: Bundle) {
		super.saveStateToBundle(stateBundle)
		writeDataToStateBundle(stateBundle)
	}


	override fun addOverlayView (view: View) {
		overlayViews.add(view)
	}

	override fun removeOverlayView (view: View) {
		overlayViews.removeOne { it === view }
	}


	override fun listenBackButtonBlocking (action: () -> Unit) = run {
		thisActivity.listenBackButtonBlocking(action)
	}


	protected open fun afterDestroy() {}

	final override fun onDestroy() {
		super.onDestroy()
		view = null
		viewController.close()
		viewContext.close()
		afterDestroy()
	}


}