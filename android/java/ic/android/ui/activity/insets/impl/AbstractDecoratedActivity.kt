package ic.android.ui.activity.insets.impl


import ic.android.ui.activity.insets.AbstractDecoratedActivity


internal inline val DecoratedActivityImpl.abstractDecoratedActivity : AbstractDecoratedActivity
	get() {
	return activity as AbstractDecoratedActivity
}