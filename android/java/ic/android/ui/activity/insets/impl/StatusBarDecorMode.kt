package ic.android.ui.activity.insets.impl


import ic.android.ui.activity.insets.ActivityDecorMode


var DecoratedActivityImpl.statusBarDecorMode : ActivityDecorMode
	get() = statusBarDecorModeField
	set(value) {
		statusBarDecorModeField = value
		updateDecorAndInsets()
	}
;