package ic.android.ui.activity.insets.impl


import android.view.View

import ic.android.ui.view.group.ext.getOrNull


val DecoratedActivityImpl.contentViewOrNull : View? get() = containerView.getOrNull(0)