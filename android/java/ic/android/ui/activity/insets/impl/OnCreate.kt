package ic.android.ui.activity.insets.impl


import android.view.*
import android.widget.FrameLayout

import ic.android.ui.activity.ext.insets.listenInsets


internal fun DecoratedActivityImpl.onCreate() {

	val abstractDecoratedActivity = abstractDecoratedActivity

	containerView = FrameLayout(activity)

	statusBarView 		= View(activity)
	navigationBarView 	= View(activity)

	overlayContainerView = FrameLayout(activity).apply {
		clipToPadding = false
		clipChildren = false
		elevation = 16384F
	}

	abstractDecoratedActivity.setContentViewIgnoringDecor(
		FrameLayout(activity).apply {
			layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
			addView(
				containerView,
				FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
			)
			addView(
				statusBarView,
				FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0).apply { gravity = Gravity.TOP }
			)
			addView(
				navigationBarView,
				FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0).apply { gravity = Gravity.BOTTOM }
			)
			addView(
				overlayContainerView,
				FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
			)
		}
	)

	statusBarDecorModeField 	= abstractDecoratedActivity.initStatusBarDecorMode()
	navigationBarDecorModeField = abstractDecoratedActivity.initNavigationBarDecorMode()

	activity.listenInsets {
		updateDecorAndInsets()
	}

}