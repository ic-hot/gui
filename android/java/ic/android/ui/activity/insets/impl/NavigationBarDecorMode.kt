package ic.android.ui.activity.insets.impl


import ic.android.ui.activity.insets.ActivityDecorMode


var DecoratedActivityImpl.navigationBarDecorMode : ActivityDecorMode
	get() = navigationBarDecorModeField
	set(value) {
		navigationBarDecorModeField = value
		updateDecorAndInsets()
	}
;