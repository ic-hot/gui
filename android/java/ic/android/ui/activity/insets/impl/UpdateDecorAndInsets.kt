package ic.android.ui.activity.insets.impl


import ic.android.ui.activity.ext.insets.setDecorSettings


@Suppress("DEPRECATION")
fun DecoratedActivityImpl.updateDecorAndInsets() {

	activity.setDecorSettings(
		isStatusBarLight     = statusBarDecorModeField.isLight,
		isNavigationBarLight = navigationBarDecorModeField.isLight
	)

	applyDecorModeToBarView(statusBarView, 		statusBarDecorModeField		)
	applyDecorModeToBarView(navigationBarView, 	navigationBarDecorModeField	)

	updateInsets()

}
