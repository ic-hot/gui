package ic.android.ui.activity.insets.impl


import ic.android.ui.activity.insets.ActivityDecorMode
import ic.android.ui.activity.ext.insets.bottomInsetPx
import ic.android.ui.activity.ext.insets.topInsetPx
import ic.android.ui.view.ext.layoutHeightPx


fun DecoratedActivityImpl.updateInsets() {

	if (statusBarDecorModeField is ActivityDecorMode.Solid) {
		containerView.setPadding(0, activity.topInsetPx, 0, activity.bottomInsetPx)
	} else {
		containerView.setPadding(0, 0, 0, 0)
	}

	statusBarView.layoutHeightPx = activity.topInsetPx

	navigationBarView.layoutHeightPx = activity.bottomInsetPx

}
