package ic.android.ui.activity.insets.impl


import ic.base.primitives.int32.Int32

import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout

import ic.android.ui.view.group.ext.inflateChild


fun DecoratedActivityImpl.setContentView (view: View) {

	containerView.removeAllViews()
	containerView.addView(view, FrameLayout.LayoutParams(
		ViewGroup.LayoutParams.MATCH_PARENT,
		ViewGroup.LayoutParams.MATCH_PARENT
	))

}


fun DecoratedActivityImpl.setContentView (layoutResId: Int32) {

	setContentView(
		containerView.inflateChild(layoutResId)
	)

}