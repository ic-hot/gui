package ic.android.ui.activity.insets.impl


import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout

import ic.android.ui.activity.insets.ActivityDecorMode


class DecoratedActivityImpl (internal val activity: Activity) {

	lateinit var containerView : ViewGroup

	lateinit var statusBarView 	: View
	lateinit var navigationBarView : View

	lateinit var overlayContainerView : FrameLayout

	lateinit var statusBarDecorModeField 		: ActivityDecorMode
	lateinit var navigationBarDecorModeField 	: ActivityDecorMode

}