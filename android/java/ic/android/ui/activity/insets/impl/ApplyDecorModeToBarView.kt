package ic.android.ui.activity.insets.impl


import android.view.View

import ic.android.ui.activity.insets.ActivityDecorMode
import ic.base.primitives.int32.ext.asFloat32


internal fun applyDecorModeToBarView (barView: View, decorMode: ActivityDecorMode) {

	if (decorMode is ActivityDecorMode.Solid) {

		barView.setBackgroundColor(decorMode.color)
		barView.elevation = decorMode.elevationPx.asFloat32

	} else {

		barView.background = null
		barView.elevation = 0F

	}

}