package ic.android.ui.activity.insets


import android.content.Context
import android.util.AttributeSet
import android.view.ViewTreeObserver
import android.widget.FrameLayout

import ic.android.ui.activity.ext.insets.listenInsets
import ic.android.ui.view.ext.activity
import ic.android.ui.view.ext.bottomInsetPx
import ic.android.ui.view.ext.topInsetPx
import ic.ifaces.cancelable.Cancelable


class FittingToDecorView : FrameLayout {


	private var listenInsetsTask : Cancelable? = null

	private fun updatePadding() {
		setPadding(
			0,
			topInsetPx,
			0,
			bottomInsetPx
		)
	}

	private val onGlobalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
		updatePadding()
	}

	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		listenInsetsTask = activity.listenInsets {
			updatePadding()
		}
		viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayoutListener)
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		listenInsetsTask!!.cancel()
		listenInsetsTask = null
		viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener)
	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super(context, attrs, defStyleAttr)
	;


}