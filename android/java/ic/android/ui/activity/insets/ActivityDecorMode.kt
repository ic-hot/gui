package ic.android.ui.activity.insets


import ic.android.graphics.util.isColorLight
import ic.base.primitives.int32.Int32


sealed class ActivityDecorMode (val isLight: Boolean) {


	class Solid (

		val color : Int,

		isLight : Boolean = isColorLight(color),

		val elevationPx : Int32 = 0

	) : ActivityDecorMode(isLight)


	class Transparent (isLight: Boolean) : ActivityDecorMode(isLight)


	object Hidden : ActivityDecorMode(false)


}
