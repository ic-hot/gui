package ic.android.ui.activity.insets


import android.os.Bundle
import android.view.*

import ic.android.ui.activity.FullScreenActivity
import ic.android.ui.activity.insets.impl.*


@Deprecated("Do not use")
abstract class DecoratedActivity : FullScreenActivity(), AbstractDecoratedActivity {


	@Suppress("LeakingThis")
	override val decorImpl = DecoratedActivityImpl(this)


	override fun onCreate (stateBundle: Bundle?) {
		super<FullScreenActivity>.onCreate(stateBundle)
		super<AbstractDecoratedActivity>.onCreate(stateBundle)
	}

	override val contentViewOrNull get() = super<AbstractDecoratedActivity>.contentViewOrNull
	override val containerView     get() = super<AbstractDecoratedActivity>.containerView

	override fun setContentViewIgnoringDecor (view: View) = super<FullScreenActivity>.setContentView(view)

	override fun setContentView (view: View) = super<AbstractDecoratedActivity>.setContentView(view)

	override fun setContentView (layoutResId: Int) = super<AbstractDecoratedActivity>.setContentView(layoutResId)


}