package ic.android.ui.activity.insets


import android.os.Bundle
import android.view.View
import android.view.ViewGroup

import ic.android.ui.activity.AbstractActivity
import ic.android.ui.activity.insets.impl.*
import ic.android.ui.activity.insets.impl.onCreate


interface AbstractDecoratedActivity : AbstractActivity {


	fun initStatusBarDecorMode() 		: ActivityDecorMode
	fun initNavigationBarDecorMode() 	: ActivityDecorMode


	val decorImpl : DecoratedActivityImpl


	override fun onCreate (stateBundle: Bundle?) {
		decorImpl.onCreate()
	}


	override val contentViewOrNull : View? get() = decorImpl.contentViewOrNull

	override val containerView : ViewGroup get() = decorImpl.containerView

	// Set content view:

	fun setContentViewIgnoringDecor (view: View)

	override fun setContentView (view: View) = decorImpl.setContentView(view)

	override fun setContentView (layoutResId: Int) = decorImpl.setContentView(layoutResId)


	var statusBarDecorMode : ActivityDecorMode
		get() = decorImpl.statusBarDecorMode
		set(value) { decorImpl.statusBarDecorMode = value }
	;

	var navigationBarDecorMode : ActivityDecorMode
		get() = decorImpl.navigationBarDecorMode
		set(value) { decorImpl.navigationBarDecorMode = value }
	;


}