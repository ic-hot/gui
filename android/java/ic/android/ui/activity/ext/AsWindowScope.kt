package ic.android.ui.activity.ext


import android.app.Activity

import ic.android.ui.activity.ext.insets.bottomInsetPx
import ic.android.ui.activity.ext.insets.topInsetPx
import ic.android.ui.activity.ext.keyboard.isKeyboardShown
import ic.android.ui.activity.ext.nav.external.openImageViewer
import ic.android.ui.activity.ext.overlays.addOverlayView
import ic.ifaces.cancelable.Cancelable
import ic.struct.list.editable.EditableList
import ic.struct.list.editable.ext.removeOne

import ic.graphics.image.Image
import ic.graphics.image.ext.asAndroidBitmap

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.px.ext.inDp
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.context.AndroidViewContext
import ic.gui.view.View
import ic.gui.window.scope.WindowScope


val Activity.asWindowScope : WindowScope get() {

	val viewContext = AndroidViewContext(
		androidContext = thisActivity,
		getRootMaxWidth  = { thisActivity.widthPx.inDp  },
		getRootMaxHeight = { thisActivity.heightPx.inDp }
	)

	val overlayViews = EditableList<View>()

	val view = Stack(
		width = ByContainer, height = ByContainer,
		children = overlayViews
	)

	viewContext.open(view)

	thisActivity.addOverlayView(view.impl as android.view.View)

	return object : WindowScope {

		override val sourceViewScope = viewContext

		override val topDecorHeight    get() = thisActivity.topInsetPx.inDp
		override val bottomDecorHeight get() = thisActivity.bottomInsetPx.inDp

		override val windowWidth  get() = thisActivity.widthPx.inDp
		override val windowHeight get() = thisActivity.heightPx.inDp

		override val isKeyboardShown get() = thisActivity.isKeyboardShown

		override fun openImageViewer (image: Image) {
			thisActivity.openImageViewer(image.asAndroidBitmap)
		}

		override fun openImageViewer (imageUrl: String) {
			thisActivity.openImageViewer(imageUrl)
		}

		override fun addOverlayView (view: View) {
			overlayViews.add(view)
		}

		override fun removeOverlayView (view: View) {
			overlayViews.removeOne { it === view }
		}

		override fun listenBackButtonBlocking (action: () -> Unit): Cancelable {
			return thisActivity.listenBackButtonBlocking(action)
		}

		private var isClosed : Boolean = false

		override val isOpen get() = !isClosed

	}

}