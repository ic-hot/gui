package ic.android.ui.activity.ext


import android.app.Activity

import ic.gui.dim.px.ext.inDp


val Activity.width get() = widthPx.inDp

val Activity.height get() = heightPx.inDp