package ic.android.ui.activity.ext.overlays


import ic.android.ui.activity.ext.containerView
import ic.ifaces.cancelable.Cancelable

import android.app.Activity
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout


fun Activity.addOverlayView (
	view : View,
	layoutParams : FrameLayout.LayoutParams = FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
) : Cancelable {
	containerView.addView(view, layoutParams)
	return object : Cancelable {
		override fun cancel() {
			removeOverlayView(view)
		}
	}
}


fun Activity.removeOverlayView (view: View) {
	containerView.removeView(view)
}