package ic.android.ui.activity.ext


import android.app.Activity
import android.widget.FrameLayout


internal inline val Activity.containerView : FrameLayout get() {

	return findViewById(android.R.id.content)

}