package ic.android.ui.activity.ext.permissions


import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.app.Activity

import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.copyConvertToArray

import ic.gui.permissions.Permission
import ic.gui.permissions.Permission.*


@SuppressLint("InlinedApi")
fun Activity.requestPermission (

	permissions : Collection<Permission>

) {

	requestPermission(
		*permissions.copyConvertToArray { permission ->
			when (permission) {
				Location      -> ACCESS_FINE_LOCATION
				Camera        -> CAMERA
				Notifications -> POST_NOTIFICATIONS
			}
		}
	)

}