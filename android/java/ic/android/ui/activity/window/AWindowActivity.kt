package ic.android.ui.activity.window


import android.Manifest
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_GET_CONTENT
import android.content.IntentFilter
import android.content.pm.ActivityInfo.*
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle

import ic.android.graphics.bitmap.ext.asImage
import ic.android.ui.activity.FullScreenActivity
import ic.android.ui.activity.ext.activity
import ic.android.ui.activity.ext.extras
import ic.android.ui.activity.ext.height
import ic.android.ui.activity.ext.insets.bottomInsetDp
import ic.android.ui.activity.ext.insets.setDecorSettings
import ic.android.ui.activity.ext.insets.topInsetDp
import ic.android.ui.activity.ext.keyboard.isKeyboardShown
import ic.android.ui.activity.ext.permissions.requestPermission
import ic.android.ui.activity.ext.width
import ic.android.util.bundle.ext.getAsByteArray
import ic.stream.input.frombytearray.ByteInputFromByteArray
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.copyConvertToArray
import ic.stream.buffer.ByteBuffer
import ic.stream.input.ByteInput
import ic.stream.input.fromis.ByteInputFromInputStream
import ic.stream.output.ByteOutput
import ic.stream.sequence.frombytearray.ByteSequenceFromByteArray
import ic.util.url.Url
import ic.util.log.logD

import ic.gui.permissions.Permission
import ic.gui.view.context.AndroidViewContext
import ic.gui.window.Window
import ic.gui.window.context.WindowContext
import ic.gui.view.context.ViewContext
import ic.gui.window.orient.ScreenOrientationConfig
import ic.gui.window.serial.WindowSerializer
import ic.util.log.StackTrace


abstract class AWindowActivity : FullScreenActivity(), WindowContext {


	protected abstract val windowSerializer : WindowSerializer

	protected abstract fun initDefaultWindowTypeAndStateInput() : ByteInput


	internal var preparedIcWindow : Window? = null


	override val windowWidth       get() = activity.width
	override val windowHeight      get() = activity.height

	override val topDecorHeight    get() = activity.topInsetDp
	override val bottomDecorHeight get() = activity.bottomInsetDp

	override val isKeyboardShown get() = activity.isKeyboardShown


	private var isTopDecorLight    : Boolean? = null
	private var isBottomDecorLight : Boolean? = null
	private var screenOrientationConfig : ScreenOrientationConfig? = null

	override val viewContext : ViewContext = AndroidViewContext(
		androidContext = this,
		getRootMaxWidth  = { activity.width  },
		getRootMaxHeight = { activity.height },
		afterUpdateView = {
			val icWindow = this.preparedIcWindow!!
			val isTopDecorLight    = icWindow.isTopDecorLight
			val isBottomDecorLight = icWindow.isBottomDecorLight
			if (
				isTopDecorLight    != this.isTopDecorLight ||
				isBottomDecorLight != this.isBottomDecorLight
			) {
				this.isTopDecorLight    = isTopDecorLight
				this.isBottomDecorLight = isBottomDecorLight
				setDecorSettings(
					isStatusBarLight     = isTopDecorLight,
					isNavigationBarLight = isBottomDecorLight
				)
			}
			val screenOrientationConfig = icWindow.screenOrientationConfig
			if (screenOrientationConfig != this.screenOrientationConfig) {
				this.screenOrientationConfig = screenOrientationConfig
				requestedOrientation = when (screenOrientationConfig) {
					ScreenOrientationConfig.All       -> SCREEN_ORIENTATION_FULL_USER
					ScreenOrientationConfig.Portrait  -> SCREEN_ORIENTATION_PORTRAIT
					ScreenOrientationConfig.Landscape -> SCREEN_ORIENTATION_LANDSCAPE
				}
			}
		}
	)


	@SuppressLint("UnspecifiedRegisterReceiverFlag")
	override fun onCreate (stateBundle: Bundle?) {
		super.onCreate(stateBundle)
		val input = run {
			run {
				stateBundle?.getByteArray("icWindowTypeAndState") ?:
				extras.getByteArray("icWindowTypeAndState")
			}?.let { ByteInputFromByteArray(it) } ?:
			initDefaultWindowTypeAndStateInput()
		}
		val icWindow = input.run {
			windowSerializer.run {
				readWindow()
			}
		}.also { this.preparedIcWindow = it }
		val view = icWindow.notifyOpen(
			context = this,
			stateInput = input
		)
		setContentView(view.impl as android.view.View)
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
			registerReceiver(
				broadcastReceiver,
				IntentFilter("ic.gui.broadcast")
			)
		} else {
			registerReceiver(
				broadcastReceiver,
				IntentFilter("ic.gui.broadcast"), RECEIVER_NOT_EXPORTED
			)
		}
	}


	override fun onResume() {
		super.onResume()
		val url = run {
			val intent = this.intent
			val dataString = intent.dataString
			if (dataString == null) {
				null
			} else {
				this.intent = Intent(intent).apply { data = null }
				Url.parseOrNull(dataString)
			}
		}
		url?.let { preparedIcWindow?.notifyUrl(it) }
	}


	override fun onKeyboardShown() {
		preparedIcWindow?.notifyKeyboardShown()
	}

	override fun onKeyboardHidden() {
		preparedIcWindow?.notifyKeyboardHidden()
	}

	override fun onSizeChanged() {
		logD("gui") { "onSizeChanged $windowWidth, $windowHeight : $topInsetDp $bottomInsetDp" }
		viewContext.updateAllViews()
	}

	override fun onInsetsChanged() {
		logD("gui") { "onInsetsChanged $windowWidth, $windowHeight : $topInsetDp $bottomInsetDp" }
		viewContext.updateAllViews()
	}


	private val broadcastReceiver = object : BroadcastReceiver() {
		override fun onReceive (context: Context, intent: Intent) {
			preparedIcWindow!!.notifyBroadcast(
				ByteInputFromByteArray(
					intent.extras!!.getAsByteArray("data")
				)
			)
		}
	}


	object RequestCode {
		val IcWindowResult = 4096
		val SelectImage    = 4097
		val SelectFile     = 4098
	}

	override fun openImageSelector (titleText: String) {
		val intent = Intent(ACTION_GET_CONTENT).apply { type = "image/*" }
		startActivityForResult(intent, RequestCode.SelectImage)
	}

	override fun openFileSelector() {
		val getContentIntent = Intent().apply { type = "*/*"; action = ACTION_GET_CONTENT }
		val chooserIntent = Intent.createChooser(getContentIntent, null)
		startActivityForResult(chooserIntent, RequestCode.SelectFile)
	}

	final override fun onActivityResult (requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data) // TODO
		when (requestCode) {
			RequestCode.IcWindowResult -> {
				data ?: return
				val windowTypeAndResultByteArray = data.getByteArrayExtra("icWindowTypeAndResult")!!
				preparedIcWindow!!.notifyWindowResult(
					ByteSequenceFromByteArray(windowTypeAndResultByteArray).newIterator(),
				)
			}
			RequestCode.SelectImage -> {
				if (resultCode != RESULT_OK) return
				val uri = data?.data ?: return
				try {
					val inputStream = activity.contentResolver.openInputStream(uri) ?: return
					val bitmap = BitmapFactory.decodeStream(inputStream)
					if (bitmap == null) {
						preparedIcWindow!!.notifyUnableToParseSelectedImage()
					} else {
						preparedIcWindow!!.notifyImageSelected(image = bitmap.asImage)
					}
				} catch (_: java.io.FileNotFoundException) {
					preparedIcWindow!!.notifySelectedImageNotExists()
				}
			}
			RequestCode.SelectFile -> {
				if (resultCode != RESULT_OK) return
				val uri = data?.data ?: return
				preparedIcWindow!!.notifyFileSelected(
					fileInput = ByteInputFromInputStream(
						activity.contentResolver.openInputStream(uri) ?: return
					)
				)
			}
			else -> onNonIcWindowRelatedActivityResult(requestCode, resultCode, data)
		}
	}

	protected open fun onNonIcWindowRelatedActivityResult (
		requestCode: Int, resultCode: Int, data: Intent?
	) {}


	@SuppressLint("InlinedApi")
	override fun requestPermission (permissions: Collection<Permission>) {
		requestPermission(
			*permissions.copyConvertToArray { permission ->
				when (permission) {
					Permission.Location      -> Manifest.permission.ACCESS_FINE_LOCATION
					Permission.Camera        -> Manifest.permission.CAMERA
					Permission.Notifications -> Manifest.permission.POST_NOTIFICATIONS
				}
			}
		)
	}

	override fun onPermissionDenied() {
		preparedIcWindow!!.notifyPermissionDenied()
	}

	override fun onPermissionGranted() {
		preparedIcWindow!!.notifyPermissionGranted()
	}


	override fun onBackButton() {
		preparedIcWindow?.notifyBackButton()
	}


	private lateinit var closeReason : Window.CloseReason

	override fun close() {
		closeReason = Window.CloseReason.ByCode
		finish()
	}

	override fun initResultOutput() : ByteOutput {
		return ByteBuffer(
			onClose = {
				val result = this
				val intent = Intent()
				val windowTypeAndResultBuffer = ByteBuffer()
				windowTypeAndResultBuffer.run {
					windowSerializer.run {
						writeWindow(preparedIcWindow!!)
					}
					write(result)
				}
				intent.putExtra("icWindowTypeAndResult", windowTypeAndResultBuffer.toByteArray())
				setResult(RESULT_OK, intent)
				closeReason = Window.CloseReason.ByCode
				finish()
			}
		)
	}

	internal fun getWindowTypeAndStateByteArray() : ByteArray {
		val byteBuffer = ByteBuffer()
		byteBuffer.run {
			windowSerializer.run {
				writeWindow(preparedIcWindow!!)
			}
		}
		preparedIcWindow!!.notifySave(output = byteBuffer)
		return byteBuffer.toByteArray()
	}

	override fun saveStateToBundle (stateBundle: Bundle) {
		closeReason = Window.CloseReason.BySystem
		stateBundle.putByteArray("icWindowTypeAndState", getWindowTypeAndStateByteArray())
	}

	override fun onDestroy() {
		super.onDestroy()
		unregisterReceiver(broadcastReceiver)
		preparedIcWindow!!.notifyClose(reason = closeReason)
		preparedIcWindow = null
	}


}