package ic.android.ui.activity.window


import ic.stream.input.ByteInput
import ic.stream.output.ext.Empty

import ic.gui.window.Window
import ic.gui.window.serial.single.SingleWindowSerializer


abstract class WindowActivity : AWindowActivity() {


	protected abstract val icWindow : Window

	final override val windowSerializer = SingleWindowSerializer(
		initWindow = { icWindow }
	)


	protected open fun initDefaultStateInput() = ByteInput.Empty

	final override fun initDefaultWindowTypeAndStateInput() : ByteInput {
		return initDefaultStateInput()
	}


	override fun initOpenWindowOutput() = throw RuntimeException()

	override fun externalCall (message: Any?) = throw RuntimeException()


}