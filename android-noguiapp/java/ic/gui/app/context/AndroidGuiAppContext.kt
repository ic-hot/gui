package ic.gui.app.context


import ic.stream.buffer.ByteBuffer
import ic.stream.input.ByteInput
import ic.stream.output.ByteOutput


internal object AndroidGuiAppContext : GuiAppContext {


	internal var rootWindowTypeAndStateInput : ByteInput? = null


	override fun initOpenWindowOutput() : ByteOutput {
		return ByteBuffer(
			onClose = {
				rootWindowTypeAndStateInput = this.newIterator()
			}
		)
	}


	override val isOpen get() = true


	override fun externalCall (message: Any?) : Any? {
		return null
	}


}