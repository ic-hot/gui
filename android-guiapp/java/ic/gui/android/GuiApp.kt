package ic.gui.android


import android.app.Application
import android.content.Context

import ic.android.app.thisAppOrNull
import ic.app.setRuntimeApp
import ic.base.reflect.getClassByName
import ic.base.reflect.instantiate
import ic.base.reflect.getClassByNameOrNull

import ic.gui.app.GuiApp
import ic.gui.app.mainGuiApp
import ic.gui.app.external.ApplicationExternalInterface


private var isGuiAppInitialized : Boolean = false


fun Context.initGuiAppIfNeeded() {

	if (isGuiAppInitialized) return
	isGuiAppInitialized = true

	mainGuiApp = getClassByName<GuiApp>("ic.app.Main").instantiate()

	setRuntimeApp(mainGuiApp)

	val application = applicationContext as Application

	thisAppOrNull = application

	val externalInterfaceClassName = "ic.app.external.MainApplicationExternalInterface"

	ApplicationExternalInterface.main = (
		getClassByNameOrNull<ApplicationExternalInterface>(externalInterfaceClassName)
		?.instantiate()
		?.also {
			it.init(androidApplication = application)
		}
	)

	mainGuiApp.run()

}


fun Context.callGuiApp (message: Any?) {
	initGuiAppIfNeeded()
	mainGuiApp.notifyExternalCall(message)
}