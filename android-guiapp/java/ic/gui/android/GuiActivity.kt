package ic.gui.android


import android.content.Intent
import android.os.Bundle

import ic.android.ui.activity.ext.thisActivity
import ic.android.ui.activity.window.AWindowActivity
import ic.base.reflect.getClassByNameOrNull
import ic.base.reflect.instantiate
import ic.stream.buffer.ByteBuffer
import ic.stream.output.ByteOutput

import ic.gui.app.context.AndroidGuiAppContext.rootWindowTypeAndStateByteSequence
import ic.gui.app.external.ActivityExternalInterface
import ic.gui.app.context.impl.globalWindowSerializer


class GuiActivity : AWindowActivity() {


	override fun onCreate (stateBundle: Bundle?) {
		initGuiAppIfNeeded()
		GuiApplication.isAlreadyOpened = true
		externalInterface?.notifyCreate()
		super.onCreate(stateBundle)
	}

	override val windowSerializer get() = globalWindowSerializer

	override fun initDefaultWindowTypeAndStateInput() = run {
		rootWindowTypeAndStateByteSequence!!.newIterator()
	}


	override fun initOpenWindowOutput() : ByteOutput {
		return ByteBuffer(
			onClose = {
				val intent = Intent(thisActivity, GuiActivity::class.java)
				intent.putExtra("icWindowTypeAndState", this.toByteArray())
				startActivityForResult(intent, RequestCode.IcWindowResult)
			}
		)
	}


	// External:

	private var isExternalInterfaceInitialized : Boolean = false

	private var externalInterfaceField : ActivityExternalInterface? = null

	private val externalInterface : ActivityExternalInterface? get() {
		if (!isExternalInterfaceInitialized) {
			isExternalInterfaceInitialized = true
			val className = "ic.app.external.MainActivityExternalInterface"
			val externalInterface = (
				getClassByNameOrNull<ActivityExternalInterface>(className)?.instantiate()
			)
			if (externalInterface != null) {
				externalInterface.init(
					activity = this,
					onCallback = { message ->
						preparedIcWindow?.notifyExternalCall(message)
					}
				)
			}
			this.externalInterfaceField = externalInterface
		}
		return this.externalInterfaceField
	}

	override fun externalCall (message: Any?) : Any? {
		return externalInterface?.notifyCall(message)
	}

	override fun onNonIcWindowRelatedActivityResult (
		requestCode : Int, resultCode : Int, data : Intent?
	) {
		externalInterface?.notifyOnActivityResult(
			requestCode = requestCode,
			resultCode = resultCode,
			data = data
		)
	}


}