package ic.gui.app.context


import ic.stream.buffer.ByteBuffer
import ic.stream.sequence.ByteSequence
import ic.stream.output.ByteOutput

import ic.gui.app.external.ApplicationExternalInterface


internal object AndroidGuiAppContext : GuiAppContext {


	internal var rootWindowTypeAndStateByteSequence : ByteSequence? = null


	override fun initOpenWindowOutput() : ByteOutput {
		return ByteBuffer(
			onClose = { rootWindowTypeAndStateByteSequence = this }
		)
	}


	override val isOpen get() = true


	override fun externalCall (message: Any?) : Any? {
		return ApplicationExternalInterface.main!!.notifyCall(message)
	}


}