package ic.gui.app.external


import android.app.Activity
import android.content.Intent


abstract class ActivityExternalInterface {


	protected lateinit var activity : Activity; private set

	private lateinit var onCallback : (Any?) -> Unit


	internal fun init (
		activity : Activity,
		onCallback : (Any?) -> Unit
	) {
		this.activity = activity
		this.onCallback = onCallback
	}


	protected open fun onCreate() {}

	internal fun notifyCreate() {
		onCreate()
	}


	protected abstract fun onCall (message: Any?) : Any?

	internal fun notifyCall (message: Any?) : Any? {
		return onCall(message)
	}


	protected open fun onActivityResult (requestCode: Int, resultCode: Int, data: Intent?) {}

	internal fun notifyOnActivityResult (requestCode: Int, resultCode: Int, data: Intent?) {
		onActivityResult(requestCode = requestCode, resultCode = resultCode, data = data)
	}


	protected fun call (message: Any?) {
		onCallback(message)
	}


}