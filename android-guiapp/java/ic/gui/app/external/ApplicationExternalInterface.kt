package ic.gui.app.external


import kotlin.reflect.KClass

import ic.gui.app.mainGuiApp

import android.app.Application
import ic.android.app.setAndroidBuildConfigClass


abstract class ApplicationExternalInterface {


	protected open fun onInit() {}



	protected abstract val androidBuildConfigClass : KClass<*>

	protected lateinit var application : Application; private set


	internal fun init (androidApplication: Application) {
		setAndroidBuildConfigClass(androidBuildConfigClass)
		this.application = androidApplication
		onInit()
	}


	protected open fun onCall (message: Any?) : Any? = throw RuntimeException()

	internal fun notifyCall (message: Any?) : Any? {
		return onCall(message)
	}


	protected fun call (message: Any?) {
		mainGuiApp.notifyExternalCall(message)
	}


	companion object {

		var main : ApplicationExternalInterface? = null; internal set

	}


}