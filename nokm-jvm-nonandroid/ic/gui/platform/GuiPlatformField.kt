package ic.gui.platform


import ic.gui.platfrom.NonAndroidJvmGuiPlatform


internal inline val guiPlatform : GuiPlatform get() = NonAndroidJvmGuiPlatform