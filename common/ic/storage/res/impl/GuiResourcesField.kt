package ic.storage.res.impl


import ic.gui.impl.guiPlatform


internal inline val guiResources : GuiResources get() = guiPlatform.resources