package ic.storage.res.impl


import ic.gui.font.Font


internal interface GuiResources {


	fun getDefaultFont() : Font

	fun getDefaultBoldFont() : Font

	fun getDefaultMonospaceFont() : Font


	fun getFontOrNull (path: String, fontName: String) : Font?


}