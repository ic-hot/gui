package ic.storage.res


import ic.storage.res.impl.guiResources
import ic.struct.map.editable.EditableMap
import ic.struct.value.Val

import ic.gui.font.Font


private val cache = EditableMap<String, Val<Font?>>()

fun getResFontOrNull (path: String, fontName: String) : Font? {
	val v = (
		cache[fontName]
		?: Val(guiResources.getFontOrNull(path = path, fontName = fontName))
		.also { cache[fontName] = it }
	)
	return v.valueOrNull
}


fun getResFont (path: String, fontName: String) = getResFontOrNull(path = path, fontName = fontName)!!