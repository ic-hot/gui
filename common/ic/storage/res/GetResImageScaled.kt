package ic.storage.res


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt64
import ic.util.cache.RamCache

import ic.graphics.image.Image

import ic.gui.dim.dp.screenDensityFactor


private data class Key (
	val name : String,
	val nativeDensity : Float32
)


private val cache = RamCache<Key, Image>()


fun getResImage (name: String, nativeDensityFactor: Float32) : Image {
	return cache.getOr(
		Key(name = name, nativeDensity = nativeDensityFactor)
	) {
		val image = getResImage(name)
		val multiplier = screenDensityFactor / nativeDensityFactor
		image.copyResize(
			width  = (image.width  * multiplier).asInt64,
			height = (image.height * multiplier).asInt64
		)
	}
}