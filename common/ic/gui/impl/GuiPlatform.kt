package ic.gui.impl


import ic.base.primitives.float32.Float32
import ic.stream.output.ByteOutput

import ic.storage.res.impl.GuiResources
import ic.gui.app.context.GuiAppContext


internal interface GuiPlatform {

	val resources : GuiResources

	val guiAppContext : GuiAppContext

	fun getScreenDensityFactor() : Float32

	fun openBroadcastOutput() : ByteOutput

}