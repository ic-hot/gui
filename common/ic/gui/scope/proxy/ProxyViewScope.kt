package ic.gui.scope.proxy


import ic.base.primitives.float32.Float32
import ic.util.geo.Location

import ic.graphics.image.Image

import ic.gui.scope.ViewScope


interface ProxyViewScope : ViewScope {


	val sourceViewScope : ViewScope?


	override fun updateAllViews (ifAlreadyUpdating: ViewScope.IfAlreadyUpdating) {
		sourceViewScope?.updateAllViews(ifAlreadyUpdating = ifAlreadyUpdating)
	}


	override fun showKeyboard() {
		sourceViewScope!!.showKeyboard()
	}

	override fun hideKeyboard() {
		sourceViewScope!!.hideKeyboard()
	}

	override fun copyToClipboard (text: String) {
		sourceViewScope!!.copyToClipboard(text)
	}

	override fun getStringFromClipboard() : String? {
		return sourceViewScope!!.getStringFromClipboard()
	}

	override fun goToMapsNavigator (destination: Location) {
		sourceViewScope!!.goToMapsNavigator(destination)
	}

	override fun openUrl (url: String) {
		sourceViewScope!!.openUrl(url)
	}

	override fun openImageViewer (image: Image) {
		sourceViewScope!!.openImageViewer(image)
	}

	override fun openImageViewer (imageUrl: String) {
		sourceViewScope!!.openImageViewer(imageUrl)
	}

	override fun openPhoneDialer (phoneNumber: String) {
		sourceViewScope!!.openPhoneDialer(phoneNumber)
	}

	override fun openEmailSender (email: String) {
		sourceViewScope!!.openEmailSender(email)
	}

	override fun setAppLanguage (languageCode: String?) {
		sourceViewScope!!.setAppLanguage(languageCode)
	}

	override fun setScreenBrightness (brightness: Float32) {
		sourceViewScope!!.setScreenBrightness(brightness)
	}


}