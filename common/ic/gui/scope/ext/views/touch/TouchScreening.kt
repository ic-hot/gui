package ic.gui.scope.ext.views.touch


import ic.gui.align.Center
import ic.gui.dim.dp.ByContainer
import ic.gui.touch.screen.ScreeningTouchHandler
import ic.gui.view.container.empty.EmptyView


fun TouchScreening (

	debugName : String? = null,

	touchDebugName : String? = null

) : EmptyView {

	return object : EmptyView() {

		override val topPriorityTouchHandler = ScreeningTouchHandler

		override val layoutWidth  get() = ByContainer
		override val layoutHeight get() = ByContainer

		override val layoutWeight get() = 1F

		override val layoutHorizontalAlign get() = Center
		override val layoutVerticalAlign   get() = Center

		override val debugName get() = debugName

		override val touchDebugName get() = touchDebugName

	}

}