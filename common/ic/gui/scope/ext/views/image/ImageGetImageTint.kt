package ic.gui.scope.ext.views.image


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color
import ic.graphics.image.Image

import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.view.simple.image.ImageView
import ic.gui.view.simple.image.ScaleMode


inline fun Image (

	width : Dp, height : Dp,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign 	: VerticalAlign   = Center,
	opacity : Float32 = Float32(1),

	scaleMode : ScaleMode = ScaleMode.Fit,

	crossinline getImage : () -> Image?,

	crossinline getTint : () -> Color?,

	toUseAntialiasing : Boolean = true

) : ImageView {

	return object : ImageView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity

		override val image get() = getImage()

		override val tint get() = getTint()

		override val scaleMode get() = scaleMode

		override val toUseAntialiasing get() = toUseAntialiasing

	}

}