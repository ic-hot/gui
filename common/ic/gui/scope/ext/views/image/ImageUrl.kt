package ic.gui.scope.ext.views.image


import ic.base.primitives.float32.Float32
import ic.design.task.scope.ext.doInBackground
import ic.design.task.scope.ext.doInUiThread
import ic.util.log.logW

import ic.graphics.color.Color
import ic.graphics.image.Image
import ic.graphics.image.fromurl.getImageFromUrlCacheFirst
import ic.graphics.util.ResizeMode

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.view.ext.update
import ic.gui.view.simple.image.ImageView
import ic.gui.view.simple.image.ScaleMode


fun Image (

	width : Dp, height : Dp,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign 	: VerticalAlign   = Center,
	opacity : Float32 = 1F,

	scaleMode : ScaleMode = ScaleMode.Fit,
	imageUrl : String?,
	resizeMode : ResizeMode = ResizeMode.LeaveAsIs,
	tint : Color? = null,
	toUseAntialiasing : Boolean = true

) : ImageView {

	return object : ImageView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity

		override val tint get() = tint

		override val scaleMode get() = scaleMode

		override val toUseAntialiasing get() = toUseAntialiasing

		override var image : Image? = null

		private var isLoaded : Boolean = false

		override fun onOpen() {
			if (imageUrl != null) {
				doInBackground {
					try {
						val loadedImage = getImageFromUrlCacheFirst(
							urlString = imageUrl,
							resizeMode = resizeMode
						)
						doInUiThread {
							image = loadedImage
							isLoaded = true
							if (isSizeImmutable) {
								update()
							} else {
								updateAllViews()
							}
						}
					} catch (e: Exception) {
						logW("Uncaught") { e }
						doInUiThread {
							isLoaded = true
						}
					}
				}
			}
		}

		private val isSizeImmutable get() = (
			when (width) {
				ByContent   -> isLoaded
				ByContainer -> false
				else        -> true
			} &&
			when (height) {
				ByContent   -> isLoaded
				ByContainer -> false
				else        -> true
			}
		)

	}

}