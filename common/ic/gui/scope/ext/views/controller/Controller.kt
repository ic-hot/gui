package ic.gui.scope.ext.views.controller


import ic.gui.vc.ViewController
import ic.gui.view.View
import ic.gui.view.container.decor.SizePreservingDecoratorView


@Suppress("FunctionName")
fun Controller (

	controller : ViewController,

	debugName : String? = null

) : SizePreservingDecoratorView {

	return object : SizePreservingDecoratorView() {

		override val layoutWidth  get() = view.layoutWidth
		override val layoutHeight get() = view.layoutHeight

		override val layoutWeight get() = view.layoutWeight

		override val layoutHorizontalAlign get() = view.layoutHorizontalAlign
		override val layoutVerticalAlign   get() = view.layoutVerticalAlign

		override val opacity get() = 1F

		lateinit var view : View

		override val subview get() = view

		override fun onOpenContainer() {
			view = controller.open(viewScope = scope)
		}

		override fun onCloseContainer() {
			controller.close()
		}

		override val debugName get() = debugName

	}

}