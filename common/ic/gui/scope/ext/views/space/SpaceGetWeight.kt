package ic.gui.scope.ext.views.space


import ic.base.primitives.float32.Float32

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.view.container.empty.EmptyView


inline fun Space (

	width : Dp = ByContainer,
	height : Dp = ByContainer,

	crossinline getWeight : () -> Float32,

	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,

	debugName : String? = null

) : EmptyView {

	return object : EmptyView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = getWeight()

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val debugName get() = debugName

	}

}