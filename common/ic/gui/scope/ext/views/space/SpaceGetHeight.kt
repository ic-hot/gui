package ic.gui.scope.ext.views.space


import ic.base.primitives.float32.Float32
import ic.graphics.color.Color

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.view.container.empty.EmptyView


inline fun Space (

	width : Dp = ByContainer,

	crossinline getHeight : () -> Dp,

	weight : Float32 = 1F,

	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,

	debugName : String? = null,
	debugColor : Color? = null

) : EmptyView {

	return object : EmptyView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = getHeight()

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val debugName get() = debugName
		override val debugColor get() = debugColor

	}

}