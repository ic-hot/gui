package ic.gui.scope.ext.views.progress


import ic.base.primitives.float32.Float32
import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.align.Left
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.View
import ic.gui.view.ext.width


fun ProgressBar (

	width : Dp = ByContainer,

	height : Dp,

	doneColor : Color,

	remainingColor : Color,

	getProgress : () -> Float32

) = run {

	lateinit var view : View

	Stack(
		width = width, height = height,
		children = List(
			Solid(remainingColor),
			Solid(
				color = doneColor,
				horizontalAlign = Left,
				getWidth = { view.width * getProgress() }
			)
		)
	).also { view = it }

}