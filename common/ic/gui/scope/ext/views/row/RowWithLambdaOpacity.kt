package ic.gui.scope.ext.views.row


import ic.base.primitives.float32.Float32
import ic.struct.list.List

import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.view.View
import ic.gui.view.container.group.row.RowView


@Suppress("FunctionName")
inline fun Row (

	width : Dp, height : Dp,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,
	crossinline getOpacity : () -> Float32,

	children : List<View>

) : RowView {

	return object : RowView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = getOpacity()

		override val children get() = children

	}

}