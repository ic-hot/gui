package ic.gui.scope.ext.views.padding


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.View
import ic.gui.view.container.decor.padding.PaddingView


inline fun Padding (

	weight : Float32 = 1F,

	translationY : Dp = 0.dp,

	left   : Dp = 0.dp,
	right  : Dp = 0.dp,
	top    : Dp = 0.dp,
	bottom : Dp = 0.dp,
	horizontal : Dp = 0.dp,
	vertical   : Dp = 0.dp,
	padding : Dp = 0.dp,

	child : View,

	debugName : String? = null,
	debugColor : Color? = null,

	touchDebugName : String? = null

) : PaddingView {

	return object : PaddingView() {

		override val translationY get() = translationY

		override val leftPadding   get() = left   + horizontal + padding
		override val rightPadding  get() = right  + horizontal + padding
		override val topPadding    get() = top    + vertical   + padding
		override val bottomPadding get() = bottom + vertical   + padding

		override val subview get() = child

		override val layoutWeight get() = weight

		override val debugName  get() = debugName
		override val debugColor get() = debugColor

		override val touchDebugName get() = touchDebugName

	}

}