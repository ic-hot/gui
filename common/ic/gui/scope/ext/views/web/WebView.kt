package ic.gui.scope.ext.views.web


import ic.base.escape.skip.Skippable
import ic.base.escape.skip.skip
import ic.base.primitives.float32.Float32

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.view.simple.web.WebView


inline fun WebView (

	width : Dp = ByContainer, height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,
	opacity : Float32 = 1F,

	innerStyleCss : String = "",

	javascriptToRun : String? = null,

	toEnableJavaScript : Boolean = true,

	toUseMobileSiteVersion : Boolean = true,

	toOpenLinksInBrowser : Boolean = false,

	crossinline onRedirectOrSkip : (url: String) -> Unit = { skip },

	pageUrl : String? = null,
	pageHtml : String? = null,

	debugName : String? = null

) : WebView {

	return object : WebView() {

		override val innerStyleCss get() = innerStyleCss

		override val pageUrl get() = pageUrl

		override val pageHtml get() = pageHtml

		override val toEnableJavaScript get() = toEnableJavaScript

		override val toUseMobileSiteVersion get() = toUseMobileSiteVersion

		override val javascriptToRun get() = javascriptToRun

		override val toOpenLinksInBrowser get() = toOpenLinksInBrowser

		@Skippable()
		override fun onRedirectOrSkip (url: String) {
			onRedirectOrSkip(url)
		}

		override val layoutWidth  get() = width
		override val layoutHeight get() = height
		override val layoutWeight get() = weight
		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign
		override val opacity get() = opacity

		override val debugName get() = debugName

	}

}