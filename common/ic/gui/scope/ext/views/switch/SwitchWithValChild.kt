package ic.gui.scope.ext.views.switch


import ic.struct.value.Val

import ic.graphics.color.Color

import ic.gui.view.View
import ic.gui.view.container.decor.SizePreservingDecoratorView


fun Switch (

	child : Val<View>,

	debugName : String? = null,
	debugColor : Color? = null,

	touchDebugName : String? = null,

) : SizePreservingDecoratorView {

	return Switch(
		getChild = { child.value },
		debugName = debugName,
		debugColor = debugColor,
		touchDebugName = touchDebugName
	)

}