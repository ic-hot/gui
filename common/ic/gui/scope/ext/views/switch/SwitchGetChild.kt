package ic.gui.scope.ext.views.switch


import ic.graphics.color.Color

import ic.gui.view.View
import ic.gui.view.container.decor.SizePreservingDecoratorView


inline fun Switch (

	debugName : String? = null,
	debugColor : Color? = null,

	touchDebugName : String? = null,

	crossinline getChild : () -> View

) : SizePreservingDecoratorView {

	return object : SizePreservingDecoratorView() {

		override val layoutWidth  get() = getChild().layoutWidth
		override val layoutHeight get() = getChild().layoutHeight

		override val layoutWeight get() = getChild().layoutWeight

		override val layoutHorizontalAlign get() = getChild().layoutHorizontalAlign
		override val layoutVerticalAlign   get() = getChild().layoutVerticalAlign

		override val z get() = getChild().z

		override val opacity get() = 1F

		override val subview get() = getChild()

		override val debugName  get() = debugName
		override val debugColor get() = debugColor

		override val touchDebugName get() = touchDebugName

	}

}