package ic.gui.scope.ext.views.aspectratio


import ic.base.primitives.float32.Float32

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.Dp
import ic.gui.view.View
import ic.gui.view.container.decor.aspect.AspectRatioView


fun AspectRatio (

	width : Dp, height : Dp,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign 	: VerticalAlign   = Center,
	opacity : Float32 = 1F,

	aspectRatio : Float32,
	child : View

) : AspectRatioView {

	return object : AspectRatioView() {

		override val aspectRatio get() = aspectRatio

		override val subview get() = child

		override val layoutWidth  get() = width
		override val layoutHeight get() = height
		override val layoutWeight get() = weight
		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign
		override val opacity get() = opacity

	}

}