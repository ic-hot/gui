package ic.gui.scope.ext.views.datepicker


import ic.base.primitives.float32.Float32
import ic.util.time.Time

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.view.simple.datepicker.DatePickerView


inline fun DatePicker (

	width : Dp = ByContent, height: Dp = ByContent,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign 	: VerticalAlign   = Center,
	opacity : Float32 = 1F,

	style : DatePickerView.Style? = null,

	crossinline getValue : () -> Time?,
	crossinline onValueChanged : (Time?) -> Unit

) : DatePickerView {

	return object : DatePickerView(style = style) {

		override val value get() = getValue()

		override fun onValueChanged (value: Time?) {
			onValueChanged(value)
		}

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity

	}

}