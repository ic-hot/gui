package ic.gui.scope.ext.views.canvas


import ic.base.primitives.float32.Float32

import ic.graphics.canvas.Canvas

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.view.simple.canvas.CanvasView


fun Canvas (

	width  : Dp = ByContainer,
	height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign : VerticalAlign = Center,
	opacity : Float32 = 1F,

	toRedrawContinuously : Boolean,
	onDraw : Canvas.() -> Unit

) : CanvasView {

	return object : CanvasView() {

		override val toRedrawContinuously get() = toRedrawContinuously

		override fun Canvas.onDraw() {
			onDraw()
		}

		override val layoutWidth  get() = width
		override val layoutHeight get() = height
		override val layoutWeight get() = weight
		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign
		override val opacity get() = opacity

	}

}