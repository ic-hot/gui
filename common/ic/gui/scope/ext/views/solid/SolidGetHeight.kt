package ic.gui.scope.ext.views.solid


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color

import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.view.card.CardView


inline fun Solid (

	width : Dp = ByContainer,
	crossinline getHeight : () -> Dp,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center, verticalAlign : VerticalAlign = Center,
	opacity : Float32 = 1F,

	color : Color

) : CardView {

	return object : CardView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = getHeight()

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity

		override val backgroundColor get() = color

	}

}