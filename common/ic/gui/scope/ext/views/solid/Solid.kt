package ic.gui.scope.ext.views.solid


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color

import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ext.dp
import ic.gui.view.card.CardView


fun Solid (

	color : Color,

	width : Dp = ByContainer, height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center, verticalAlign : VerticalAlign = Center,
	opacity : Float32 = 1F,

	blurRadius : Dp = 0.dp,

	debugName : String? = null

) : CardView {

	return object : CardView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity

		override val backgroundColor get() = color

		override val blurRadius get() = blurRadius

		override val subview get() = null

		override val debugName get() = debugName

	}

}