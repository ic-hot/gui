package ic.gui.scope.ext.views.stack


import ic.base.arrays.ext.asList

import ic.gui.dim.dp.ByContainer
import ic.gui.view.View
import ic.gui.view.container.group.stack.StackView


@Suppress("NOTHING_TO_INLINE")
inline fun Stack (

	vararg children : View

) : StackView {

	return Stack(
		width = ByContainer, height = ByContainer,
		children = children.asList(isArrayImmutable = true)
	)

}