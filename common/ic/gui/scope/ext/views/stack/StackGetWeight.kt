package ic.gui.scope.ext.views.stack


import ic.base.primitives.float32.Float32
import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.view.View
import ic.gui.view.container.group.stack.StackView


fun Stack (

	width : Dp, height : Dp,
	getWeight : () -> Float32,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign : VerticalAlign = Center,
	opacity : Float32 = 1F,

	children : List<View>,

	debugName : String? = null,
	debugColor : Color? = null,

	touchDebugName : String? = null

) : StackView {

	return object : StackView() {

		override val children get() = children

		override val layoutWidth  get() = width
		override val layoutHeight get() = height
		override val layoutWeight get() = getWeight()
		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign
		override val opacity get() = opacity

		override val debugName  get() = debugName
		override val debugColor get() = debugColor

		override val touchDebugName get() = touchDebugName

	}

}