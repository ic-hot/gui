package ic.gui.scope.ext.views.stack


import ic.base.primitives.float32.Float32
import ic.struct.list.List

import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.view.View
import ic.gui.view.container.group.stack.StackView


inline fun Stack (

	width  : Dp = ByContainer,
	height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign 	: VerticalAlign   = Center,
	opacity : Float32 = 1F,

	crossinline getScale : () -> Float32,

	crossinline getTranslationY : () -> Dp,

	children : List<View>,

) : StackView {

	return object : StackView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height
		override val layoutWeight get() = weight
		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign
		override val opacity get() = opacity

		override val scaleX get() = getScale()
		override val scaleY get() = getScale()

		override val translationY get() = getTranslationY()

		override val children get() = children

	}

}