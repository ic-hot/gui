package ic.gui.scope.ext.views.column


import ic.base.primitives.float32.Float32
import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.view.View
import ic.gui.view.container.group.column.ColumnView


inline fun Column (

	width : Dp = ByContainer, height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,
	opacity : Float32 = 1F,

	crossinline onOpen : () -> Unit = {},

	crossinline beforeUpdate : () -> Unit = {},
	crossinline afterUpdate : () -> Unit = {},

	crossinline onClose : () -> Unit = {},

	toAnimateLayoutChanges : Boolean = false,

	children : List<View>,

	debugName : String? = null,
	debugColor : Color? = null,

	touchDebugName : String? = null

) : ColumnView {

	return object : ColumnView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity

		override fun onOpenContainer() {
			onOpen()
		}

		override fun beforeUpdateContainer() {
			beforeUpdate()
		}

		override fun afterUpdateContainer() {
			afterUpdate()
		}

		override fun onCloseContainer() {
			onClose()
		}

		override val children get() = children

		override val toAnimateLayoutChanges get() = toAnimateLayoutChanges

		override val debugName  get() = debugName
		override val debugColor get() = debugColor

		override val touchDebugName get() = touchDebugName

	}

}