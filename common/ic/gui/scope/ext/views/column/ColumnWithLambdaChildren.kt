package ic.gui.scope.ext.views.column


import ic.base.primitives.float32.Float32
import ic.struct.list.List

import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.view.View
import ic.gui.view.container.group.column.ColumnView


@Suppress("FunctionName")
inline fun Column (

	width : Dp, height : Dp,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,
	opacity : Float32 = 1F,

	crossinline getChildren : () -> List<View>

) : ColumnView {

	return object : ColumnView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity

		override val children get() = getChildren()

	}

}