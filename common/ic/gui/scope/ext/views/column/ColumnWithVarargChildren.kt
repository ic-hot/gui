package ic.gui.scope.ext.views.column


import ic.base.arrays.ext.asList

import ic.gui.dim.dp.ByContainer
import ic.gui.view.View
import ic.gui.view.container.group.column.ColumnView


inline fun Column (
	vararg children : View
) : ColumnView {

	return Column(
		width = ByContainer, height = ByContainer,
		children = children.asList(isArrayImmutable = true)
	)

}