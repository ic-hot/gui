package ic.gui.scope.ext.views.noview


import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.space.Space


@Suppress("NOTHING_TO_INLINE")
inline fun NoView() = Space(width = 0.dp, height = 0.dp)