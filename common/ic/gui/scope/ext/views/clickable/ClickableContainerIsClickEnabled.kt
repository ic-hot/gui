package ic.gui.scope.ext.views.clickable


import ic.base.primitives.float32.Float32
import ic.struct.list.List

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.View


inline fun Clickable (
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign 	: VerticalAlign   = Center,
	weight : Float32 = 1F,
	child : View,
	crossinline isClickEnabled : () -> Boolean,
	crossinline onClick : () -> Unit
) = (
	Stack(
		width = child.layoutWidth, height = child.layoutHeight,
		horizontalAlign = horizontalAlign,
		verticalAlign = verticalAlign,
		weight = weight,
		children = List(
			Clickable(
				onClick = onClick,
				isClickEnabled = isClickEnabled
			),
			child
		)
	)
)