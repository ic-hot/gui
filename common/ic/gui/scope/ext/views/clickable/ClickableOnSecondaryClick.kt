package ic.gui.scope.ext.views.clickable


import ic.base.primitives.float32.Float32

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.view.container.empty.touch.ClickableView


inline fun Clickable (

	width  : Dp = ByContainer,
	height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,

	crossinline onClick : () -> Unit,

	crossinline onSecondaryClick : () -> Unit

) : ClickableView {

	return object : ClickableView() {

		override val isClickEnabled get() = true

		override fun onClick() {
			onClick()
		}

		override val isSecondaryClickEnabled get() = true

		override fun onSecondaryClick() {
			onSecondaryClick()
		}

		override val layoutWidth  get() = width
		override val layoutHeight get() = height
		override val layoutWeight get() = weight
		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

	}

}