package ic.gui.scope.ext.views.clickable


import ic.base.primitives.float32.Float32
import ic.struct.list.List

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.View


@Suppress("FunctionName")
inline fun Clickable (
	horizontalAlign : HorizontalAlign 	= Center,
	verticalAlign 	: VerticalAlign 	= Center,
	weight : Float32 = 1F,
	crossinline onClick : () -> Unit,
	crossinline onSecondaryClick : () -> Unit,
	child : View
) : View = (
	Stack(
		width = child.layoutWidth, height = child.layoutHeight,
		horizontalAlign = horizontalAlign,
		verticalAlign = verticalAlign,
		weight = weight,
		children = List(
			Clickable(
				onClick = onClick,
				onSecondaryClick = onSecondaryClick
			),
			child
		)
	)
)