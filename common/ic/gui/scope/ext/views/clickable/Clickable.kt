package ic.gui.scope.ext.views.clickable


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.container.empty.touch.ClickableView


inline fun Clickable (

	width  : Dp = ByContainer,
	height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,

	isClickEnabled : Boolean = true,

	clickExtraRadius : Dp = 0.dp,

	debugName : String? = null,
	debugColor : Color? = null,

	touchDebugName : String? = null,

	crossinline onClick : () -> Unit

) : ClickableView {

	return object : ClickableView() {

		override val isClickEnabled get() = isClickEnabled

		override fun onClick() {
			onClick()
		}

		override val isSecondaryClickEnabled get() = false

		override fun onSecondaryClick() {}

		override val touchExtraRadius get() = clickExtraRadius

		override val layoutWidth  get() = width
		override val layoutHeight get() = height
		override val layoutWeight get() = weight
		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val debugName  get() = debugName
		override val debugColor get() = debugColor

		override val touchDebugName get() = touchDebugName

	}

}