package ic.gui.scope.ext.views.clickable


import ic.base.primitives.float32.Float32
import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.View


inline fun Clickable (

	weight : Float32 = 1F,

	child : View,

	clickExtraRadius : Dp = 0.dp,

	debugName : String? = null,
	debugColor : Color? = null,

	touchDebugName : String? = null,

	isClickEnabled : Boolean = true,
	crossinline onClick : () -> Unit

) : View = (

	Stack(
		width = child.layoutWidth, height = child.layoutHeight, // TODO Can change
		horizontalAlign = child.layoutHorizontalAlign, verticalAlign = child.layoutVerticalAlign, // TODO Can change
		weight = weight,
		children = List(
			Clickable(
				isClickEnabled = isClickEnabled,
				onClick = onClick,
				clickExtraRadius = clickExtraRadius,
				debugName = debugName,
				touchDebugName = touchDebugName
			),
			child
		),
		debugName = debugName,
		debugColor = debugColor
	)

)