package ic.gui.scope.ext.views.decor


import ic.struct.list.List

import ic.gui.dim.dp.ByContainer
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.View
import ic.gui.window.scope.WindowScope


fun WindowScope.DecorPadding (

	child : View

) = (

	Column(
		width = ByContainer, height = ByContainer,
		children = List(
			TopDecorSpace(),
			Stack(
				width = ByContainer, height = ByContainer,
				children = List(
					child
				)
			),
			BottomDecorSpace()
		)
	)

)