package ic.gui.scope.ext.views.decor


import ic.graphics.color.Color
import ic.gui.scope.ext.views.space.Space
import ic.gui.view.View
import ic.gui.window.scope.WindowScope


@Suppress("FunctionName")
fun WindowScope.TopDecorSpace (
	debugName : String? = null
) : View = (
	Space(
		getHeight = { topDecorHeight },
		debugName = debugName
	)
)


@Suppress("FunctionName")
fun WindowScope.BottomDecorSpace(
	debugName : String? = null,
	debugColor : Color? = null
) : View = (
	Space(
		getHeight = { bottomDecorHeight },
		debugName = debugName,
		debugColor = debugColor
	)
)