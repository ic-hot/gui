package ic.gui.scope.ext.views.text


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.Dp
import ic.gui.font.Font
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.Left
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.view.simple.text.TextView
import ic.gui.view.simple.text.span.Span


@Suppress("FunctionName")
inline fun Text (

	width : Dp,
	weight : Float32 = Float32(1),
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,
	opacity : Float32 = 1F,

	textHorizontalAlign : HorizontalAlign = Left,

	size : Dp,
	font : Font? = null,
	crossinline getColor : () -> Color,

	text : String,

	maxLength : Int32 = Int32.MAX_VALUE,
	maxLinesCount : Int32 = Int32.MAX_VALUE,

	lineSpacingExtra : Dp = 0.dp,
	toEllipsize : Boolean = false,
	toUnderscore : Boolean = false,
	toStrikeThrough : Boolean = false,
	spans : List<Span> = List(),

) : TextView {

	return object : TextView() {


		override val layoutWidth  get() = width
		override val layoutHeight get() = ByContent

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity


		override val textHorizontalAlign get() = textHorizontalAlign


		override val size get() = size

		override val font get() = font ?: Font.Default

		override val color get() = getColor()


		override val text get() = text


		override val maxLength get() = maxLength

		override val maxLinesCount get() = maxLinesCount


		override val lineSpacingExtra get() = lineSpacingExtra

		override val toEllipsize     get() = toEllipsize
		override val toUnderscore    get() = toUnderscore
		override val toStrikeThrough get() = toStrikeThrough

		override val spans get() = spans


		override val isSelectable get() = false

		override val input get() = null


	}

}