package ic.gui.scope.ext.views.text


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.Dp
import ic.gui.font.Font
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.Left
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.view.simple.text.TextInput
import ic.gui.view.simple.text.TextView


@Suppress("FunctionName")
inline fun Text (

	width : Dp,
	height : Dp = ByContent,
	weight : Float32 = Float32(1),
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,
	opacity : Float32 = 1F,

	translationY : Dp = 0.dp,

	textHorizontalAlign : HorizontalAlign = Left,

	size : Dp,
	font : Font? = null,
	crossinline getColor : () -> Color,

	crossinline getText : () -> String,

	crossinline onFocus : () -> Unit = {},
	crossinline onUnfocus : () -> Unit = {},

	maxLength : Int32 = Int32.MAX_VALUE,
	maxLinesCount : Int32 = Int32.MAX_VALUE,

	lineSpacingExtra : Dp = 0.dp,
	toEllipsize : Boolean = false,
	toUnderscore : Boolean = false,
	toStrikeThrough : Boolean = false,
	//spans : List<Span> = List(),

	input : TextInput?,
	isSelectable : Boolean = true,

	touchExtraRadius : Dp = 0.dp,

	debugName : String? = null,
	debugColor : Color? = null

) : TextView {

	return object : TextView() {


		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity

		override val translationY get() = translationY


		override val textHorizontalAlign get() = textHorizontalAlign


		override val size get() = size

		override val font get() = font ?: Font.Default

		override val color get() = getColor()


		override val text get() = getText()


		override val maxLength get() = maxLength

		override val maxLinesCount get() = maxLinesCount


		override val lineSpacingExtra get() = lineSpacingExtra

		override val toEllipsize     get() = toEllipsize
		override val toUnderscore    get() = toUnderscore
		override val toStrikeThrough get() = toStrikeThrough

		override val spans get() = List() // spans


		override val isSelectable get() = isSelectable

		override val input get() = input

		override fun onFocus() {
			onFocus()
		}

		override fun onUnfocus() {
			onUnfocus()
		}


		override val touchExtraRadius get() = touchExtraRadius


		override val debugName  get() = debugName
		override val debugColor get() = debugColor


	}

}