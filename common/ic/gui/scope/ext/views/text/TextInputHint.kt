package ic.gui.scope.ext.views.text


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.strings.ext.isEmpty
import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.Left
import ic.gui.align.Top
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.font.Font
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.simple.text.TextInput
import ic.gui.view.simple.text.TextView
import ic.gui.view.simple.text.span.Span


inline fun Text (

	width : Dp,
	height : Dp = ByContent,
	weight : Float32 = Float32(1),
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,
	opacity : Float32 = 1F,

	textHorizontalAlign : HorizontalAlign = Left,

	size : Dp,
	font : Font? = null,
	color : Color,

	crossinline getText : () -> String,

	maxLength : Int32 = Int32.MAX_VALUE,
	maxLinesCount : Int32 = Int32.MAX_VALUE,

	lineSpacingExtra : Dp = 0.dp,
	toEllipsize : Boolean = false,
	toUnderscore : Boolean = false,
	toStrikeThrough : Boolean = false,
	toSpellCheck : Boolean = true,
	spans : List<Span> = List(),

	touchExtraRadius : Dp = 0.dp,

	input : TextInput,
	isSelectable : Boolean = true,

	crossinline onFocus : () -> Unit = {},

	hint : String,
	hintColor : Color,

	onTextViewCreated : (TextView) -> Unit = {}

) = (

	Stack(
		width = width, height = height,
		weight = weight,
		horizontalAlign = horizontalAlign,
		verticalAlign = verticalAlign,
		opacity = opacity,
		children = List(
			Text(
				width = width,
				horizontalAlign = textHorizontalAlign,
				verticalAlign = Top,
				textHorizontalAlign = textHorizontalAlign,
				size = size,
				font = font,
				color = hintColor,
				getOpacity = { if (getText().isEmpty) 1F else 0F },
				text = hint,
				maxLength = maxLength,
				maxLinesCount = maxLinesCount,
				lineSpacingExtra = lineSpacingExtra,
				toEllipsize = toEllipsize,
				toUnderscore = toUnderscore,
				toStrikeThrough = toStrikeThrough,
				spans = spans
			),
			Text(
				width = width, height = height,
				horizontalAlign = textHorizontalAlign,
				verticalAlign = Top,
				textHorizontalAlign = textHorizontalAlign,
				size = size,
				font = font,
				color = color,
				getText = getText,
				maxLength = maxLength,
				maxLinesCount = maxLinesCount,
				lineSpacingExtra = lineSpacingExtra,
				toEllipsize = toEllipsize,
				toUnderscore = toUnderscore,
				toSpellCheck = toSpellCheck,
				toStrikeThrough = toStrikeThrough,
				spans = spans,
				input = input,
				isSelectable = isSelectable,
				onFocus = onFocus,
				touchExtraRadius = touchExtraRadius
			).also { onTextViewCreated(it) }
		)
	)

)