package ic.gui.scope.ext.views.text


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.view.View
import ic.gui.view.simple.text.TextInput
import ic.gui.view.simple.input.InputEvent
import ic.gui.view.simple.input.InputView
import ic.gui.view.simple.text.OnOkayAction


inline fun Input (

	width : Dp = ByContainer, height : Dp = ByContainer,

	horizontalAlign : HorizontalAlign = Center, verticalAlign : VerticalAlign = Center,

	weight : Float32 = 1F,

	inputMode : TextInput.Type = TextInput.Type.CommonText,

	onOkayAction : OnOkayAction = OnOkayAction.HideKeyboard,

	toFocusByDefault : Boolean = false,

	crossinline onInputEvent : (InputEvent) -> Unit,

	debugName : String? = null,
	debugColor : Color? = null

) : View {

	return object : InputView() {

		override val inputType get() = inputMode

		override val onOkayAction get() = onOkayAction

		override val toFocusByDefault get() = toFocusByDefault

		override fun onInputEvent (inputEvent: InputEvent) {
			onInputEvent(inputEvent)
		}

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val debugName  get() = debugName
		override val debugColor get() = debugColor

	}

}