package ic.gui.scope.ext.views.material


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.View
import ic.gui.view.card.CardView


inline fun Material (

	horizontalAlign : HorizontalAlign = Center, verticalAlign : VerticalAlign = Center,
	weight : Float32 = 1F,
	crossinline getOpacity : () -> Float32,

	backgroundColor : Color = Color.Transparent,

	elevation : Dp = 0.dp,

	topLeftCornerRadius : Dp = 0.dp,
	topRightCornerRadius : Dp = 0.dp,
	bottomLeftCornerRadius : Dp = 0.dp,
	bottomRightCornerRadius : Dp = 0.dp,
	leftCornersRadius : Dp = 0.dp,
	rightCornersRadius : Dp = 0.dp,
	topCornersRadius : Dp = 0.dp,
	bottomCornersRadius : Dp = 0.dp,
	cornersRadius : Dp = 0.dp,

	inset : Dp = 0.dp,

	blurRadius : Dp = 0.dp,

	shadowOpacity : Float32 = 1F,

	outlineThickness : Dp = 0.dp,
	outlineColor : Color = Color.Transparent,
	outlineDashLength : Dp = 0.dp,
	child : View

) : CardView {

	return object : CardView() {

		override val layoutWidth  get() = child.layoutWidth
		override val layoutHeight get() = child.layoutHeight

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = getOpacity()

		override val backgroundColor get() = backgroundColor

		override val elevation get() = elevation

		override val inset get() = inset

		override val blurRadius get() = blurRadius

		override val shadowOpacity get() = shadowOpacity

		override val topLeftCornerRadius get() = topLeftCornerRadius + leftCornersRadius + topCornersRadius + cornersRadius
		override val topRightCornerRadius get() = topRightCornerRadius + rightCornersRadius + topCornersRadius + cornersRadius
		override val bottomLeftCornerRadius get() = bottomLeftCornerRadius + leftCornersRadius + bottomCornersRadius + cornersRadius
		override val bottomRightCornerRadius get() = bottomRightCornerRadius + rightCornersRadius + bottomCornersRadius + cornersRadius
		override val outlineThickness get() = outlineThickness
		override val outlineColor get() = outlineColor
		override val outlineDashLength get() = outlineDashLength

		override val subview get() = child

	}

}