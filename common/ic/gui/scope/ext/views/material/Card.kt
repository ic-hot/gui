package ic.gui.scope.ext.views.material


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.View
import ic.gui.view.card.CardView


fun Material (

	child : View,
	weight : Float32 = 1F,
	opacity : Float32 = 1F,
	rotationDeg : Float32 = 0F,

	backgroundColor : Color = Color.Transparent,

	elevation : Dp = 0.dp,
	shadowOpacity : Float32 = 1F,

	topLeftCornerRadius : Dp = 0.dp,
	topRightCornerRadius : Dp = 0.dp,
	bottomLeftCornerRadius : Dp = 0.dp,
	bottomRightCornerRadius : Dp = 0.dp,
	leftCornersRadius : Dp = 0.dp,
	rightCornersRadius : Dp = 0.dp,
	topCornersRadius : Dp = 0.dp,
	bottomCornersRadius : Dp = 0.dp,
	cornersRadius : Dp = 0.dp,

	outlineThickness : Dp = 0.dp,
	outlineColor : Color = Color.White,
	outlineDashLength : Dp = 0.dp,

	inset : Dp = 0.dp,

	blurRadius : Dp = 0.dp,

	debugName : String? = null,
	debugColor : Color? = null,

	touchDebugName : String? = null

) : CardView {

	return object : CardView() {

		override val layoutWidth  get() = if (child.layoutWidth == ByContainer) ByContainer else ByContent
		override val layoutHeight get() = if (child.layoutHeight == ByContainer) ByContainer else ByContent

		override val layoutWeight get() = weight

		override val opacity get() = opacity
		override val rotationDeg get() = rotationDeg

		override val inset get() = inset

		override val blurRadius get() = blurRadius

		override val layoutHorizontalAlign get() = child.layoutHorizontalAlign
		override val layoutVerticalAlign   get() = child.layoutVerticalAlign

		override val elevation get() = elevation
		override val shadowOpacity get() = shadowOpacity

		override val topLeftCornerRadius get() = topLeftCornerRadius + leftCornersRadius + topCornersRadius + cornersRadius
		override val topRightCornerRadius get() = topRightCornerRadius + rightCornersRadius + topCornersRadius + cornersRadius
		override val bottomLeftCornerRadius get() = bottomLeftCornerRadius + leftCornersRadius + bottomCornersRadius + cornersRadius
		override val bottomRightCornerRadius get() = bottomRightCornerRadius + rightCornersRadius + bottomCornersRadius + cornersRadius
		override val outlineThickness get() = outlineThickness
		override val outlineColor get() = outlineColor
		override val outlineDashLength get() = outlineDashLength

		override val backgroundColor get() = backgroundColor

		override val subview get() = child

		override val debugName  get() = debugName
		override val debugColor get() = debugColor

		override val touchDebugName get() = touchDebugName

	}

}