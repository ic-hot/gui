package ic.gui.scope.ext.views.gradient


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color
import ic.graphics.color.ColorArgb

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.view.simple.gradient.LinearGradientView


inline fun LinearGradient (
	width : Dp = ByContainer, height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center, verticalAlign : VerticalAlign = Center,
	opacity : Float32 = 1F,
	startColor : ColorArgb, endColor: ColorArgb,
	directionX : Float32 = 1F, directionY : Float32 = 0F,
) : LinearGradientView {
	return LinearGradient(
		width = width, height = height,
		weight = weight,
		horizontalAlign = horizontalAlign, verticalAlign = verticalAlign,
		opacity = opacity,
		startColor = Color(startColor), endColor = Color(endColor),
		directionX = directionX,
		directionY = directionY
	)
}