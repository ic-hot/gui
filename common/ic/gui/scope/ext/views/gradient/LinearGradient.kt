package ic.gui.scope.ext.views.gradient


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.view.simple.gradient.LinearGradientView


inline fun LinearGradient (

	width : Dp = ByContainer, height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center, verticalAlign : VerticalAlign = Center,
	opacity : Float32 = 1F,

	startColor : Color, endColor : Color,
	directionX : Float32 = 1F, directionY : Float32 = 0F

) : LinearGradientView {

	return object : LinearGradientView() {

		override val startColor get() = startColor
		override val endColor   get() = endColor

		override val directionX get() = directionX
		override val directionY get() = directionY

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity

	}

}