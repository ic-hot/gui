package ic.gui.scope.ext.views.camera


import ic.base.primitives.float32.Float32

import ic.gui.dim.dp.Dp
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.view.simple.camera.CameraScannerView


fun CameraScannerView (

	width  : Dp = ByContainer,
	height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign : VerticalAlign = Center,
	opacity : Float32 = 1F,

	onScanned : (String) -> Unit

) : CameraScannerView {

	return object : CameraScannerView() {

		override fun onScanned (code: String) {
			onScanned(code)
		}

		override val layoutWidth  get() = width
		override val layoutHeight get() = height
		override val layoutWeight get() = weight
		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign
		override val opacity get() = opacity

	}

}