package ic.gui.scope.ext.views.hideable


import ic.gui.scope.ext.views.noview.NoView
import ic.gui.scope.ext.views.switch.Switch
import ic.gui.view.View


@Suppress("FunctionName")
inline fun Hideable (

	child : View,

	crossinline isVisible : () -> Boolean

) : View {

	var noView : View? = null

	return Switch(
		getChild = {
			if (isVisible()) {
				child
			} else {
				noView ?: NoView().also { noView = it }
			}
		}
	)

}


inline fun Hideable (
	crossinline isVisible : () -> Boolean,
	crossinline initChild : () -> View,
) : View {
	var noView : View? = null
	var child : View? = null
	return Switch(
		getChild = {
			if (isVisible()) {
				child ?: initChild().also { child = it }
			} else {
				noView ?: NoView().also { noView = it }
			}
		}
	)
}