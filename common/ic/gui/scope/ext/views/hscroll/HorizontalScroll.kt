package ic.gui.scope.ext.views.hscroll


import ic.base.primitives.float32.Float32

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.view.View
import ic.gui.view.container.decor.scroll.HorizontalScrollView


@Suppress("FunctionName")
inline fun HorizontalScroll (

	width : Dp = ByContainer, height : Dp = ByContainer,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center, verticalAlign : VerticalAlign = Center,
	opacity : Float32 = 1F,

	child : View,

	crossinline onScroll : (Dp) -> Unit = {},

	touchDebugName : String? = null

) : HorizontalScrollView {

	return object : HorizontalScrollView() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

		override val opacity get() = opacity

		override val subview get() = child

		override fun onScroll (offset: Dp) {
			onScroll(offset)
		}

		override val touchDebugName get() = touchDebugName

	}

}