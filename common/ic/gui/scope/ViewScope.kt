package ic.gui.scope


import ic.base.primitives.float32.Float32
import ic.util.geo.Location

import ic.graphics.image.Image


interface ViewScope {


	sealed class IfAlreadyUpdating {
		object Ignore      : IfAlreadyUpdating()
		object UpdateAgain : IfAlreadyUpdating()
	}

	fun updateAllViews (ifAlreadyUpdating: IfAlreadyUpdating = IfAlreadyUpdating.Ignore)


	fun showKeyboard()
	fun hideKeyboard()

	fun copyToClipboard (text: String)

	fun getStringFromClipboard() : String?

	fun goToMapsNavigator (destination: Location)

	fun openImageViewer (image: Image)

	fun openImageViewer (imageUrl: String)

	fun openPhoneDialer (phoneNumber: String)

	fun openEmailSender (email: String)

	fun openUrl (url: String)


	fun setAppLanguage (languageCode: String?)

	fun setScreenBrightness (brightness: Float32)


}