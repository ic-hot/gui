package ic.gui.vc


import ic.base.assert.assert
import ic.ifaces.lifecycle.closeable.Closeable

import ic.gui.scope.ViewScope
import ic.gui.scope.proxy.ProxyViewScope
import ic.gui.view.View


abstract class ViewController : AViewController(), ProxyViewScope, Closeable {


	var view : View? = null


	private var viewScope : ViewScope? = null

	override val sourceViewScope get() = viewScope


	fun open (viewScope: ViewScope) : View {
		assert { this.viewScope == null }
		this.viewScope = viewScope
		val view = initView()
		this.view = view
		onOpen()
		return view
	}


	final override val isOpen get() = viewScope != null


	protected open fun onClose() {}

	final override fun close() {
		cancelTasks()
		onClose()
		viewScope = null
		view = null
	}


}