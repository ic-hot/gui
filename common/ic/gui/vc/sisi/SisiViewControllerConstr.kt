package ic.gui.vc.sisi


import ic.base.throwables.NotNeededException
import ic.design.sisi.StateIntent
import ic.design.sisi.ext.invoke
import ic.ifaces.state.HasState

import ic.gui.view.View


inline fun <State, Intent> SisiViewController (
	crossinline initView : SisiViewScope<State, Intent>.() -> View,
	crossinline init : HasState<State>.() -> StateIntent<State, Intent>
		= { throw NotNeededException }
	,
	crossinline onIntent : SisiViewController<State, Intent, State>.(State, Intent) -> StateIntent<State, Intent>
) : SisiViewController<State, Intent, State> {
	return object : SisiViewController<State, Intent, State>() {
		override fun getViewState (state: State) = state
		override fun onOpen() {
			try {
				val stateIntent = init()
				invoke(stateIntent)
			} catch (_: NotNeededException) {}
		}
		override fun SisiViewScope<State, Intent>.initView() = initView(this)
		override fun onIntent (state: State, intent: Intent) = onIntent(this, state, intent)
	}
}


inline fun <State, Intent, ViewState> SisiViewController (
	crossinline getViewState : (State) -> ViewState,
	crossinline initView : SisiViewScope<ViewState, Intent>.() -> View,
	crossinline init : HasState<State>.() -> StateIntent<State, Intent>
		= { throw NotNeededException }
	,
	crossinline onIntent : SisiViewController<State, Intent, ViewState>.(State, Intent) -> StateIntent<State, Intent>
) : SisiViewController<State, Intent, ViewState> {
	return object : SisiViewController<State, Intent, ViewState>() {
		override fun getViewState (state: State) = getViewState(state)
		override fun onOpen() {
			try {
				val stateIntent = init()
				invoke(stateIntent)
			} catch (_: NotNeededException) {}
		}
		override fun SisiViewScope<ViewState, Intent>.initView() = initView(this)
		override fun onIntent (state: State, intent: Intent) = onIntent(this, state, intent)
	}
}