package ic.gui.vc.sisi


import ic.design.sisi.SisiInvokeScope

import ic.gui.scope.ViewScope
import ic.gui.scope.proxy.ProxyViewScope


inline fun <State, Intent> SisiViewScope (

	viewScope : ViewScope,

	invokeScope : SisiInvokeScope<Intent>,

	crossinline getState : () -> State

) : SisiViewScope<State, Intent> {

	return object : SisiViewScope<State, Intent>, ProxyViewScope {

		override val sourceViewScope get() = viewScope

		override val state get() = getState()

		override fun invoke (intent: Intent) {
			invokeScope.invoke(intent)
		}

	}

}