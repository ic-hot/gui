package ic.gui.vc.sisi


import ic.design.sisi.SisiInvokeScope
import ic.gui.scope.ViewScope
import ic.ifaces.state.HasState


interface SisiViewScope<State, Intent> : ViewScope, HasState<State>, SisiInvokeScope<Intent>