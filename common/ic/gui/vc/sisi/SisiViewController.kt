package ic.gui.vc.sisi


import ic.base.assert.assert
import ic.design.sisi.SisiController
import ic.ifaces.state.HasSettableState
import ic.util.log.logD

import ic.gui.vc.ViewController
import ic.gui.view.View


abstract class SisiViewController<State, Intent, ViewState>
	: ViewController()
	, SisiController<State, Intent>
	, HasSettableState<State>
{


	protected abstract fun getViewState (state: State) : ViewState


	private var isStateSet : Boolean = false

	private var sisiState : State? = null

	private var viewState : ViewState? = null


	@Suppress("UNCHECKED_CAST")
	final override var state : State
		get() {
			assert { isStateSet }
			return sisiState as State
		}
		set(value) {
			if (!isStateSet || sisiState != value) {
				sisiState = value
				viewState = getViewState(value)
				isStateSet = true
				logD("SisiViewController") { "state: $value" }
				if (isOpen) {
					updateAllViews()
				}
			}
		}
	;


	protected abstract fun SisiViewScope<ViewState, Intent>.initView() : View

	@Suppress("LeakingThis")
	private val viewScope = SisiViewScope(
		viewScope = this,
		invokeScope = this,
		getState = {
			@Suppress("UNCHECKED_CAST")
			sisiState as ViewState
		}
	)

	final override fun initView() : View {
		return viewScope.initView()
	}


}