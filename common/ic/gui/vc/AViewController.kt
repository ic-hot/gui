package ic.gui.vc


import ic.design.task.scope.BaseTaskScope

import ic.gui.view.View


abstract class AViewController : BaseTaskScope() {


	protected abstract fun initView() : View


	protected open fun onOpen() {}


}