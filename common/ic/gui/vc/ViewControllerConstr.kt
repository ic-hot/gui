package ic.gui.vc


import ic.gui.view.View


inline fun ViewController (

	crossinline initView : ViewController.() -> View

) : ViewController {

	return object : ViewController() {

		override fun initView() : View {
			return initView(this)
		}

	}

}