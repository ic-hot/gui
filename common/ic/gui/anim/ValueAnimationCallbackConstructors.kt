package ic.gui.anim


import ic.base.primitives.float32.Float32


inline fun ValueAnimationCallback (

	crossinline onStop : () -> Unit,

	crossinline onBreak 	: () -> Unit,
	crossinline onCancel	: () -> Unit,
	crossinline onComplete	: () -> Unit,

	crossinline onFrame	: (phase: Float32) -> Unit

) : ValueAnimationCallback {

	return object : ValueAnimationCallback {

		override fun onStop() = onStop()

		override fun onBreak() = onBreak()

		override fun onCancel() = onCancel()

		override fun onComplete() = onComplete()

		override fun onFrame (phase: Float32) = onFrame(phase)

	}

}