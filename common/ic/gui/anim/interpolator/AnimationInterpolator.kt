package ic.gui.anim.interpolator


import ic.base.primitives.float32.Float32


interface AnimationInterpolator {

	fun getPhase (time: Float32) : Float32

}