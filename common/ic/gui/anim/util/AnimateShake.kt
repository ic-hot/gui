package ic.gui.anim.util


import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64
import ic.design.task.Task
import ic.math.sin

import ic.gui.anim.animateValue
import ic.gui.anim.interpolator.LinearInterpolator


inline fun animateShake (

	crossinline applyTranslationDp : (Float32) -> Unit

) : Task? {

	return animateValue(
		durationMs = Int64(1024),
		interpolator = LinearInterpolator,
		onStop = {},
		onBreak = {},
		onCancel = {},
		onComplete = {
			applyTranslationDp(Float32(0))
		},
		onFrame = { phase ->
			applyTranslationDp(
				sin(phase * 64) * 16 * (
					if (phase < 1f / 8) {
						phase * 8
					} else {
						(1F - phase) * 8 / 7
					}
				)
			)
		}
	)

}