package ic.gui.anim


import ic.base.primitives.float32.Float32


interface ValueAnimationCallback {

	fun onStop()
	fun onBreak()
	fun onCancel()
	fun onComplete()

	fun onFrame (phase: Float32)

}