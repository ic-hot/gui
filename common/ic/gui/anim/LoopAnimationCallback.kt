package ic.gui.anim


import ic.base.primitives.float32.Float32


interface LoopAnimationCallback {

	fun onStop()
	fun onBreak()
	fun onCancel()

	fun onFrame (phase: Float32)

}