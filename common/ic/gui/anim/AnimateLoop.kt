package ic.gui.anim


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.ext.asFloat32
import ic.design.task.Task
import ic.system.funs.getCurrentEpochTimeMs
import ic.math.denormalize


fun animateLoop (

	toAnimate 	 : Boolean = true,
	toCallAtOnce : Boolean = true,

	from : Float32 = 0F,
	to 	 : Float32 = 1F,

	periodMs : Int32 = 1024,

	callback : LoopAnimationCallback

) : Task? {

	if (!toAnimate) return null

	val startTimeMs = getCurrentEpochTimeMs()

	return animateForever(

		toCallAtOnce = toCallAtOnce,

		onStop = callback::onStop,
		onCancel = callback::onCancel,
		onBreak = callback::onBreak,

		onFrame = {

			val currentTimeMs = getCurrentEpochTimeMs()

			val timeSinceStartMs = currentTimeMs - startTimeMs

			val timeSinceIterationStartMs = timeSinceStartMs % periodMs

			val phase = (timeSinceIterationStartMs.asFloat32 / periodMs).denormalize(from, to)

			callback.onFrame(phase)

		}

	)

}


inline fun animateLoop (

	toAnimate 		: Boolean = true,
	toCallAtOnce 	: Boolean = true,

	from 	: Float32 = 0F,
	to 		: Float32 = 1F,

	periodMs : Int32 = 1024,

	crossinline onStop 		: () -> Unit = {},
	crossinline onBreak 	: () -> Unit = {},
	crossinline onCancel	: () -> Unit = {},

	crossinline onFrame	: (phase: Float32) -> Unit

) : Task? {

	return animateLoop(
		toAnimate = toAnimate,
		toCallAtOnce = toCallAtOnce,
		from = from,
		to = to,
		periodMs = periodMs,
		callback = LoopAnimationCallback(
			onStop = onStop,
			onBreak = onBreak,
			onCancel = onCancel,
			onFrame = onFrame
		)
	)

}