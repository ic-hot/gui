package ic.gui.anim


import ic.base.escape.breakable.Break
import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asFloat32
import ic.design.task.Task

import ic.gui.anim.interpolator.AnimationInterpolator
import ic.gui.anim.interpolator.SmoothInterpolator
import ic.math.denormalize
import ic.system.funs.getCurrentEpochTimeMs


fun animateValue (

	toAnimate 		: Boolean = true,
	toCallAtOnce 	: Boolean = true,

	from 	: Float32 = 0F,
	to		: Float32 = 1F,

	durationMs : Int64 = 256L,

	interpolator : AnimationInterpolator,

	callback : ValueAnimationCallback

) : Task? {

	if (toCallAtOnce) {

		if (!toAnimate || durationMs == 0L) {
			callback.onFrame(to)
			callback.onStop()
			callback.onComplete()
			return null
		}

		callback.onFrame(from)

	}

	var isCompleted : Boolean = false

	val startTimeMs = getCurrentEpochTimeMs()

	return animateForever(
		toCallAtOnce = false,
		onStop = callback::onStop,
		onBreak = {
			if (isCompleted) {
				callback.onComplete()
			} else {
				callback.onBreak()
			}
		},
		onCancel = callback::onCancel,
	) {
		val currentTimeMs = getCurrentEpochTimeMs()
		val timeSinceStartMs = currentTimeMs - startTimeMs
		if (timeSinceStartMs >= durationMs) {
			callback.onFrame(to)
			isCompleted = true
			Break()
		}
		val rawPhase = timeSinceStartMs.asFloat32 / durationMs
		val interpolatedPhase = interpolator.getPhase(rawPhase)
		val denormalizedValue = interpolatedPhase.denormalize(from, to)
		callback.onFrame(denormalizedValue)
	}

}


inline fun animateValue (

	from : Float32 = 0F,
	to	 : Float32 = 1F,

	toAnimate 	 : Boolean = true,
	toCallAtOnce : Boolean = true,

	durationMs : Int64 = Int64(384),

	interpolator : AnimationInterpolator = SmoothInterpolator,

	crossinline onStop : () -> Unit = {},

	crossinline onBreak 	: () -> Unit = {},
	crossinline onCancel	: () -> Unit = {},
	crossinline onComplete	: () -> Unit = {},

	crossinline onFrame	: (phase: Float32) -> Unit

) = animateValue(
	toAnimate = toAnimate,
	toCallAtOnce = toCallAtOnce,
	from = from,
	to = to,
	durationMs = durationMs,
	interpolator = interpolator,
	callback = ValueAnimationCallback(
		onStop = onStop,
		onBreak = onBreak,
		onCancel = onCancel,
		onComplete = onComplete,
		onFrame = onFrame
	)
)