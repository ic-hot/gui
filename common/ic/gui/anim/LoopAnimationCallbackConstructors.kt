package ic.gui.anim


import ic.base.primitives.float32.Float32


inline fun LoopAnimationCallback (

	crossinline onStop : () -> Unit,

	crossinline onBreak 	: () -> Unit,
	crossinline onCancel	: () -> Unit,

	crossinline onFrame	: (phase: Float32) -> Unit

) : LoopAnimationCallback {

	return object : LoopAnimationCallback {

		override fun onStop() = onStop()

		override fun onBreak() = onBreak()

		override fun onCancel() = onCancel()

		override fun onFrame (phase: Float32) = onFrame(phase)

	}

}