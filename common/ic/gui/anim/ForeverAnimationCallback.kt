package ic.gui.anim


interface ForeverAnimationCallback {

	fun onStop()
	fun onBreak()
	fun onCancel()

	fun onFrame()

}