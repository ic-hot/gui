package ic.gui.anim


inline fun ForeverAnimationCallback (

	crossinline onStop : () -> Unit,

	crossinline onBreak 	: () -> Unit,
	crossinline onCancel	: () -> Unit,

	crossinline onFrame	: () -> Unit

) : ForeverAnimationCallback {

	return object : ForeverAnimationCallback {

		override fun onStop() = onStop()

		override fun onBreak() = onBreak()

		override fun onCancel() = onCancel()

		override fun onFrame() = onFrame()

	}

}