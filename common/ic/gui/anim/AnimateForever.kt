package ic.gui.anim


import ic.base.escape.breakable.Break
import ic.base.throwables.NotExistsException
import ic.design.task.Task
import ic.parallel.annotations.UiThread
import ic.parallel.funs.doInUiThread
import ic.struct.collection.ext.copy.copyToList
import ic.struct.collection.ext.count.isEmpty
import ic.struct.list.ext.foreach.forEach
import ic.struct.set.editable.EditableSet

import ic.struct.collection.ext.count.isNotEmpty


private val runningAnimations = EditableSet<ForeverAnimationCallback>()


internal fun processGlobalFrame() {
	if (runningAnimations.isEmpty) return
	runningAnimations.copyToList().forEach { animation ->
		if (runningAnimations.contains(animation)) {
			try {
				animation.onFrame()
			} catch (_: Break) {
				runningAnimations.remove(animation)
				animation.onStop()
				animation.onBreak()
			}
		}
	}
	scheduleGlobalFrame()
}


private fun scheduleGlobalFrame() {
	doInUiThread {
		processGlobalFrame()
	}
}


@UiThread
fun animateForever (
	toCallAtOnce : Boolean,
	callback : ForeverAnimationCallback
) : Task? {
	if (toCallAtOnce) {
		try {
			callback.onFrame()
		} catch (_: Break) {
			callback.onStop()
			callback.onBreak()
			return null
		}
	}
	val task = Task(
		cancel = {
			try {
				runningAnimations.removeOrThrowNotExists(callback)
				callback.onStop()
				callback.onCancel()
			} catch (_: NotExistsException) {}
		}
	)
	val isAnimationAlreadyRunning = runningAnimations.isNotEmpty
	runningAnimations.add(callback)
	if (!isAnimationAlreadyRunning) {
		scheduleGlobalFrame()
	}
	return task
}


inline fun animateForever (

	toCallAtOnce : Boolean,

	crossinline onStop : () -> Unit = {},

	crossinline onBreak  : () -> Unit = {},
	crossinline onCancel : () -> Unit = {},

	crossinline onFrame	: () -> Unit

) : Task? {

	return animateForever(
		toCallAtOnce = toCallAtOnce,
		callback = ForeverAnimationCallback(
			onStop = onStop,
			onBreak = onBreak,
			onCancel = onCancel,
			onFrame = onFrame
		)
	)

}