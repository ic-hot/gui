package ic.gui.anim.dynamic


import ic.base.primitives.float32.Float32

import ic.gui.anim.dynamic.newtonian.NewtonianValueAnimator


inline fun DynamicValueAnimatorForSwipeDp (

	crossinline applyState : DynamicValueAnimator.(position: Float32, velocity: Float32) -> Unit

) : DynamicValueAnimator = NewtonianValueAnimator(

	mass 			= Float32(1),
	friction 		= Float32(16),
	forceMagnitude 	= Float32(16384),

	applyState = applyState

)


inline fun DynamicValueAnimatorForScrollDp (

	crossinline applyState : DynamicValueAnimator.(position: Float32, velocity: Float32) -> Unit

) : DynamicValueAnimator = NewtonianValueAnimator(

	mass 			= Float32(4),
	friction 		= Float32(16),
	forceMagnitude 	= Float32(16384),

	applyState = applyState

)