package ic.gui.anim.dynamic.newtonian


import ic.base.primitives.float32.Float32

import ic.gui.anim.dynamic.DynamicValueAnimator


inline fun NewtonianValueAnimator (

	mass 			: Float32,
	friction 		: Float32,
	forceMagnitude 	: Float32,

	endSpeedInvS : Float32 = Float32(4),

	velocityTrackingTimeThresholdS 		: Float32 = 1F / 4,
	velocityTrackingSpeedThresholdInvS 	: Float32 = 4F,
	velocityTrackingMaxSpeedInvS 		: Float32 = 4096F,

	crossinline applyState : DynamicValueAnimator.(position: Float32, velocity: Float32) -> Unit

) : NewtonianValueAnimator {

	return object : NewtonianValueAnimator(

		mass 			= mass,
		friction 		= friction,
		forceMagnitude 	= forceMagnitude,

		endSpeedInvS = endSpeedInvS,

		velocityTrackingTimeThresholdS 		= velocityTrackingTimeThresholdS,
		velocityTrackingSpeedThresholdInvS 	= velocityTrackingSpeedThresholdInvS,
		velocityTrackingMaxSpeedInvS 		= velocityTrackingMaxSpeedInvS

	) {

		override fun applyState (position: Float32, velocity: Float32) {
			applyState(this, position, velocity)
		}

	}

}