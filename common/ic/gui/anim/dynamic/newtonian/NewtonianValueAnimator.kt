package ic.gui.anim.dynamic.newtonian


import kotlin.math.absoluteValue
import kotlin.math.exp
import kotlin.math.ln

import ic.base.primitives.bool.asSymmetricFloat32
import ic.base.primitives.float32.Float32

import ic.gui.anim.dynamic.DynamicValueAnimator


abstract class NewtonianValueAnimator (

	private val mass 			: Float32,
	private val friction 		: Float32,
	private val forceMagnitude 	: Float32,

	private val endSpeedInvS : Float32,

	velocityTrackingTimeThresholdS 		: Float32,
	velocityTrackingSpeedThresholdInvS 	: Float32,
	velocityTrackingMaxSpeedInvS 		: Float32

) : DynamicValueAnimator(
	velocityTrackingTimeThresholdS 		= velocityTrackingTimeThresholdS,
	velocityTrackingSpeedThresholdInvS 	= velocityTrackingSpeedThresholdInvS,
	velocityTrackingMaxSpeedInvS 		= velocityTrackingMaxSpeedInvS
) {


	private var a : Float32 = -friction / mass

	private var c : Float32 = Float32(0)
	private var d : Float32 = Float32(0)
	private var e : Float32 = Float32(0)
	private var f : Float32 = Float32(0)
	private var g : Float32 = Float32(0)

	private var releaseTimeS 		: Float32 = Float32(0)
	private var releaseVelocityInvS : Float32 = Float32(0)

	private var endPositionField 	: Float32 = Float32(0)
	private var endTimeSField 		: Float32 = Float32(0)


	private inline fun setUp (
		startPosition : Float32,
		startVelocityInvS : Float32,
		force : Float32,
		releaseTimeS : Float32,
		getReleaseVelocityInvS : () -> Float32,
		endPosition : Float32,
	) {
		c = force / friction
		d = startVelocityInvS - c
		e = d / a
		f = startPosition - e
		releaseVelocityInvS = getReleaseVelocityInvS()
		g = releaseVelocityInvS / a
		val releaseSpeedEnvS = releaseVelocityInvS.absoluteValue
		val endTimeAfterReleaseS = (
			if (releaseSpeedEnvS < endSpeedInvS) {
				Float32(0)
			} else {
				ln(endSpeedInvS / releaseSpeedEnvS) / a
			}
		)
		this.releaseTimeS = releaseTimeS
		endTimeSField = endTimeAfterReleaseS + releaseTimeS

		this.endPositionField = endPosition

	}

	override fun setUpTeleologically (startPosition: Float32, startVelocityInvS: Float32, endPosition: Float32) {
		val releaseTimeIfForceIsPositive = (
			(friction * (endPosition - startPosition) - startVelocityInvS * mass) / forceMagnitude
		)
		val toUseNegativeForce = releaseTimeIfForceIsPositive < 0
		val releaseTimeS = releaseTimeIfForceIsPositive * (!toUseNegativeForce).asSymmetricFloat32
		setUp(
			startPosition = startPosition, startVelocityInvS = startVelocityInvS,
			force = forceMagnitude * (!toUseNegativeForce).asSymmetricFloat32,
			releaseTimeS = releaseTimeS,
			getReleaseVelocityInvS = { getVelocityBeforeRelease(releaseTimeS) },
			endPosition = endPosition
		)
	}


	override fun setUpLocally (startPosition: Float32, startVelocityInvS: Float32) {
		setUp(
			startPosition = startPosition, startVelocityInvS = startVelocityInvS,
			force = forceMagnitude,
			releaseTimeS = Float32(0),
			getReleaseVelocityInvS = { startVelocityInvS },
			endPosition = startPosition - startVelocityInvS / a
		)
	}


	// Get velocity:

	private fun getVelocityBeforeRelease (time: Float32) : Float32 {
		val result = d * exp(a * time) + c
		return result
	}

	private fun getVelocityAfterRelease (timeAfterRelease: Float32) : Float32 = (
		releaseVelocityInvS * exp(a * timeAfterRelease)
	)

	override fun getVelocityInvS (timeS: Float32) : Float32 = (
		if (timeS < releaseTimeS) {
			getVelocityBeforeRelease(time = timeS)
		} else {
			getVelocityAfterRelease(timeAfterRelease = timeS - releaseTimeS)
		}
	)


	// Get position:

	private fun getPositionBeforeRelease (timeS: Float32) : Float32 {
		val result = e * exp(a*timeS) + c * timeS + f
		return result
	}

	private fun getPositionAfterRelease (timeSinceReleaseS: Float32) : Float32 {
		val result = g * exp(a * timeSinceReleaseS) + endPositionField
		return result
	}

	override fun getPosition (timeS: Float32) : Float32 {
		if (timeS < releaseTimeS) {
			return getPositionBeforeRelease(timeS = timeS)
		} else {
			return getPositionAfterRelease(timeSinceReleaseS = timeS - releaseTimeS)
		}
	}

	override fun getEndPosition() 	: Float32 = endPositionField
	override fun getEndTimeS() 		: Float32 = endTimeSField

	override fun waitFor() {
		// TODO
	}


}