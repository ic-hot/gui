package ic.gui.anim.dynamic


import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64
import ic.design.task.Task
import ic.ifaces.action.Action
import ic.ifaces.stoppable.Stoppable
import ic.base.primitives.int64.ext.asFloat32
import ic.ifaces.action.donothing.DoNothingAction
import ic.system.funs.getCurrentEpochTimeMs
import ic.util.velocitytracker.VelocityTracker

import ic.gui.anim.animateForever
import kotlin.math.absoluteValue


abstract class DynamicValueAnimator (

	velocityTrackingTimeThresholdS : Float32,

	private val velocityTrackingSpeedThresholdInvS : Float32,

	velocityTrackingMaxSpeedInvS : Float32

) : Stoppable {


	protected abstract fun applyState (position: Float32, velocity: Float32)


	// Getters

	fun getPosition() : Float32 = getPosition(timeSinceStartS)

	fun getVelocityInvS() : Float32 = getVelocityInvS(timeSinceStartS)

	abstract fun getEndPosition() : Float32


	// Actions:

	fun push (startPosition: Float32, startVelocityInvS: Float32) {
		seizeTimeEpochMs = -1
		implementPush(startPosition = startPosition, startVelocityInvS = startVelocityInvS)
	}

	fun push (startPosition: Float32, startVelocityInvS: Float32, endPosition: Float32) {
		seizeTimeEpochMs = -1
		implementPush(startPosition = startPosition, startVelocityInvS = startVelocityInvS, endPosition = endPosition)
	}

	fun setStationary (position: Float32 = getPosition()) {
		push(startPosition = position, startVelocityInvS = Float32(0))
	}

	val isStationary : Boolean get() = animationTask == null

	override fun stopNonBlockingOrThrowNotNeeded() = setStationary(position = getPosition(timeSinceStartS))

	fun setEndPosition (endPosition: Float32) {
		val currentTime = this.timeSinceStartS
		val currentPosition = getPosition(currentTime)
		val currentVelocityInvS = getVelocityInvS(currentTime)
		push(
			startPosition = currentPosition,
			startVelocityInvS = currentVelocityInvS,
			endPosition = endPosition
		)
	}

	fun correct (position: Float32, velocityInvS: Float32) {
		implementPush(startPosition = position, startVelocityInvS = velocityInvS)
	}


	// Seize and release:

	private val velocityTracker = VelocityTracker(
		timeThreshold = velocityTrackingTimeThresholdS,
		maxSpeed = velocityTrackingMaxSpeedInvS
	)

	private var seizeTimeEpochMs : Int64 = -1

	private val timeSinceSeizeS get() = (getCurrentEpochTimeMs() - seizeTimeEpochMs).asFloat32 / 1000

	fun seize (downPosition: Float32, downTimeEpochMs: Int64 = getCurrentEpochTimeMs()) {
		if (seizeTimeEpochMs >= 0) return
		seizeTimeEpochMs = downTimeEpochMs
		velocityTracker.addPoint(time = 0F, position = downPosition)
		implementPush(startPosition = downPosition, startVelocityInvS = Float32(0))
	}

	fun move (position : Float32) {
		if (seizeTimeEpochMs < 0) return
		velocityTracker.addPoint(time = timeSinceSeizeS, position = position)
		implementPush(startPosition = position, startVelocityInvS = Float32(0))
	}

	fun release (position: Float32 = getPosition()) {
		if (seizeTimeEpochMs < 0) return
		var releaseVelocityInvS = velocityTracker.calculateVelocity(time = timeSinceSeizeS)
		if (releaseVelocityInvS.absoluteValue < velocityTrackingSpeedThresholdInvS) {
			releaseVelocityInvS = Float32(0)
		}
		velocityTracker.clear()
		push(
			startPosition = position,
			startVelocityInvS = releaseVelocityInvS
		)
	}


	// Dynamics:

	protected abstract fun setUpLocally (startPosition: Float32, startVelocityInvS: Float32)

	protected abstract fun setUpTeleologically (startPosition: Float32, startVelocityInvS: Float32, endPosition: Float32)

	protected abstract fun getPosition (timeS: Float32) : Float32

	protected abstract fun getVelocityInvS (timeS: Float32) : Float32

	protected abstract fun getEndTimeS() : Float32


	// Animation:

	private var animationTask : Task? = null

	private fun startAnimationIfNeeded() {
		if (animationTask != null) return
		if (timeSinceStartS > getEndTimeS()) return
		animationTask = animateForever(toCallAtOnce = false) {
			val currentTime = this.timeSinceStartS
			if (currentTime > getEndTimeS()) {
				setStationary(
					position = getEndPosition()
				)
				invokeOnStop()
				animationTask = null
			} else {
				applyState(
					position = getPosition(currentTime),
					velocity = getVelocityInvS(currentTime)
				)
			}
		}
	}

	private fun implementPush (startPosition: Float32, startVelocityInvS: Float32, endPosition: Float32) {
		startTimeEpochMs = getCurrentEpochTimeMs()
		setUpTeleologically(startPosition = startPosition, startVelocityInvS = startVelocityInvS, endPosition = endPosition)
		if (startVelocityInvS == Float32(0) && endPosition == startPosition) {
			animationTask?.cancel()
			animationTask = null
		} else {
			startAnimationIfNeeded()
		}
		applyState(position = startPosition, velocity = startVelocityInvS)
	}

	private fun implementPush (startPosition: Float32, startVelocityInvS: Float32) {
		startTimeEpochMs = getCurrentEpochTimeMs()
		setUpLocally(startPosition = startPosition, startVelocityInvS = startVelocityInvS)
		if (startVelocityInvS == Float32(0)) {
			animationTask?.cancel()
			animationTask = null
		} else {
			startAnimationIfNeeded()
		}
		applyState(position = startPosition, velocity = startVelocityInvS)
	}


	// Time:

	private var startTimeEpochMs : Int64 = 0

	private val timeSinceStartS : Float32 get() = (getCurrentEpochTimeMs() - startTimeEpochMs).asFloat32 / 1000


	// On stop action:

	var onCompleteAction : Action = DoNothingAction

	private fun invokeOnStop() {
		onCompleteAction.run()
		onCompleteAction = DoNothingAction
	}


}