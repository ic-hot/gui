package ic.gui.window.ext


import ic.gui.window.Window


inline val Window.thisWindow get() = this