package ic.gui.window.orient


sealed class ScreenOrientationConfig {

	object Landscape : ScreenOrientationConfig()

	object Portrait : ScreenOrientationConfig()

	object All : ScreenOrientationConfig()

}