package ic.gui.window.scope


import ic.ifaces.lifecycle.hasopenstate.HasIsOpenState
import ic.ifaces.cancelable.Cancelable

import ic.gui.dim.dp.Dp
import ic.gui.scope.proxy.ProxyViewScope
import ic.gui.view.View


interface WindowScope : HasIsOpenState, ProxyViewScope {


	val topDecorHeight    : Dp
	val bottomDecorHeight : Dp

	val windowWidth  : Dp
	val windowHeight : Dp

	val isKeyboardShown : Boolean


	fun addOverlayView (view: View)

	fun removeOverlayView (view: View)


	fun listenBackButtonBlocking (action : () -> Unit) : Cancelable


}