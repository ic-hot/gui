package ic.gui.window.scope.ext


import ic.ifaces.lifecycle.closeable.Closeable

import ic.gui.popup.Popup
import ic.gui.window.scope.WindowScope


fun WindowScope.openPopup (

	popup : Popup

) : Closeable {

	popup.open(this)

	return popup

}