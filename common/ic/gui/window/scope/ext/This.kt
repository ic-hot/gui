package ic.gui.window.scope.ext


import ic.gui.window.scope.WindowScope


inline val WindowScope.thisWindowScope get() = this