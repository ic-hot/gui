package ic.gui.window


import ic.base.arrays.ext.copy.copyToCollection
import ic.base.assert.assert
import ic.ifaces.cancelable.Cancelable
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.stream.input.ByteInput
import ic.struct.list.Lazy
import ic.struct.list.List
import ic.struct.list.editable.removeAll
import ic.struct.list.ext.first
import ic.struct.list.ext.length.isEmpty
import ic.struct.list.editable.EditableList
import ic.struct.list.editable.ext.removeOne
import ic.stream.output.ByteOutput
import ic.struct.collection.Collection
import ic.util.url.Url

import ic.graphics.image.Image

import ic.gui.vc.AViewController
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.permissions.Permission
import ic.gui.scope.ext.views.clickable.Clickable
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.View
import ic.gui.window.context.WindowContext
import ic.gui.window.orient.ScreenOrientationConfig
import ic.gui.window.scope.WindowScope
import ic.util.log.StackTrace
import ic.util.log.logD


abstract class Window : AViewController(), WindowScope {


	open val isResizable : Boolean get() = true

	open val isCloseable : Boolean get() = true


	open val title : String? get() = null

	open val initialWidth  : Dp get() = 512.dp
	open val initialHeight : Dp get() = 512.dp

	open val isTopDecorLight    : Boolean get() = true
	open val isBottomDecorLight : Boolean get() = true

	open val screenOrientationConfig : ScreenOrientationConfig get() = ScreenOrientationConfig.All


	private var context : WindowContext? = null


	private var view : View? = null



	// Lifecycle:

	override val isOpen get() = context != null

	protected open fun ByteInput.load() {}

	internal fun notifyOpen (context: WindowContext, stateInput: ByteInput) : View {
		assert { this.context == null }
		this.context = context
		stateInput.runAndCloseOrCancel { load() }
		assert { view == null }
		val hideKeyboardClickView = Clickable { hideKeyboard() }
		val viewWithoutOverlays = initView()
		view = Stack(
			width = ByContainer, height = ByContainer,
			children = List.Lazy(
				getLength = { 1 + 1 + overlayViews.length },
				getItem = { index ->
					when (index) {
						0L   -> hideKeyboardClickView
						1L   -> viewWithoutOverlays
						else -> overlayViews[index - 2]
					}
				}
			)
		)
		onOpen()
		context.viewContext.open(view!!)
		return view!!
	}


	protected open fun onResume() {}

	internal fun notifyResume() {
		onResume()
	}


	protected open fun onPause() {}

	internal fun notifyPause() {
		onPause()
	}


	protected open fun onUrl (url: Url) {}

	internal fun notifyUrl (url: Url) {
		onUrl(url)
	}


	protected open fun ByteInput.onBroadcast() {}

	internal fun notifyBroadcast (input: ByteInput) {
		input.runAndCloseOrCancel {
			onBroadcast()
		}
	}


	protected fun initResultOutput() : ByteOutput {
		return context!!.initResultOutput()
	}

	protected inline fun closeWithResult (writeResult: ByteOutput.() -> Unit) {
		initResultOutput().runAndCloseOrCancel(writeResult)
	}

	protected open fun ByteOutput.save() {}

	internal fun notifySave (output: ByteOutput?) {
		output?.runAndCloseOrCancel {
			save()
		}
	}

	sealed class CloseReason {
		object ByCode   : CloseReason()
		object ByUser   : CloseReason()
		object BySystem : CloseReason()
	}

	protected open fun onClose (reason: CloseReason) {}

	fun notifyClose (reason: CloseReason) {
		cancelTasks()
		onClose(reason = reason)
		context!!.viewContext.close()
		context = null
		view = null
	}

	protected fun close() {
		context!!.close()
	}


	// Back button:

	protected open fun onBackButton() {
		if (isCloseable) {
			close()
		}
	}

	private val stackingBackButtonActions = EditableList<() -> Unit>()

	override fun listenBackButtonBlocking (action: () -> Unit) : Cancelable {
		stackingBackButtonActions.insert(0, action)
		return Cancelable(
			cancel = { stackingBackButtonActions.removeAll { it == action } }
		)
	}

	internal fun notifyBackButton() {
		if (stackingBackButtonActions.isEmpty) {
			onBackButton()
		} else {
			stackingBackButtonActions.first.invoke()
		}
	}


	// Keyboard:

	protected open fun onKeyboardShown() {}

	internal fun notifyKeyboardShown() {
		onKeyboardShown()
	}


	protected open fun onKeyboardHidden() {}

	internal fun notifyKeyboardHidden() {
		onKeyboardHidden()
	}


	// Open window:


	protected fun initOpenWindowOutput() : ByteOutput {
		return context!!.initOpenWindowOutput()
	}

	protected inline fun openWindow (
		writeWindowTypeAndState  : ByteOutput.() -> Unit,
	) {
		initOpenWindowOutput().runAndCloseOrCancel(writeWindowTypeAndState)
	}


	protected open fun ByteInput.onWindowResult() {}

	fun notifyWindowResult (windowTypeAndResultInput : ByteInput) {
		windowTypeAndResultInput.runAndCloseOrCancel {
			onWindowResult()
		}
	}


	// File selector:

	protected fun openFileSelector() = context!!.openFileSelector()

	protected open fun onFileSelected (fileInput: ByteInput) {}

	internal fun notifyFileSelected (fileInput: ByteInput) {
		onFileSelected(fileInput)
	}


	// Image selector:

	protected fun openImageSelector (titleText: String) {
		context!!.openImageSelector(titleText = titleText)
	}

	protected open fun onImageSelected (image: Image) {}

	internal fun notifyImageSelected (image: Image) {
		onImageSelected(image = image)
	}

	protected open fun onSelectedImageNotExists() {}

	internal fun notifySelectedImageNotExists() {
		onSelectedImageNotExists()
	}

	protected open fun onUnableToParseSelectedImage() {}

	internal fun notifyUnableToParseSelectedImage() {
		onUnableToParseSelectedImage()
	}


	// Permissions:

	protected fun requestPermission (permissions: Collection<Permission>) {
		context!!.requestPermission(permissions)
	}

	protected fun requestPermission (vararg permissions: Permission) {
		requestPermission(
			permissions = permissions.copyToCollection()
		)
	}

	protected open fun onPermissionDenied() {}

	internal fun notifyPermissionDenied() {
		onPermissionDenied()
	}

	protected open fun onPermissionGranted() {}

	internal fun notifyPermissionGranted() {
		onPermissionGranted()
	}


	// External interface:

	protected fun externalCall (message: Any?) : Any? {
		return context!!.externalCall(message)
	}

	protected open fun onExternalCall (message: Any?) : Any? = null

	internal fun notifyExternalCall (message: Any?) : Any? {
		return onExternalCall(message)
	}


	// WindowScope:

	override val sourceViewScope get() = context!!.viewContext

	override val windowWidth  get() = context!!.windowWidth
	override val windowHeight get() = context!!.windowHeight

	override val topDecorHeight    get() = context!!.topDecorHeight
	override val bottomDecorHeight get() = context!!.bottomDecorHeight

	override val isKeyboardShown get() = context!!.isKeyboardShown


	// Overlays:

	private val overlayViews = EditableList<View>()

	override fun addOverlayView (view: View) {
		overlayViews.add(view)
	}

	override fun removeOverlayView (view: View) {
		overlayViews.removeOne { it === view }
	}


}