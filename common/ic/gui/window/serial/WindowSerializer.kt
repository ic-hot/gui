package ic.gui.window.serial


import ic.base.throwables.IoException
import ic.stream.input.ByteInput
import ic.stream.output.ByteOutput

import ic.gui.window.Window


interface WindowSerializer {


	@Throws(IoException::class)
	fun ByteInput.readWindow() : Window


	@Deprecated("To remove")
	@Throws(IoException::class)
	fun ByteOutput.writeWindow (window: Window)


}