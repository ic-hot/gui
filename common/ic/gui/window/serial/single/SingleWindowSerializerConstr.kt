package ic.gui.window.serial.single


import ic.gui.window.Window


inline fun SingleWindowSerializer (

	crossinline initWindow : () -> Window

) : SingleWindowSerializer {

	return object : SingleWindowSerializer() {

		override fun initWindow() = initWindow()

	}

}