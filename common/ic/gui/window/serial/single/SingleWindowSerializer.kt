package ic.gui.window.serial.single


import ic.gui.window.Window
import ic.gui.window.serial.WindowSerializer

import ic.stream.input.ByteInput
import ic.stream.output.ByteOutput


abstract class SingleWindowSerializer : WindowSerializer {

	protected abstract fun initWindow () : Window

	final override fun ByteInput.readWindow () = initWindow()

	final override fun ByteOutput.writeWindow (window: Window) {}

}