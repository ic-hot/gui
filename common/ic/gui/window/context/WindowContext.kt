package ic.gui.window.context


import ic.ifaces.lifecycle.closeable.Closeable
import ic.struct.collection.Collection
import ic.stream.output.ByteOutput

import ic.gui.dim.dp.Dp
import ic.gui.permissions.Permission
import ic.gui.root.context.GuiRootContext
import ic.gui.view.context.ViewContext


interface WindowContext : GuiRootContext, Closeable {


	val viewContext : ViewContext


	val topDecorHeight    : Dp
	val bottomDecorHeight : Dp

	val windowWidth  : Dp
	val windowHeight : Dp

	val isKeyboardShown : Boolean
	

	fun initResultOutput() : ByteOutput


	fun openImageSelector (titleText: String)

	fun openFileSelector()


	fun requestPermission (permissions: Collection<Permission>)


}