package ic.gui.ifaces


import ic.gui.dim.dp.Dp


interface HasGlobalPosition {


	val globalX : Dp
	val globalY : Dp


}