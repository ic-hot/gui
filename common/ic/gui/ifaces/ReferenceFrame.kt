package ic.gui.ifaces


import ic.gui.dim.dp.Dp


interface ReferenceFrame : HasGlobalPosition {


	val HasGlobalPosition.localX : Dp get() {
		return this.globalX - this@ReferenceFrame.globalX
	}

	val HasGlobalPosition.localY : Dp get() {
		return this.globalY - this@ReferenceFrame.globalY
	}


}