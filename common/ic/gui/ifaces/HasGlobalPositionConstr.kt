package ic.gui.ifaces


import ic.gui.dim.dp.Dp


inline fun HasGlobalPosition (

	crossinline getGlobalX : () -> Dp,
	crossinline getGlobalY : () -> Dp

) : HasGlobalPosition {

	return object : HasGlobalPosition {

		override val globalX get() = getGlobalX()
		override val globalY get() = getGlobalY()

	}

}