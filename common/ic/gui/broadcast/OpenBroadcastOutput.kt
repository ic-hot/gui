package ic.gui.broadcast


import ic.stream.output.ByteOutput

import ic.gui.impl.guiPlatform


fun openBroadcastOutput() : ByteOutput {

	return guiPlatform.openBroadcastOutput()

}