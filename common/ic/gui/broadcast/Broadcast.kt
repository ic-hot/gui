package ic.gui.broadcast


import ic.base.throwables.IoException
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.stream.output.ByteOutput


inline fun broadcast (

	write : ByteOutput.() -> Unit

) {

	try {

		openBroadcastOutput().runAndCloseOrCancel(write)

	} catch (_: IoException) {
		throw RuntimeException()
	}


}