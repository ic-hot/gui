package ic.gui.trans


sealed class SimpleTransition {


	object None : SimpleTransition()


	object LeftToRight : SimpleTransition()
	object RightToLeft : SimpleTransition()


}