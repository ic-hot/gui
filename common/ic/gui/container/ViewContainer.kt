package ic.gui.container


import ic.gui.dim.dp.Dp
import ic.gui.ifaces.HasGlobalPosition
import ic.gui.view.View


interface ViewContainer : HasGlobalPosition {


	fun getLocalXOf (subview: View) : Dp
	fun getLocalYOf (subview: View) : Dp

	fun getWidthOf  (subview: View) : Dp
	fun getHeightOf (subview: View) : Dp


}