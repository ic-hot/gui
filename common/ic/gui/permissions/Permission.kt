package ic.gui.permissions


sealed class Permission {

	object Camera : Permission()

	object Location : Permission()

	object Notifications : Permission()

}