package ic.gui.adapters


import ic.gui.dim.dp.Dp
import ic.gui.view.View


interface ViewAdapter<V: View, Impl: Any> {


	fun V.initImpl() : Impl


	fun V.updateImplLocalPosition (impl: Impl, localX: Dp, localY: Dp)

	fun updateImplSize (icView: V, impl: Impl, width: Dp, height: Dp)


	fun V.updateImplSelf (impl: Impl)


	fun getIntrinsicMinWidth (view: V, impl: Impl) : Dp

	fun getIntrinsicMinHeight (view: V, impl: Impl) : Dp


}