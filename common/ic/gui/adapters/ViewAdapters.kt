package ic.gui.adapters


import ic.gui.view.simple.camera.CameraScannerView
import ic.gui.view.simple.canvas.CanvasView
import ic.gui.view.card.CardView
import ic.gui.view.container.ContainerView
import ic.gui.view.simple.datepicker.DatePickerView
import ic.gui.view.simple.gradient.LinearGradientView
import ic.gui.view.simple.image.ImageView
import ic.gui.view.simple.pdf.PdfView
import ic.gui.view.simple.text.impl.adapter.TextViewAdapter
import ic.gui.view.simple.web.WebView


interface ViewAdapters<ViewImpl: Any> {


	val cameraScannerViewAdapter : ViewAdapter<CameraScannerView, out ViewImpl>

	val canvasViewAdapter : ViewAdapter<CanvasView, out ViewImpl>

	val cardViewAdapter : ViewAdapter<CardView, out ViewImpl>

	val containerViewAdapter : ViewAdapter<ContainerView, out ViewImpl>

	val datePickerViewAdapter : ViewAdapter<DatePickerView, out ViewImpl>

	val imageViewAdapter : ViewAdapter<ImageView, out ViewImpl>

	val linearGradientViewAdapter : ViewAdapter<LinearGradientView, out ViewImpl>

	val pdfViewAdapter : ViewAdapter<PdfView, out ViewImpl>

	val textViewAdapter : TextViewAdapter<out ViewImpl>

	val webViewAdapter : ViewAdapter<WebView, out ViewImpl>


}