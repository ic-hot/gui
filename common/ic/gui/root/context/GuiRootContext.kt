package ic.gui.root.context


import ic.stream.output.ByteOutput


interface GuiRootContext {


	fun initOpenWindowOutput() : ByteOutput


	fun externalCall (message: Any?) : Any?


}