package ic.gui.app


import ic.app.App
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.parallel.funs.sleepWhile
import ic.stream.output.ByteOutput

import ic.gui.app.context.guiAppContext
import ic.gui.app.context.impl.globalWindowSerializer
import ic.gui.window.serial.WindowSerializer


abstract class AGuiApp : App(), WindowSerializer {


	protected abstract fun ByteOutput.writeInitialWindowTypeAndState (args: String)


	protected open fun beforeStart () {}

	final override fun implRun (args: String) {
		globalWindowSerializer = this
		beforeStart()
		guiAppContext.initOpenWindowOutput().runAndCloseOrCancel {
			writeInitialWindowTypeAndState(args)
		}
	}


	final override fun implStopNonBlocking() {}


	override fun implWaitFor() {
		sleepWhile { guiAppContext.isOpen }
	}


	// External

	protected fun externalCall (message: Any?) : Any? {
		return guiAppContext.externalCall(message)
	}

	protected open fun onExternalCall (message: Any?) : Any? = throw RuntimeException()

	internal fun notifyExternalCall (message: Any?) : Any? {
		return onExternalCall(message)
	}


}