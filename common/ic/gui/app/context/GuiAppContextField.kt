package ic.gui.app.context


import ic.base.kfunctions.ext.getValue
import ic.struct.value.cached.Cached

import ic.gui.impl.guiPlatform


internal val guiAppContext by Cached { guiPlatform.guiAppContext }