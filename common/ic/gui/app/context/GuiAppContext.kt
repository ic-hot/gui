package ic.gui.app.context


import ic.ifaces.lifecycle.hasopenstate.HasIsOpenState

import ic.gui.root.context.GuiRootContext


interface GuiAppContext : GuiRootContext, HasIsOpenState {


}