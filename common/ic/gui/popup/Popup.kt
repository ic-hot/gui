package ic.gui.popup


import ic.base.escape.skip.skip
import ic.base.primitives.float32.Float32
import ic.design.task.scope.BaseTaskScope
import ic.gui.anim.animateValue
import ic.ifaces.cancelable.Cancelable
import ic.ifaces.lifecycle.closeable.Closeable
import ic.struct.list.List
import ic.math.denormalize

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.noview.NoView
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.proxy.ProxyViewScope
import ic.gui.touch.click.ClickHandler
import ic.gui.touch.drag.VerticalDragHandler
import ic.gui.touch.group.GroupTouchHandler
import ic.gui.touch.screen.ScreeningTouchHandler
import ic.gui.view.View
import ic.gui.view.container.empty.touch.Touchable
import ic.gui.view.ext.updatePosition
import ic.gui.view.ext.updateSelf
import ic.gui.window.scope.WindowScope
import ic.math.ext.clamp


abstract class Popup : BaseTaskScope(), Closeable, ProxyViewScope {


	protected abstract val behavior : Behavior

	protected open fun WindowScope.BackgroundView() : View = NoView()

	protected abstract fun WindowScope.PopupView() : View


	sealed class Behavior {
		object ClickThrough : Behavior()
		object Blocking     : Behavior()
		object Closeable    : Behavior()
	}


	private var backgroundView          : View? = null
	private var backgroundTouchableView : View? = null
	private var popupView               : View? = null
	private var decorView               : View? = null


	private var animation : Cancelable? = null

	private var phase : Float32 = 0F

	private fun setPhase (phase: Float32) {
		this.phase = phase
		backgroundView?.updateSelf()
		popupView?.updatePosition()
	}


	private var windowScope : WindowScope? = null


	final override var isOpen : Boolean = false; private set

	protected open fun onOpen() {}

	private var listenBackTask : Cancelable? = null

	fun open (windowScope: WindowScope) {
		this.windowScope = windowScope
		isOpen = true
		val behavior = this.behavior
		backgroundTouchableView = when (behavior) {
			Behavior.ClickThrough -> NoView()
			else -> Touchable(
				touchHandler = GroupTouchHandler(
					children = List(
						initItem0 = {
							when (behavior) {
								Behavior.Closeable -> {
									var dragStartPhase : Float32 = 0F
									var dragStartY : Dp = 0.dp
									VerticalDragHandler(
										onDragStart = { event ->
											dragStartPhase = phase
											dragStartY = event.globalY
										},
										onDragMove = { event ->
											setPhase(
												(
													dragStartPhase -
													(event.globalY - dragStartY) / windowScope.windowHeight
												).clamp()
											)
										},
										onDragEnd = {
											if (phase < dragStartPhase) {
												close(byUser = true)
											} else {
												animateValue(phase, 1F) { phase ->
													setPhase(phase)
												}
											}
										},
										onDragCancel = {
											animateValue(phase, 1F) { phase ->
												setPhase(phase)
											}
										}
									)
								}
								else -> skip
							}
						},
						initItem1 = {
							when (behavior) {
								Behavior.Closeable -> ClickHandler { close(byUser = true) }
								else -> skip
							}
						},
						initItem2 = { ScreeningTouchHandler }
					)
				)
			)
		}
		backgroundView = Stack(
			width = ByContainer, height = ByContainer,
			getOpacity = { phase },
			children = List(
				windowScope.BackgroundView()
			)
		)
		popupView = Stack(
			width = ByContainer, height = ByContainer,
			getTranslationY = {
				phase.denormalize(windowScope.windowHeight, 0)
			},
			children = List(
				windowScope.PopupView()
			)
		)
		decorView = Stack(
			width = ByContainer, height = ByContainer,
			children = List(
				backgroundTouchableView!!,
				backgroundView!!,
				popupView!!
			)
		)
		windowScope.addOverlayView(decorView!!)
		onOpen()
		updateAllViews()
		animateValue(0F, 1F) { phase ->
			setPhase(phase)
		}
		if (behavior == Behavior.Closeable) {
			listenBackTask = windowScope.listenBackButtonBlocking {
				close()
			}
		}
	}


	protected open fun onClose (byUser: Boolean) {}

	final override fun close() {
		close(byUser = false)
	}

	private fun close (byUser: Boolean) {
		if (!isOpen) return
		cancelTasks()
		onClose(byUser = byUser)
		isOpen = false
		listenBackTask?.cancel()
		listenBackTask = null
		animateValue(
			phase, 0F,
			onFrame = { setPhase(it) },
			onComplete = {
				if (windowScope != null) {
					if (windowScope!!.isOpen) {
						windowScope!!.removeOverlayView(decorView!!)
						windowScope!!.updateAllViews()
					}
					decorView = null
					backgroundTouchableView = null
					backgroundView = null
					popupView = null
					decorView = null
					windowScope = null
				}
			}
		)
	}


	override val sourceViewScope get() = windowScope?.sourceViewScope


}