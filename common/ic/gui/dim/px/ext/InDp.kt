package ic.gui.dim.px.ext


import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.screenDensityFactor
import ic.gui.dim.px.Px


inline val Px.inDp : Dp get() = this / screenDensityFactor