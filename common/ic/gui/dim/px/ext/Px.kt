package ic.gui.dim.px.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.int32.Int32

import ic.gui.dim.px.Px


inline val Int32.px : Px get() = this

inline val Float32.px : Px get() = this.asInt32