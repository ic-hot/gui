package ic.gui.dim.px


import ic.base.primitives.int32.Int32


const val ByContent   = Int32.MIN_VALUE
const val ByContainer = Int32.MAX_VALUE