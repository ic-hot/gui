@file:Suppress("NOTHING_TO_INLINE")


package ic.gui.dim.px


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.int32.Int32


typealias Px = Int32

inline fun Px (int32: Int32) : Px = int32

inline fun Px (float32: Float32) : Px = float32.asInt32