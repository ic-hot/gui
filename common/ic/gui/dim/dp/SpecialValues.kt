package ic.gui.dim.dp


import ic.base.primitives.float32.Float32


const val ByContent   : Dp = Float32.NEGATIVE_INFINITY
const val ByContainer : Dp = Float32.POSITIVE_INFINITY