package ic.gui.dim.dp


import ic.base.primitives.float32.Float32
import ic.base.primitives.float32.ext.isNaN

import ic.gui.impl.guiPlatform


private var cached : Float32 = Float32.NaN

val screenDensityFactor : Float32 get() {
	cached.let {
		if (!it.isNaN) return it
	}
	return guiPlatform.getScreenDensityFactor().also {
		cached = it
	}
}


const val Mdpi = 1F

const val Xxxhdpi = 4F