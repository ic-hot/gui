package ic.gui.dim.dp.ext


import ic.math.ext.roundAsInt32

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.screenDensityFactor
import ic.gui.dim.px.Px


inline val Dp.inPx : Px get() = when (this) {

	ic.gui.dim.dp.ByContent   -> ic.gui.dim.px.ByContent
	ic.gui.dim.dp.ByContainer -> ic.gui.dim.px.ByContainer

	else -> (this * screenDensityFactor).roundAsInt32

}