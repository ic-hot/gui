package ic.gui.dim.dp.ext


import ic.base.primitives.float32.Float32
import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32

import ic.gui.dim.dp.Dp


inline val Int32.dp : Dp get() = this.asFloat32

inline val Float32.dp : Dp get() = this

inline val Float64.dp : Dp get() = this.asFloat32