@file:Suppress("NOTHING_TO_INLINE")


package ic.gui.dim.dp


import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asFloat32


typealias Dp = Float32

inline fun Dp (int32: Int32) : Dp = int32.asFloat32