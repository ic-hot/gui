package ic.gui.align


import ic.base.primitives.float32.Float32


typealias HorizontalAlign = Float

val Left  : Float32 = Float32(0)
val Right : Float32 = Float32(1)


typealias VerticalAlign = Float

val Top    : Float32 = Float32(0)
val Bottom : Float32 = Float32(1)


val Center = Float32(.5)
