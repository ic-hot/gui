package ic.gui.touch


inline fun TouchHandler (

	crossinline onDown : (TouchEvent.Down) -> TouchDirective,

	crossinline onMove : (TouchEvent.Move) -> TouchDirective,

	crossinline onUp : (TouchEvent.Up) -> TouchDirective,

	crossinline onCancel : (TouchEvent.Cancel) -> Unit

) : TouchHandler {

	return object : TouchHandler() {

		override fun onDown (event: TouchEvent.Down) : TouchDirective {
			return onDown(event)
		}

		override fun onMove(event: TouchEvent.Move) : TouchDirective {
			return onMove(event)
		}

		override fun onUp (event: TouchEvent.Up) : TouchDirective {
			return onUp(event)
		}

		override fun onCancel (event: TouchEvent.Cancel) {
			onCancel(event)
		}

	}

}