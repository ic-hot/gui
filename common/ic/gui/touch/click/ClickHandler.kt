package ic.gui.touch.click


import ic.math.ext.squared

import ic.gui.touch.TouchDirective
import ic.gui.touch.TouchDirective.*
import ic.gui.touch.TouchEvent
import ic.gui.touch.TouchHandler


abstract class ClickHandler : TouchHandler() {


	protected abstract val isClickEnabled : Boolean

	protected abstract fun onClick()


	protected abstract val isSecondaryClickEnabled : Boolean

	protected abstract fun onSecondaryClick()


	private var downEvent : TouchEvent.Down? = null

	override fun onDown (event: TouchEvent.Down) : TouchDirective {
		if (downEvent == null) {
			if (isClickEnabled) {
				downEvent = event
				return Watch
			} else {
				return Forget
			}
		} else {
			return Forget
		}
	}

	override fun onMove (event: TouchEvent.Move) : TouchDirective {
		val downEvent = this.downEvent!!
		val dx = event.globalX - downEvent.globalX
		val dy = event.globalY - downEvent.globalY
		if (dx.squared + dy.squared < clickRadiusThreshold.squared) {
			return Watch
		} else {
			this.downEvent = null
			return Forget
		}
	}

	override fun onUp (event: TouchEvent.Up) : TouchDirective {
		downEvent = null
		onClick()
		return Capture
	}

	override fun onCancel (event: TouchEvent.Cancel) {
		downEvent = null
	}


}