package ic.gui.touch.click

import ic.gui.ifaces.HasGlobalPosition


inline fun ClickHandler (

	crossinline onClick : () -> Unit,

	crossinline isInTouchableArea : (HasGlobalPosition) -> Boolean = { true },

	crossinline isClickEnabled: () -> Boolean,

	crossinline isSecondaryClickEnabled : () -> Boolean,

	crossinline onSecondaryClick : () -> Unit,

	crossinline getDebugName : () -> String? = { null },

) : ClickHandler {

	return object : ClickHandler() {

		override val debugName get() = getDebugName()

		override fun isInTouchableArea (event: HasGlobalPosition) = isInTouchableArea(event)

		override val isClickEnabled get() = isClickEnabled()

		override fun onClick() {
			onClick()
		}

		override val isSecondaryClickEnabled get() = isSecondaryClickEnabled()

		override fun onSecondaryClick() {
			onSecondaryClick()
		}

	}

}


inline fun ClickHandler (

	crossinline isClickEnabled: () -> Boolean = { true },

	crossinline isInTouchableArea : (HasGlobalPosition) -> Boolean = { true },

	crossinline getDebugName : () -> String? = { null },

	crossinline onClick : () -> Unit,

) = (
	ClickHandler(
		isInTouchableArea = isInTouchableArea,
		isClickEnabled = isClickEnabled,
		onClick = onClick,
		isSecondaryClickEnabled = { false },
		onSecondaryClick = {},
		getDebugName = getDebugName
	)
)