package ic.gui.touch.click


import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp


val clickRadiusThreshold : Dp = 4.dp