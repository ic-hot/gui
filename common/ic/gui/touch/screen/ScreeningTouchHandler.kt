package ic.gui.touch.screen


import ic.gui.touch.TouchDirective
import ic.gui.touch.TouchDirective.*
import ic.gui.touch.TouchEvent
import ic.gui.touch.TouchHandler


object ScreeningTouchHandler : TouchHandler() {

	override fun onDown (event: TouchEvent.Down) : TouchDirective {
		return Screen.AndForget
	}

	override fun onMove (event: TouchEvent.Move) : TouchDirective {
		throw RuntimeException()
	}

	override fun onUp (event: TouchEvent.Up) : TouchDirective {
		throw RuntimeException()
	}

	override fun onCancel (event: TouchEvent.Cancel) {
		throw RuntimeException()
	}

}