package ic.gui.touch.group


import ic.base.escape.skip.skip
import ic.base.escape.skip.skippable
import ic.base.primitives.int64.Int64
import ic.ifaces.gettersetter.gettersetter1.ext.untyped.getOrCreateIfNull
import ic.struct.collection.ext.foreach.forEach
import ic.struct.collection.ext.count.isEmpty
import ic.struct.collection.ext.copy.copy
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach
import ic.struct.map.editable.EditableMap
import ic.struct.set.editable.EditableSet

import ic.gui.touch.TouchDirective
import ic.gui.touch.TouchDirective.*
import ic.gui.touch.TouchEvent
import ic.gui.touch.TouchHandler
import ic.gui.touch.ext.cancelEvent


abstract class GroupTouchHandler : TouchHandler() {


	protected abstract val children : List<TouchHandler>


	private val subscribedChildrenByTouchId = EditableMap<Int64, EditableSet<TouchHandler>>()

	private fun getSubscribedChildren (touchId: Int64) = (
		subscribedChildrenByTouchId.getOrCreateIfNull(touchId) { EditableSet() }
	)


	final override fun onDown (event: TouchEvent.Down) : TouchDirective {
		val subscribed = getSubscribedChildren(touchId = event.touchId)
		var screenDirective : TouchDirective.Screen? = null
		skippable {
			children.forEach { child ->
				val directive = child.handle(event)
				when (directive) {
					Forget -> {}
					Watch -> {
						subscribed.addIfNotExists(child)
					}
					Capture -> {
						val cancelEvent = event.cancelEvent
						subscribed.forEach {
							if (it != child) {
								it.handle(cancelEvent)
							}
						}
						subscribed.empty()
						subscribed.add(child)
						return Capture
					}
					Screen.AndForget -> {}
					Screen.AndWatch -> {
						subscribed.addIfNotExists(child)
					}
				}
				if (directive is Screen) {
					screenDirective = directive
					skip
				}
			}
		}
		if (subscribed.isEmpty) {
			if (screenDirective == null) {
				return Forget
			} else {
				return screenDirective!!
			}
		} else {
			if (screenDirective == null) {
				return Watch
			} else {
				return Screen.AndWatch
			}
		}
	}

	override fun onMove (event: TouchEvent.Move) : TouchDirective {
		val subscribed = getSubscribedChildren(touchId = event.touchId)
		subscribed.copy().forEach { child ->
			val directive = child.handle(event)
			when (directive) {
				Forget -> {
					subscribed.removeIfExists(child)
				}
				Watch -> {}
				Capture -> {
					val cancelEvent = event.cancelEvent
					subscribed.forEach {
						if (it != child) {
							it.handle(cancelEvent)
						}
					}
					subscribed.empty()
					subscribed.add(child)
					return Capture
				}
				is Screen -> throw RuntimeException()
			}
		}
		if (subscribed.isEmpty) {
			return Forget
		} else {
			return Watch
		}
	}

	override fun onUp (event: TouchEvent.Up) : TouchDirective {
		val subscribed = getSubscribedChildren(touchId = event.touchId)
		subscribed.copy().forEach { child ->
			val directive = child.handle(event)
			when (directive) {
				Forget -> {}
				Watch -> {}
				Capture -> {
					val cancelEvent = event.cancelEvent
					subscribed.forEach {
						if (it != child) {
							it.handle(cancelEvent)
						}
					}
					subscribed.empty()
					return Capture
				}
				is Screen -> throw RuntimeException()
			}
		}
		subscribed.empty()
		return Forget
	}

	override fun onCancel (event: TouchEvent.Cancel) {
		val subscribed = getSubscribedChildren(touchId = event.touchId)
		subscribed.forEach { it.handle(event) }
		subscribed.empty()
	}


}