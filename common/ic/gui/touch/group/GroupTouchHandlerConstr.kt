package ic.gui.touch.group


import ic.struct.list.List

import ic.gui.ifaces.HasGlobalPosition
import ic.gui.touch.TouchHandler


inline fun GroupTouchHandler (

	children : List<TouchHandler>,

	crossinline isInTouchableArea : (HasGlobalPosition) -> Boolean = { true },

	debugName : String? = null

) : GroupTouchHandler {

	return object : GroupTouchHandler() {

		override val children get() = children

		override val debugName get() = debugName

		override fun isInTouchableArea (event: HasGlobalPosition): Boolean {
			return isInTouchableArea(event)
		}

	}

}


inline fun GroupTouchHandler (

	crossinline initChildren : () -> List<TouchHandler>,

	crossinline isInTouchableArea : (HasGlobalPosition) -> Boolean = { true },

	crossinline getDebugName : () -> String? = { null }

) : GroupTouchHandler {

	return object : GroupTouchHandler() {

		private var childrenField : List<TouchHandler>? = null

		override val children : List<TouchHandler> get() {
			return childrenField ?: initChildren().also { childrenField = it }
		}

		override val debugName get() = getDebugName()

		override fun isInTouchableArea (event: HasGlobalPosition): Boolean {
			return isInTouchableArea(event)
		}

	}

}