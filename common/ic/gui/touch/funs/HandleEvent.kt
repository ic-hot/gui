package ic.gui.touch.funs


import ic.gui.touch.TouchDirective
import ic.gui.touch.TouchDirective.*
import ic.gui.touch.TouchEvent
import ic.gui.touch.TouchEvent.*
import ic.gui.touch.TouchHandler


fun TouchHandler.handle (

	event : TouchEvent

) : TouchDirective {

	return when (event) {

		is Down -> {
			handle(event)
		}

		is Move -> {
			handle(event)
		}

		is Up -> {
			handle(event)
			Forget
		}

		is Cancel -> {
			handle(event)
			Forget
		}

	}


}