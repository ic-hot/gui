package ic.gui.touch


import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp


val touchMoveThreshold : Dp = 4.dp

val touchMoveThresholdSquared: Dp = touchMoveThreshold * touchMoveThreshold