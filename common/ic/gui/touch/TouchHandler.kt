package ic.gui.touch


import ic.base.reflect.ext.simpleClassName
import ic.util.log.logD

import ic.gui.ifaces.HasGlobalPosition
import ic.gui.touch.TouchDirective.Forget


abstract class TouchHandler {


	protected open fun isInTouchableArea (event: HasGlobalPosition) : Boolean = true

	protected open val debugName : String? get() = null


	private fun logIfNeeded (event: TouchEvent, isInTouchableArea: Boolean? = null) {
		val debugName = this.debugName
		if (debugName != null) {
			if (isInTouchableArea == null) {
				logD("gui") { "$debugName $event" }
			} else {
				logD("gui") { "$debugName $event $isInTouchableArea" }
			}
		}
	}

	private fun logIfNeeded (directive: TouchDirective) {
		val debugName = this.debugName
		if (debugName != null) {
			logD("gui") { "$debugName ${ directive.simpleClassName }" }
		}
	}


	protected abstract fun onDown (event: TouchEvent.Down) : TouchDirective

	fun handle (event: TouchEvent.Down) : TouchDirective {
		val isInTouchableArea = isInTouchableArea(event)
		logIfNeeded(event, isInTouchableArea)
		return (
			if (isInTouchableArea) {
				onDown(event)
			} else {
				Forget
			}
		).also {
			logIfNeeded(it)
		}
	}


	protected abstract fun onMove (event: TouchEvent.Move) : TouchDirective

	fun handle (event: TouchEvent.Move) : TouchDirective {
		logIfNeeded(event)
		return onMove(event).also {
			logIfNeeded(it)
		}
	}


	protected abstract fun onUp (event: TouchEvent.Up) : TouchDirective

	fun handle (event: TouchEvent.Up) : TouchDirective {
		logIfNeeded(event)
		return onUp(event).also {
			logIfNeeded(it)
		}
	}


	protected abstract fun onCancel (event: TouchEvent.Cancel)

	fun handle (event: TouchEvent.Cancel) {
		logIfNeeded(event)
		onCancel(event)
	}


	object DoNothing : TouchHandler() {

		override fun onDown (event: TouchEvent.Down) : TouchDirective {
			return Forget
		}

		override fun onMove (event: TouchEvent.Move) : TouchDirective {
			return Forget
		}

		override fun onUp (event: TouchEvent.Up) : TouchDirective {
			return Forget
		}

		override fun onCancel (event: TouchEvent.Cancel) {}

	}


}