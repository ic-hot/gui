package ic.gui.touch.drag


import kotlin.math.absoluteValue

import ic.gui.touch.TouchDirective
import ic.gui.touch.TouchDirective.*
import ic.gui.touch.TouchEvent
import ic.gui.touch.TouchHandler
import ic.gui.touch.touchMoveThresholdSquared


abstract class HorizontalDragHandler : TouchHandler() {


	protected abstract fun onDragStart (event: TouchEvent.Down)

	protected abstract fun onDragMove (event: TouchEvent.Move)

	protected abstract fun onDragEnd (event: TouchEvent.Up)

	protected abstract fun onDragCancel()


	private var downEvent : TouchEvent.Down? = null

	private var isDragStarted : Boolean = false


	override fun onDown (event: TouchEvent.Down) : TouchDirective {
		if (this.downEvent == null) {
			this.downEvent = event
			return Watch
		} else {
			return Forget
		}
	}

	override fun onMove (event: TouchEvent.Move) : TouchDirective {
		val downEvent = this.downEvent
		if (downEvent == null) return Forget
		if (event.touchId != downEvent.touchId) return Forget
		if (isDragStarted) {
			onDragMove(event)
			return Watch
		} else {
			val dx = event.globalX - downEvent.globalX
			val dy = event.globalY - downEvent.globalY
			if (dx * dx + dy * dy < touchMoveThresholdSquared) {
				return Watch
			} else {
				if (dx.absoluteValue > dy.absoluteValue) {
					isDragStarted = true
					onDragStart(downEvent)
					onDragMove(event)
					return Capture
				} else {
					this.downEvent = null
					return Forget
				}
			}
		}
	}

	override fun onUp (event: TouchEvent.Up) : TouchDirective {
		val downEvent = this.downEvent
		if (downEvent == null) return Forget
		if (event.touchId != downEvent.touchId) return Forget
		this.downEvent = null
		if (isDragStarted) {
			onDragEnd(event)
			isDragStarted = false
			return Capture
		} else {
			return Forget
		}
	}

	override fun onCancel (event: TouchEvent.Cancel) {
		val downEvent = this.downEvent
		if (downEvent == null) return
		if (event.touchId != downEvent.touchId) return
		this.downEvent = null
		if (isDragStarted) {
			onDragCancel()
			isDragStarted = false
		}
	}


}