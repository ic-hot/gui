package ic.gui.touch.drag


import ic.gui.touch.TouchEvent


inline fun VerticalDragHandler (

	crossinline onDragStart : (TouchEvent.Down) -> Unit,

	crossinline onDragMove : (TouchEvent.Move) -> Unit,

	crossinline onDragEnd : (TouchEvent.Up) -> Unit,

	crossinline onDragCancel : () -> Unit,

	crossinline getDebugName : () -> String? = { null },

) : VerticalDragHandler {

	return object : VerticalDragHandler() {

		override val debugName get() = getDebugName()

		override fun onDragStart (event: TouchEvent.Down) {
			onDragStart(event)
		}

		override fun onDragMove (event: TouchEvent.Move) {
			onDragMove(event)
		}

		override fun onDragEnd (event: TouchEvent.Up) {
			onDragEnd(event)
		}

		override fun onDragCancel() {
			onDragCancel()
		}

	}

}