package ic.gui.touch.ext


import ic.gui.touch.TouchEvent


val TouchEvent.cancelEvent : TouchEvent.Cancel get() {

	return TouchEvent.Cancel(
		touchId = touchId
	)

}