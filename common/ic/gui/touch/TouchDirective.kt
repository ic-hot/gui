package ic.gui.touch


sealed class TouchDirective {

	object Capture : TouchDirective()

	object Watch : TouchDirective()

	object Forget : TouchDirective()

	sealed class Screen : TouchDirective() {

		object AndForget : Screen()

		object AndWatch : Screen()

	}

}