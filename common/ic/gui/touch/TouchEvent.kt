package ic.gui.touch


import ic.base.primitives.int64.Int64

import ic.gui.dim.dp.Dp
import ic.gui.ifaces.HasGlobalPosition


sealed class TouchEvent {

	abstract val touchId : Int64

	data class Down (
		override val touchId : Int64,
		override val globalX : Dp,
		override val globalY : Dp
	) : TouchEvent(), HasGlobalPosition

	data class Move (
		override val touchId : Int64,
		override val globalX : Dp,
		override val globalY : Dp
	) : TouchEvent(), HasGlobalPosition

	data class Up (
		override val touchId : Int64
	) : TouchEvent()

	data class Cancel (
		override val touchId : Int64
	) : TouchEvent()

}
