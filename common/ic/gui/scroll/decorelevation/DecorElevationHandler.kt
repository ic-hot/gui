package ic.gui.scroll.decorelevation


import ic.gui.dim.dp.Dp


abstract class DecorElevationHandler (

	val maxTopBarElevation    : Dp,
	val maxBottomBarElevation : Dp

) {

	abstract fun setTopBarElevation    (elevation: Dp)
	abstract fun setBottomBarElevation (elevation: Dp)

}