package ic.gui.scroll.decorelevation


import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp


inline fun DecorElevationHandler (

	maxTopBarElevation    : Dp = 8.dp,
	maxBottomBarElevation : Dp = 16.dp,

	crossinline setTopBarElevation    : (Dp) -> Unit = {},
	crossinline setBottomBarElevation : (Dp) -> Unit = {}

) : DecorElevationHandler {

	return object : DecorElevationHandler(
		maxTopBarElevation    = maxTopBarElevation,
		maxBottomBarElevation = maxBottomBarElevation
	) {

		override fun setTopBarElevation    (elevation: Dp) = setTopBarElevation(elevation)
		override fun setBottomBarElevation (elevation: Dp) = setBottomBarElevation(elevation)

	}

}