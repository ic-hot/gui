package ic.gui.font


import ic.storage.res.impl.guiResources
import ic.struct.value.ext.getValue
import ic.struct.value.cached.Cached


interface Font {

	companion object {

		val Default by Cached {
			guiResources.getDefaultFont()
		}

		val DefaultBold by Cached {
			guiResources.getDefaultBoldFont()
		}

		val DefaultMonospace by Cached {
			guiResources.getDefaultMonospaceFont()
		}

	}

}