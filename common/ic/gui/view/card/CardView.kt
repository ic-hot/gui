package ic.gui.view.card


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color

import ic.gui.dim.dp.Dp
import ic.gui.adapters.ViewAdapters
import ic.gui.dim.dp.ext.dp
import ic.gui.ifaces.HasGlobalPosition
import ic.gui.view.View
import ic.gui.view.container.decor.DecoratorView
import ic.gui.view.ext.height
import ic.gui.view.ext.minHeight
import ic.gui.view.ext.minWidth
import ic.gui.view.ext.width


abstract class CardView : DecoratorView() {


	override val subview : View? get() = null


	open val inset : Dp get() = 0.dp

	abstract val backgroundColor : Color

	open val topLeftCornerRadius     : Dp get() = 0.dp
	open val topRightCornerRadius    : Dp get() = 0.dp
	open val bottomLeftCornerRadius  : Dp get() = 0.dp
	open val bottomRightCornerRadius : Dp get() = 0.dp

	open val outlineThickness  : Dp    get() = 0.dp
	open val outlineColor      : Color get() = Color.Transparent
	open val outlineDashLength : Dp    get() = 0.dp

	open val elevation : Dp get() = 0.dp
	open val shadowOpacity : Float32 get() = 1F

	open val blurRadius : Dp get() = 0.dp


	final override val intrinsicMinWidth : Dp get() {
		val child = this.subview ?: return 0.dp
		return child.minWidth + inset * 2
	}

	final override val intrinsicMinHeight : Dp get() {
		val child = this.subview ?: return 0.dp
		return child.minHeight + inset * 2
	}


	final override fun getLocalXOf (subview: View) : Dp {
		return inset
	}

	final override fun getLocalYOf (subview: View) : Dp {
		return inset
	}

	final override fun getWidthOf (subview: View) : Dp {
		return width - inset * 2
	}

	final override fun getHeightOf (subview: View) : Dp {
		return height - inset * 2
	}


	override fun isInTouchableArea (event: HasGlobalPosition) = isInRect(event)


	final override val ViewAdapters<*>.adapter get() = cardViewAdapter
	

}