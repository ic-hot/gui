package ic.gui.view.container.decor.scroll


import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.math.ext.clamp

import ic.gui.anim.dynamic.DynamicValueAnimatorForScrollDp
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.touch.drag.HorizontalDragHandler
import ic.gui.view.View
import ic.gui.view.container.decor.DecoratorView
import ic.gui.view.ext.height
import ic.gui.view.ext.minHeight
import ic.gui.view.ext.minWidth
import ic.gui.view.ext.updatePosition
import ic.gui.view.ext.width


abstract class HorizontalScrollView : DecoratorView() {


	protected open fun onScroll (offset: Dp) {}


	private var isScrollOpen : Boolean = false


	final override fun onOpenContainer() {
		isScrollOpen = true
	}


	override val intrinsicMinWidth : Dp get() = 0.dp

	override val intrinsicMinHeight : Dp get() {
		val child = this.subview ?: return 0.dp
		return child.minHeight
	}


	var scrollOffset : Dp = 0.dp; private set


	override fun getLocalXOf (subview: View) : Dp {
		return -scrollOffset
	}

	override fun getLocalYOf (subview: View) : Dp {
		return 0.dp
	}

	override fun getWidthOf (subview: View) : Dp {
		return subview.minWidth
	}

	override fun getHeightOf (subview: View) : Dp {
		return height
	}


	private val animator = DynamicValueAnimatorForScrollDp { position, _ ->
		val child = subview
		if (isScrollOpen && child != null) {
			// TODO reimplement boundary corrections
			val newScrollOffset = position.clamp(
				0.dp,
				(child.minWidth - width).clamp(from = 0)
			)
			if (newScrollOffset != scrollOffset) {
				scrollOffset = newScrollOffset
				child.updatePosition()
				onScroll(position)
			}
		}
	}


	private var relativeDownOffset : Dp = 0.dp

	final override val topPriorityTouchHandler = HorizontalDragHandler(
		getDebugName = { touchDebugName?.let { "$it.Drag" } },
		onDragStart = { event ->
			relativeDownOffset = scrollOffset - -event.globalX
			animator.seize(scrollOffset)
		},
		onDragMove = { event ->
			animator.move(relativeDownOffset + -event.globalX)
		},
		onDragEnd = {
			animator.release()
		},
		onDragCancel = {
			animator.setStationary()
		}
	)


	final override fun onCloseContainer() {
		isScrollOpen = false
		animator.stopNonBlockingIfNeeded()
	}


}