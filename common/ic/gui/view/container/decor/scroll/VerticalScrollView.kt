package ic.gui.view.container.decor.scroll


import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.math.ext.clamp
import ic.math.funs.max

import ic.gui.anim.dynamic.DynamicValueAnimatorForScrollDp
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.touch.drag.VerticalDragHandler
import ic.gui.view.View
import ic.gui.view.container.decor.DecoratorView
import ic.gui.view.ext.height
import ic.gui.view.ext.minHeight
import ic.gui.view.ext.updatePosition
import ic.gui.view.ext.width
import ic.util.log.logD


abstract class VerticalScrollView : DecoratorView() {


	protected open fun onScroll (offset: Dp) {}


	override val intrinsicMinWidth  get() = 0.dp
	override val intrinsicMinHeight get() = 0.dp


	var scrollOffset : Dp = 0.dp; private set


	override fun getLocalXOf (subview: View) : Dp {
		return 0.dp
	}

	override fun getLocalYOf (subview: View) : Dp {
		 return -scrollOffset
	}

	override fun getWidthOf (subview: View) : Dp {
		return width
	}

	override fun getHeightOf (subview: View) = when (val layoutHeight = subview.layoutHeight) {
		ByContainer -> max(subview.minHeight, height)
		ByContent -> subview.minHeight
		else -> layoutHeight
	}


	private var isScrollOpen : Boolean = false

	final override fun onOpenContainer() {
		isScrollOpen = true
	}


	override fun afterUpdateContainer() {
		/*
		TODO
		val child = this.subview ?: return
		if (isScrollOpen) {
			val newScrollOffset = scrollOffset.clamp(
				0.dp,
				(child.minHeight - height).clamp(from = 0)
			)
			if (newScrollOffset != scrollOffset) {
				scrollOffset = newScrollOffset
				child.updatePosition()
				onScroll(newScrollOffset)
			}
		}
		 */
	}


	private val animator = DynamicValueAnimatorForScrollDp { position, _ ->
		val child = subview
		if (isScrollOpen && child != null) {
			// TODO reimplement boundary corrections
			val newScrollOffset = position.clamp(
				0.dp,
				(child.minHeight - height).clamp(from = 0)
			)
			if (newScrollOffset != scrollOffset) {
				scrollOffset = newScrollOffset
				child.updatePosition()
				onScroll(newScrollOffset)
			}
		}
	}


	private var relativeDownOffset : Dp = 0.dp

	final override val topPriorityTouchHandler = VerticalDragHandler(
		getDebugName = { touchDebugName?.let { "$it.Drag" } },
		onDragStart = { event ->
			relativeDownOffset = scrollOffset - -event.globalY
			animator.seize(scrollOffset)
		},
		onDragMove = { event ->
			animator.move(relativeDownOffset + -event.globalY)
		},
		onDragEnd = {
			animator.release()
		},
		onDragCancel = {
			animator.setStationary()
		}
	)


	fun scrollToTop (toAnimate: Boolean) {
		if (!isScrollOpen) return
		if (toAnimate) {
			animator.setEndPosition(0.dp)
		} else {
			animator.setStationary(0.dp)
		}
	}

	fun scrollToBottom (toAnimate: Boolean) {
		val child = this.subview ?: return
		if (!isScrollOpen) return
		val offset = child.minHeight - height
		if (toAnimate) {
			animator.setEndPosition(offset)
		} else {
			animator.setStationary(offset)
		}
	}

	fun scrollToDescendant (view: View, toAnimate: Boolean) {
		if (!isScrollOpen) return
		val offset = this.globalY - view.globalY
		if (toAnimate) {
			animator.setEndPosition(offset)
		} else {
			animator.setStationary(offset)
		}
	}


	final override fun onCloseContainer() {
		isScrollOpen = false
		animator.stopNonBlockingIfNeeded()
	}


}
