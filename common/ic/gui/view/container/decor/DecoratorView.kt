package ic.gui.view.container.decor


import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.view.View
import ic.gui.view.container.ContainerView


abstract class DecoratorView : ContainerView() {


	abstract val subview : View?


	override val layoutHorizontalAlign : HorizontalAlign get() {
		val child = this.subview ?: return Center
		return child.layoutHorizontalAlign
	}

	override val layoutVerticalAlign : VerticalAlign get() {
		val child = this.subview ?: return Center
		return child.layoutVerticalAlign
	}


	final override fun prepareSubviews() {
		subview?.let { prepareSubview(it) }
	}


}