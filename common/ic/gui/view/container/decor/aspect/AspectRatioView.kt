package ic.gui.view.container.decor.aspect


import ic.base.primitives.float32.Float32

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.View
import ic.gui.view.container.decor.DecoratorView
import ic.gui.view.ext.height
import ic.gui.view.ext.width


abstract class AspectRatioView : DecoratorView() {


	abstract val aspectRatio : Float32


	override val intrinsicMinWidth get() = height * aspectRatio

	override val intrinsicMinHeight get() = width / aspectRatio


	override fun getLocalXOf (subview: View) : Dp {
		return 0.dp
	}

	override fun getLocalYOf (subview: View) : Dp {
		return 0.dp
	}

	override fun getWidthOf (subview: View) : Dp {
		return width
	}

	override fun getHeightOf (subview: View): Dp {
		return height
	}


}