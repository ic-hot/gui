package ic.gui.view.container.decor


import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.View
import ic.gui.view.ext.height
import ic.gui.view.ext.minHeight
import ic.gui.view.ext.minWidth
import ic.gui.view.ext.width


abstract class SizePreservingDecoratorView : DecoratorView() {


	final override val intrinsicMinWidth : Dp get() {
		val child = this.subview ?: return 0.dp
		return child.minWidth
	}

	final override val intrinsicMinHeight : Dp get() {
		val child = this.subview ?: return 0.dp
		return child.minHeight
	}


	final override fun getLocalXOf (subview: View) : Dp {
		return 0.dp
	}

	final override fun getLocalYOf (subview: View) : Dp {
		return 0.dp
	}

	final override fun getWidthOf (subview: View) : Dp {
		return width
	}

	final override fun getHeightOf (subview: View) : Dp {
		return height
	}


}