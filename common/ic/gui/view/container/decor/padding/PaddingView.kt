package ic.gui.view.container.decor.padding


import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.View
import ic.gui.view.container.decor.DecoratorView
import ic.gui.view.ext.height
import ic.gui.view.ext.minHeight
import ic.gui.view.ext.minWidth
import ic.gui.view.ext.width


abstract class PaddingView : DecoratorView() {


	abstract val leftPadding   : Dp
	abstract val rightPadding  : Dp
	abstract val topPadding    : Dp
	abstract val bottomPadding : Dp


	final override val layoutWidth : Dp get() {
		val child = this.subview ?: return 0.dp
		val childLayoutWidth = child.layoutWidth
		when (childLayoutWidth) {
			ByContainer -> return ByContainer
			else        -> return ByContent
		}
	}

	final override val layoutHeight : Dp get() {
		val child = this.subview ?: return 0.dp
		val childLayoutHeight = child.layoutHeight
		when (childLayoutHeight) {
			ByContainer -> return ByContainer
			else        -> return ByContent
		}
	}


	override val intrinsicMinWidth : Dp get() {
		val child = this.subview ?: return 0.dp
		return child.minWidth  + leftPadding + rightPadding
	}

	override val intrinsicMinHeight : Dp get() {
		val child = this.subview ?: return 0.dp
		return child.minHeight + topPadding  + bottomPadding
	}

	override fun getLocalXOf (subview: View) : Dp {
		return leftPadding
	}

	override fun getLocalYOf (subview: View) : Dp {
		return topPadding
	}

	override fun getWidthOf (subview: View) : Dp {
		return width - leftPadding - rightPadding
	}

	override fun getHeightOf (subview: View) : Dp {
		return height - topPadding - bottomPadding
	}


	override val opacity get() = 1F


}