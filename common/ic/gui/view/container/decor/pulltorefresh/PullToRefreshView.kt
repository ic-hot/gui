package ic.gui.view.container.decor.pulltorefresh


import ic.gui.dim.dp.Dp
import ic.gui.view.container.decor.DecoratorView


abstract class PullToRefreshView : DecoratorView() {


	abstract val refreshingOffsetDp : Dp


	abstract val isRefreshing : Boolean


	abstract fun onPullToRefresh()


	abstract fun onOverscrollOffsetChanged (overscroll: Dp)


}