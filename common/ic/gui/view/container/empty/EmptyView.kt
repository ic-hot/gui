package ic.gui.view.container.empty


import ic.gui.dim.dp.Dp
import ic.gui.ifaces.HasGlobalPosition
import ic.gui.view.View
import ic.gui.view.container.ContainerView


abstract class EmptyView : ContainerView() {


	override fun prepareSubviews() {}


	final override fun getLocalXOf (subview: View) : Dp {
		throw RuntimeException()
	}

	final override fun getLocalYOf (subview: View): Dp {
		throw RuntimeException()
	}

	override fun getWidthOf (subview: View) : Dp {
		throw RuntimeException()
	}

	override fun getHeightOf (subview: View): Dp {
		throw RuntimeException()
	}


	override fun isInTouchableArea (event: HasGlobalPosition) = isInRect(event)


}