package ic.gui.view.container.empty.touch


import ic.gui.dim.dp.ext.dp
import ic.gui.ifaces.HasGlobalPosition
import ic.gui.touch.click.ClickHandler
import ic.gui.view.container.empty.EmptyView


abstract class ClickableView : EmptyView() {


	protected abstract val isClickEnabled : Boolean

	protected abstract fun onClick()


	protected abstract val isSecondaryClickEnabled : Boolean

	protected abstract fun onSecondaryClick()


	override val intrinsicMinWidth  get() = 0.dp
	override val intrinsicMinHeight get() = 0.dp


	override fun isInTouchableArea (event: HasGlobalPosition) = isInRect(event)


	final override val topPriorityTouchHandler = ClickHandler(
		getDebugName = {
			touchDebugName?.let { "$it.ClickHandler" }
		},
		isClickEnabled = { isClickEnabled },
		onClick = {
			onClick()
		},
		isSecondaryClickEnabled = { isSecondaryClickEnabled },
		onSecondaryClick = {
			onSecondaryClick()
		}
	)


}