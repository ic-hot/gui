package ic.gui.view.container.empty.touch


import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.ifaces.HasGlobalPosition
import ic.gui.touch.TouchHandler
import ic.gui.view.container.empty.EmptyView


fun Touchable (

	touchHandler : TouchHandler,

	layoutWidth  : Dp = ByContainer,
	layoutHeight : Dp = ByContainer

) : EmptyView {

	return object : EmptyView() {

		override val layoutWidth  get() = layoutWidth
		override val layoutHeight get() = layoutHeight

		override fun isInTouchableArea (event: HasGlobalPosition) = isInRect(event)

		override val topPriorityTouchHandler get() = touchHandler

	}

}