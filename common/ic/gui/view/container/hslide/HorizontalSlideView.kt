package ic.gui.view.container.hslide


import ic.base.primitives.float32.Float32
import ic.base.primitives.int64.Int64
import ic.base.primitives.int64.ext.asFloat32
import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.math.ext.clamp
import ic.math.ext.roundAsInt64
import ic.struct.list.ext.findIndex
import ic.struct.list.List
import ic.struct.map.editable.EditableMap

import ic.gui.anim.dynamic.DynamicValueAnimatorForSwipeDp
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.touch.drag.HorizontalDragHandler
import ic.gui.view.View
import ic.gui.view.container.ContainerView
import ic.gui.view.ext.height
import ic.gui.view.ext.update
import ic.gui.view.ext.width


abstract class HorizontalSlideView<Item> : ContainerView() {


	protected abstract val items : List<Item>

	internal val thisItems get() = items


	protected abstract val isCycle : Boolean

	abstract val currentItemIndex : Int64


	protected open fun onCurrentItemChanged (itemIndex: Int64) {}

	internal fun callOnCurrentItemChanged (itemIndex: Int64) {
		onCurrentItemChanged(itemIndex)
	}


	protected abstract fun ItemViewScope.initSubview() : View


	inner class ItemViewScope internal constructor (val thisItem: Item) {
		internal val subview = initSubview()
	}

	private val scopes = EditableMap<Item, ItemViewScope>()

	private var isViewStateInitialized : Boolean = false

	internal var phase : Float32 = 0F

	private fun scopeFor (item: Item) = scopes[item] ?: ItemViewScope(item).also { scopes[item] = it }


	private var isSlideOpen : Boolean = false

	final override fun onOpenContainer() {
		isSlideOpen = true
		phase = currentItemIndex.asFloat32
	}


	final override fun getLocalXOf (subview: View) : Dp {
		val index = items.findIndex { item ->
			scopeFor(item).subview === subview
		}
		return - (phase - index) * width
	}

	final override fun getLocalYOf (subview: View) : Dp {
		return 0.dp
	}

	final override fun prepareSubviews() {
		val index = currentItemIndex
		run {
			val item = items[index]
			val scope = scopeFor(item)
			prepareSubview(scope.subview)
		}
		if (index > 0) {
			val item = items[index - 1]
			val scope = scopeFor(item)
			prepareSubview(scope.subview)
		}
		if (index < items.count - 1) {
			val item = items[index + 1]
			val scope = scopeFor(item)
			prepareSubview(scope.subview)
		}
	}

	override fun getWidthOf (subview: View) : Dp {
		return width
	}

	override fun getHeightOf (subview: View) : Dp {
		return height
	}


	private val animator = DynamicValueAnimatorForSwipeDp { offset, _ ->
		if (isSlideOpen) {
			// TODO reimplement boundary corrections
			val newPhase = (offset / width).clamp(0, items.count - 1)
			if (newPhase != phase) {
				phase = newPhase
				update()
				val newIndex = phase.roundAsInt64.clamp(0, items.count)
				if (newIndex != currentItemIndex) {
					onCurrentItemChanged(newIndex)
				}
			}
		}
	}


	private var relativeDownOffset : Float32 = 0F

	final override val topPriorityTouchHandler = HorizontalDragHandler(
		getDebugName = { debugName?.let { "$it.Drag" } },
		onDragStart = { event ->
			val currentPosition = phase * width
			relativeDownOffset = currentPosition - -event.globalX
			animator.seize(currentPosition)
		},
		onDragMove = { event ->
			animator.move(relativeDownOffset + -event.globalX)
		},
		onDragEnd = {
			animator.release()
			val endPosition = animator.getEndPosition()
			val width = this.width
			val currentIndex = this.currentItemIndex
			animator.setEndPosition(
				(animator.getEndPosition() / width)
				.roundAsInt64
				.clamp(currentIndex - 1, currentIndex + 2)
				.clamp(0, items.count)
				* width
			)
		},
		onDragCancel = {
			animator.release()
			val width = this.width
			animator.setEndPosition(
				(phase / width).roundAsInt64.clamp(0, items.count) * width
			)
		}
	)


	final override fun onCloseContainer() {
		isViewStateInitialized = false
		isSlideOpen = false
		animator.stopNonBlockingIfNeeded()
	}


}