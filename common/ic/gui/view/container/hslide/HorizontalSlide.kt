package ic.gui.view.container.hslide


import ic.base.primitives.int64.Int64
import ic.struct.list.List

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.view.View


inline fun <Item> HorizontalSlide (

	width : Dp = ByContainer, height : Dp = ByContainer,

	crossinline getItems : () -> List<Item>,

	crossinline getCurrentItemIndex : () -> Int64,

	crossinline initChild : HorizontalSlideView<Item>.ItemViewScope.() -> View,

	crossinline onCurrentItemChanged : (itemIndex: Int64) -> Unit

) : HorizontalSlideView<Item> {

	return object : HorizontalSlideView<Item>() {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val items get() = getItems()

		override val isCycle get() = false

		override val currentItemIndex get() = getCurrentItemIndex()

		override fun ItemViewScope.initSubview() : View {
			return initChild()
		}

		override fun onCurrentItemChanged (itemIndex: Int64) {
			onCurrentItemChanged(itemIndex)
		}

	}

}