package ic.gui.view.container.impl


import ic.gui.view.container.ContainerView


internal inline val ContainerView.thisView get() = this