package ic.gui.view.container


import ic.base.assert.assert
import ic.base.escape.skip.skip
import ic.base.escape.skip.skippable
import ic.base.loop.repeat
import ic.struct.list.Lazy
import ic.struct.list.List
import ic.struct.list.editable.EditableList
import ic.struct.list.ext.foreach.forEach
import ic.struct.list.ext.length.isNullOrEmpty
import ic.struct.list.ext.reduce.find.none

import ic.gui.adapters.ViewAdapter
import ic.gui.adapters.ViewAdapters
import ic.gui.container.ViewContainer
import ic.gui.ifaces.HasGlobalPosition
import ic.gui.touch.TouchHandler
import ic.gui.touch.group.GroupTouchHandler
import ic.gui.view.View
import ic.gui.view.ext.needsToUpdate
import ic.gui.view.ext.updateWithoutImpl


abstract class ContainerView : View(), ViewContainer {


	private var oldSubviews : EditableList<View>? = null
	private var newSubviews : EditableList<View>? = null

	internal val preparedSubviews get() = newSubviews ?: List()

	protected abstract fun prepareSubviews()

	protected fun prepareSubview (subview: View) {
		if (newSubviews == null) newSubviews = EditableList()
		skippable {
			repeat(newSubviews!!.length) { i ->
				if (subview.z < newSubviews!![i].z) {
					newSubviews!!.insert(i, subview)
					skip
				}
			}
			newSubviews!!.add(subview)
		}
	}


	protected open val toAnimateLayoutChanges : Boolean get() = false


	protected open fun onOpenContainer() {}

	final override fun onOpen() {
		assert { oldSubviews.isNullOrEmpty }
		assert { newSubviews.isNullOrEmpty }
		onOpenContainer()
	}


	override fun onNeedsToUpdate() {
		newSubviews?.forEach { subview ->
			subview.needsToUpdate()
		}
	}

	protected open fun beforeUpdateContainer() {}

	protected open fun afterUpdateContainer() {}

	final override fun onUpdate() {

		beforeUpdateContainer()

		newSubviews?.empty()
		prepareSubviews()

		oldSubviews?.forEach { subview ->
			if (newSubviews == null || newSubviews!!.none { it === subview }) {
				subview.close()
			}
		}

		newSubviews?.forEach { subview ->
			if (oldSubviews == null || oldSubviews!!.none { it === subview }) {
				subview.open(container = this, context = context)
				subview.needsToUpdate()
			}
		}

		oldSubviews?.empty()

		newSubviews?.forEach { subview ->
			if (oldSubviews == null) oldSubviews = EditableList()
			oldSubviews!!.add(subview)
		}

		newSubviews?.forEach { subview ->
			subview.updateWithoutImpl()
		}

		afterUpdateContainer()

	}


	override fun isInTouchableArea (event: HasGlobalPosition) = true

	protected open val topPriorityTouchHandler    : TouchHandler get() = TouchHandler.DoNothing
	protected open val bottomPriorityTouchHandler : TouchHandler get() = TouchHandler.DoNothing

	final override val touchHandler = GroupTouchHandler(
		getDebugName = { touchDebugName },
		isInTouchableArea = { isInTouchableArea(it) },
		initChildren = {
			List.Lazy(
				getLength = { preparedSubviews.length + 2 }
			) { index ->
				when (index) {
					0L                          -> topPriorityTouchHandler
					preparedSubviews.length + 1 -> bottomPriorityTouchHandler
					else -> {
						val childIndex = preparedSubviews.length - 1 - (index - 1)
						preparedSubviews[childIndex].touchHandler
					}
				}
			}
		}
	)


	override val ViewAdapters<*>.adapter : ViewAdapter<out ContainerView, *>
		get() = containerViewAdapter
	;


	protected open fun onCloseContainer() {}

	final override fun onClose() {
		onCloseContainer()
		newSubviews?.forEach { it.close() }
		newSubviews?.empty()
		oldSubviews?.empty()
	}


}