package ic.gui.view.container.group.column


import ic.math.funs.max
import ic.struct.list.ext.reduce.minmax.maxFloat32
import ic.struct.list.ext.reduce.sum.sumOf
import ic.struct.list.ext.findIndex
import ic.util.log.logD

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.View
import ic.gui.view.container.group.GroupView
import ic.gui.view.ext.height
import ic.gui.view.ext.minHeight
import ic.gui.view.ext.minWidth
import ic.gui.view.ext.width


abstract class ColumnView : GroupView() {


	override val intrinsicMinWidth get() = preparedSubviews.maxFloat32 { it.minWidth }

	override val intrinsicMinHeight get() = preparedSubviews.sumOf { it.minHeight }


	override fun getLocalXOf (subview: View) : Dp {
		return (width - subview.width) * subview.layoutHorizontalAlign
	}

	override fun getLocalYOf (subview: View) : Dp {
		val childIndex = preparedSubviews.findIndex { it == subview }
		if (childIndex == 0L) {
			return 0.dp
		} else {
			val previousChild = preparedSubviews[childIndex - 1]
			return previousChild.localY + previousChild.height
		}
	}

	override fun getWidthOf (subview: View) = when (val layoutWidth = subview.layoutWidth) {
		ByContent -> subview.minWidth
		ByContainer -> width
		else -> layoutWidth
	}

	override fun getHeightOf (subview: View) = when (val layoutHeight = subview.layoutHeight) {
		ByContent -> subview.minHeight
		ByContainer -> {
			val totalWeight = preparedSubviews.sumOf {
				if (it.layoutHeight == ByContainer) {
					it.layoutWeight
				} else {
					0F
				}
			}
			val contentMinHeight = preparedSubviews.sumOf {
				if (it.layoutHeight == ByContainer) {
					0F
				} else {
					it.minHeight
				}
			}
			val totalFreeSpace = max(0, height - contentMinHeight)
			if (debugName != null) {
				logD("gui") { "$debugName totalFreeSpace: $totalFreeSpace" }
			}
			totalFreeSpace * subview.layoutWeight / totalWeight
		}
		else -> layoutHeight
	}


}