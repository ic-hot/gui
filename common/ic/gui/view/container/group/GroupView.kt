package ic.gui.view.container.group


import ic.struct.list.List
import ic.struct.list.ext.foreach.forEach

import ic.gui.view.View
import ic.gui.view.container.ContainerView


abstract class GroupView : ContainerView() {


	protected abstract val children : List<View>


	override fun prepareSubviews() {
		children.forEach { subview ->
			prepareSubview(subview)
		}
	}


}