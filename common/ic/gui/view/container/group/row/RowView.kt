package ic.gui.view.container.group.row


import ic.struct.list.ext.findIndex
import ic.struct.list.ext.reduce.minmax.maxFloat32
import ic.struct.list.ext.reduce.sum.sumOf

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.View
import ic.gui.view.container.group.GroupView
import ic.gui.view.ext.height
import ic.gui.view.ext.minHeight
import ic.gui.view.ext.minWidth
import ic.gui.view.ext.width


abstract class RowView : GroupView() {


	override val intrinsicMinWidth get() = preparedSubviews.sumOf { it.minWidth }

	override val intrinsicMinHeight get() = preparedSubviews.maxFloat32 { it.minHeight }


	override fun getLocalXOf (subview: View) : Dp {
		val childIndex = preparedSubviews.findIndex { it == subview }
		if (childIndex == 0L) {
			return 0.dp
		} else {
			val previousChild = preparedSubviews[childIndex - 1]
			return previousChild.localX + previousChild.width
		}
	}

	override fun getLocalYOf (subview: View) : Dp {
		return (height - subview.height) * subview.layoutVerticalAlign
	}

	override fun getWidthOf (subview: View) = when (val layoutWidth = subview.layoutWidth) {
		ByContent -> subview.minWidth
		ByContainer -> {
			val totalWeight = preparedSubviews.sumOf {
				if (it.layoutWidth == ByContainer) {
					it.layoutWeight
				} else {
					0F
				}
			}
			val totalChildrenMinWidth = preparedSubviews.sumOf {
				if (it.layoutWidth == ByContainer) {
					0F
				} else {
					it.minWidth
				}
			}
			val totalFreeSpace = width - totalChildrenMinWidth
			totalFreeSpace * subview.layoutWeight / totalWeight
		}
		else -> layoutWidth
	}

	override fun getHeightOf (subview: View) = when (val layoutHeight = subview.layoutHeight) {
		ByContent -> subview.minHeight
		ByContainer -> height
		else -> layoutHeight
	}


}