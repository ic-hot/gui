package ic.gui.view


import ic.base.assert.assert
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.Int32
import ic.base.primitives.int64.Int64
import ic.base.reflect.ext.className
import ic.design.task.scope.BaseTaskScope
import ic.ifaces.lifecycle.closeable.Closeable
import ic.util.log.logD

import ic.graphics.color.Color

import ic.gui.adapters.ViewAdapter
import ic.gui.adapters.ViewAdapters
import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.container.ViewContainer
import ic.gui.view.context.ViewContext
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.ifaces.HasGlobalPosition
import ic.gui.ifaces.ReferenceFrame
import ic.gui.scope.ViewScope
import ic.gui.scope.proxy.ProxyViewScope
import ic.gui.touch.TouchHandler
import ic.gui.view.ext.getLocalX
import ic.gui.view.ext.getLocalY
import ic.gui.view.ext.height
import ic.gui.view.ext.width
import ic.gui.view.impl.thisAdapter
import ic.gui.view.impl.updateImplSelf


abstract class View : BaseTaskScope(), ReferenceFrame, Closeable, ProxyViewScope {


	// Container:

	private var thisContainer : ViewContainer? = null

	val container get() = thisContainer!!


	// Scope:

	private var thisContext : ViewContext? = null

	internal val context get() = thisContext!!

	val scope : ViewScope get() = context

	override val sourceViewScope get() = thisContext!!


	// Adapter:

	protected abstract val ViewAdapters<*>.adapter : ViewAdapter<*, *>

	@Suppress("UNCHECKED_CAST")
	open val adapter get() = context.thisAdapters.adapter as ViewAdapter<*, Any>

	private var preparedImpl : Any? = null

	internal val impl get() = (
		preparedImpl ?:
		thisAdapter.run { initImpl() }
		.also { this.preparedImpl = it }
	)

	internal var implLocalX : Dp = Dp.NaN
	internal var implLocalY : Dp = Dp.NaN

	internal var implWidth  : Dp = Dp.NaN
	internal var implHeight : Dp = Dp.NaN

	internal var implUpdatedVersion : Int64 = 0


	// Open:

	protected open fun onOpen () {}

	fun open (
		container : ViewContainer,
		context : ViewContext
	) {
		if (debugName != null) {
			logD("gui") { "$debugName open" }
		}
		assert { this.thisContainer == null }
		this.thisContainer = container
		assert { this.thisContext == null }
		this.thisContext = context
		onOpen()
	}

	override val isOpen get() = thisContainer != null


	// Layout params:

	abstract val layoutWidth  : Dp
	abstract val layoutHeight : Dp

	open val layoutWeight : Dp get() = 1F

	open val layoutHorizontalAlign : HorizontalAlign get() = Center
	open val layoutVerticalAlign   : VerticalAlign   get() = Center


	// Local position:

	protected open val intrinsicLocalX : Dp get() = container.getLocalXOf(this)
	protected open val intrinsicLocalY : Dp get() = container.getLocalYOf(this)

	internal val thisIntrinsicLocalX get() = intrinsicLocalX
	internal val thisIntrinsicLocalY get() = intrinsicLocalY

	internal var positionUpdateVersion : Int64 = 0

	internal var localXCacheVersion : Int64 = 0
	internal var localYCacheVersion : Int64 = 0

	internal var cachedLocalX : Dp = 0.dp
	internal var cachedLocalY : Dp = 0.dp

	val localX : Dp get() = getLocalX()
	val localY : Dp get() = getLocalY()

	open val z : Int32 get() = 0


	// Global position:

	final override val globalX : Dp get() = container.globalX + localX
	final override val globalY : Dp get() = container.globalY + localY


	// Min size:

	open val intrinsicMinWidth : Dp get() = run {
		updateImplSelf()
		thisAdapter.getIntrinsicMinWidth(this, impl).also {
			if (debugName != null) {
				logD("gui") { "$debugName intrinsicMinWidth: $it" }
			}
		}
	}

	open val intrinsicMinHeight : Dp get() = run {
		updateImplSelf()
		thisAdapter.getIntrinsicMinHeight(this, impl).also {
			if (debugName != null) {
				logD("gui") { "$debugName intrinsicMinHeight: $it" }
			}
		}
	}

	internal val thisIntrinsicMinWidth  get() = intrinsicMinWidth
	internal val thisIntrinsicMinHeight get() = intrinsicMinHeight

	internal var minWidthCacheVersion  : Int64 = 0
	internal var minHeightCacheVersion : Int64 = 0

	internal var cachedMinWidth  : Dp = Dp.NaN
	internal var cachedMinHeight : Dp = Dp.NaN


	// Size:

	protected open val intrinsicWidth get() = container.getWidthOf(this)

	internal val thisIntrinsicWidth get() = intrinsicWidth

	protected open val intrinsicHeight get() = container.getHeightOf(this)

	internal val thisIntrinsicHeight get() = intrinsicHeight

	internal var widthCacheVersion  : Int64 = 0
	internal var heightCacheVersion : Int64 = 0

	internal var cachedWidth  : Dp = Dp.NaN
	internal var cachedHeight : Dp = Dp.NaN

	internal var cachedMaxWidthForWidth   : Dp = Dp.NaN
	internal var cachedMaxHeightForHeight : Dp = Dp.NaN


	// Other parameters:

	open val opacity : Float32 get() = 1F

	open val translationX : Dp get() = 0.dp
	open val translationY : Dp get() = 0.dp

	open val scaleX : Float32 get() = 1F
	open val scaleY : Float32 get() = 1F

	open val rotationDeg : Float32 get() = 0F


	// Update:

	protected open fun onNeedsToUpdate() {}

	internal fun callOnNeedsToUpdate() = onNeedsToUpdate()

	protected open fun onUpdate() {}

	internal fun callOnUpdate() = onUpdate()

	var updateVersion : Int64 = 0; internal set

	internal var updatedVersion : Int64 = 0


	// Touch:

	open val touchExtraRadius : Dp get() = 0.dp

	protected fun isInRect (point: HasGlobalPosition) : Boolean {
		val localX = point.localX
		val localY = point.localY
		val width = this.width
		val height = this.height
		return when {
			localX < -touchExtraRadius -> false
			localY < -touchExtraRadius -> false
			localX >= width  + touchExtraRadius -> false
			localY >= height + touchExtraRadius -> false
			else -> true
		}.also { result ->
			val touchDebugName = this.touchDebugName
			if (touchDebugName != null) {
				logD("gui") {
					"$touchDebugName isInTouchableArea " +
					"localX: $localX, localY: $localY, " +
					"width: $width, height: $height " +
					"-> $result"
				}
			}
		}
	}

	protected open fun isInTouchableArea (event: HasGlobalPosition) : Boolean = isInRect(event)

	open val touchHandler : TouchHandler get() = TouchHandler.DoNothing


	// Close:

	protected open fun onClose() {}

	final override fun close() {
		if (debugName != null) {
			logD("gui") { "$debugName close" }
		}
		assert { thisContainer != null }
		assert { thisContext != null }
		cancelTasks()
		onClose()
		thisContainer = null
		thisContext = null
		preparedImpl = null
		updateVersion = 0
		updatedVersion = 0
		positionUpdateVersion = 0
		widthCacheVersion = 0
		heightCacheVersion = 0
		cachedWidth = Dp.NaN
		cachedHeight = Dp.NaN
		localXCacheVersion = 0
		localYCacheVersion = 0
		cachedLocalX = Dp.NaN
		cachedLocalY = Dp.NaN
		minWidthCacheVersion = 0
		minHeightCacheVersion = 0
		implUpdatedVersion = 0
		implLocalX = Dp.NaN
		implLocalY = Dp.NaN
		implWidth = Dp.NaN
		implHeight = Dp.NaN
	}


	open val debugName : String? get() = null
	open val debugColor : Color? get() = null

	open val touchDebugName : String? get() = null


}