package ic.gui.view.context


import ic.base.escape.skip.skip
import ic.base.escape.skip.skippable
import ic.base.loop.loop
import ic.gui.adapters.ViewAdapters
import ic.gui.container.ViewContainer
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ViewScope
import ic.gui.scope.ViewScope.IfAlreadyUpdating.*
import ic.gui.view.View
import ic.gui.view.ext.update
import ic.util.log.logD


abstract class ViewContext : ViewScope, ViewContainer {


	protected abstract val rootMaxWidth  : Dp
	protected abstract val rootMaxHeight : Dp

	protected abstract val adapters : ViewAdapters<*>

	internal val thisAdapters get() = adapters


	private var view : View? = null


	fun open (view: View) {
		this.view = view
		view.open(container = this, context = this)
	}


	final override val globalX get() = 0.dp
	final override val globalY get() = 0.dp

	final override fun getLocalXOf (subview: View) = 0.dp
	final override fun getLocalYOf (subview: View) = 0.dp

	final override fun getWidthOf  (subview: View) = rootMaxWidth
	final override fun getHeightOf (subview: View) = rootMaxHeight


	protected open fun afterUpdate() {}

	private var areViewsUpdating          : Boolean = false
	private var areViewsNeedToUpdateAgain : Boolean = false

	override fun updateAllViews (ifAlreadyUpdating: ViewScope.IfAlreadyUpdating) {
		if (areViewsUpdating) {
			when (ifAlreadyUpdating) {
				Ignore -> {}
				UpdateAgain -> {
					areViewsNeedToUpdateAgain = true
				}
			}
			return
		}
		areViewsUpdating = true
		try {
			skippable {
				loop {
					areViewsNeedToUpdateAgain = false
					val view = this.view ?: return
					view.update()
					afterUpdate()
					if (!areViewsNeedToUpdateAgain) skip
				}
			}
		} finally {
			areViewsUpdating = false
		}
	}


	fun close() {
		view!!.close()
		view = null
	}


	override fun openPhoneDialer (phoneNumber: String) {
		openUrl("tel://$phoneNumber")
	}

	override fun openEmailSender (email: String) {
		openUrl("mailto:$email")
	}


}