package ic.gui.view.ext


import ic.util.log.logD

import ic.gui.view.View
import ic.gui.view.impl.updateImplPosition
import ic.gui.view.impl.updateImplSelf
import ic.gui.view.impl.updateImplSize


internal fun View.updateImpl () {

	if (debugName != null) logD("gui") { "$debugName updateImpl" }

	updateImplSelf()
	updateImplPosition()
	updateImplSize()

}