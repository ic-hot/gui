package ic.gui.view.ext


import ic.gui.view.View
import ic.gui.view.impl.updateImplPosition


internal fun View.updatePosition() {

	if (!isOpen) return

	positionUpdateVersion++

	updateImplPosition()

}