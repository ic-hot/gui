package ic.gui.view.ext


import ic.gui.view.View


fun View.needsToUpdate() {

	positionUpdateVersion++
	updateVersion++

	callOnNeedsToUpdate()

}