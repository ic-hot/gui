package ic.gui.view.ext


import ic.util.log.logD

import ic.gui.view.View
import ic.gui.view.impl.updateImplPosition
import ic.gui.view.impl.updateImplSelf
import ic.gui.view.impl.updateImplSize
import ic.gui.view.impl.updateSelfOnly


fun View.update() {

	if (debugName != null) {
		logD("gui") { "$debugName update" }
	}

	needsToUpdate()

	updateSelfOnly()

	updateImplPosition()
	updateImplSize()
	updateImplSelf()

}