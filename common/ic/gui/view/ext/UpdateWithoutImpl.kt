package ic.gui.view.ext


import ic.util.log.logD

import ic.gui.view.View
import ic.gui.view.impl.updateSelfOnly


internal fun View.updateWithoutImpl() {

	if (debugName != null) logD("gui") { "$debugName updateWithoutImpl" }

	updateSelfOnly()

}