package ic.gui.view.ext


import ic.util.log.logD

import ic.gui.dim.dp.Dp
import ic.gui.view.View


val View.width : Dp get() {
	val updateVersion = this.updateVersion
	val isUpdateNeeded = (
		if (widthCacheVersion == 0L) {
			true
		} else {
			if (widthCacheVersion == updateVersion) {
				false
			} else {
				true
			}
		}
	)
	if (isUpdateNeeded) {
		widthCacheVersion = updateVersion
		cachedWidth = thisIntrinsicWidth
		if (debugName != null) logD("gui") { "$debugName width: $cachedWidth" }
	}
	return cachedWidth
}


val View.height : Dp get() {
	val updateVersion = this.updateVersion
	val isUpdateNeeded = (
		if (heightCacheVersion == 0L) {
			true
		} else {
			if (heightCacheVersion == updateVersion) {
				false
			} else {
				true
			}
		}
	)
	if (isUpdateNeeded) {
		heightCacheVersion = updateVersion
		cachedHeight = thisIntrinsicHeight
		if (debugName != null) logD("gui") { "$debugName height: $cachedHeight" }
	}
	return cachedHeight
}