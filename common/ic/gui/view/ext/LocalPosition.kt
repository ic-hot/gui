package ic.gui.view.ext


import ic.gui.dim.dp.Dp
import ic.gui.view.View


internal fun View.getLocalX() : Dp {

	val positionUpdateVersion = this.positionUpdateVersion

	val isUpdateNeeded = (
		if (localXCacheVersion == 0L) {
			true
		} else {
			if (localXCacheVersion == positionUpdateVersion) {
				false
			} else {
				true
			}
		}
	)

	if (isUpdateNeeded) {
		localXCacheVersion = positionUpdateVersion
		cachedLocalX = thisIntrinsicLocalX + translationX
	}

	return cachedLocalX

}


internal fun View.getLocalY() : Dp {

	val positionUpdateVersion = this.positionUpdateVersion

	val isUpdateNeeded = (
		if (localYCacheVersion == 0L) {
			true
		} else {
			if (localYCacheVersion == positionUpdateVersion) {
				false
			} else {
				true
			}
		}
	)

	if (isUpdateNeeded) {
		localYCacheVersion = positionUpdateVersion
		cachedLocalY = thisIntrinsicLocalY + translationY
	}

	return cachedLocalY

}