package ic.gui.view.ext


import ic.util.log.logD

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.view.View


val View.minWidth : Dp get() {
	val isUpdateNeeded = (
		if (minWidthCacheVersion == 0L) {
			true
		} else {
			if (minWidthCacheVersion == updateVersion) {
				false
			} else {
				true
			}
		}
	)
	if (isUpdateNeeded) {
		minWidthCacheVersion = updateVersion
		cachedMinWidth = when (val layoutWidth = this.layoutWidth) {
			ByContainer, ByContent -> thisIntrinsicMinWidth
			else                   -> layoutWidth
		}
		if (debugName != null) {
			logD("gui") { "$debugName minWidth: $cachedMinWidth" }
		}
	}
	return cachedMinWidth
}


val View.minHeight : Dp get() {
	if (debugName != null) {
		logD("gui") {
			"$debugName minHeight " +
			"updateVersion: $updateVersion, " +
			"minHeightCacheVersion: $minHeightCacheVersion"
		}
	}
	val isUpdateNeeded = (
		if (minHeightCacheVersion == 0L) {
			true
		} else {
			if (minHeightCacheVersion == updateVersion) {
				false
			} else {
				true
			}
		}
	)
	if (isUpdateNeeded) {
		minHeightCacheVersion = updateVersion
		cachedMinHeight = when (val layoutHeight = this.layoutHeight) {
			ByContainer, ByContent -> thisIntrinsicMinHeight
			else                   -> layoutHeight
		}
		if (debugName != null) {
			logD("gui") { "$debugName minHeight recalculated: $cachedMinHeight" }
		}
	}
	return cachedMinHeight
}