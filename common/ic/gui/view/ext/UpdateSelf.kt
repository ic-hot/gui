package ic.gui.view.ext


import ic.gui.view.View
import ic.gui.view.impl.updateImplSelf
import ic.gui.view.impl.updateSelfOnly


internal fun View.updateSelf() {

	if (!isOpen) return

	needsToUpdate()

	updateSelfOnly()
	updateImplSelf()

}