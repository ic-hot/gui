package ic.gui.view.simple.tumbler


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.ext.dp
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.scope.ext.views.clickable.Clickable
import ic.gui.scope.ext.views.hideable.Hideable
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.row.Row
import ic.gui.scope.ext.views.space.Space


fun Tumbler (

	activeBackgroundColor : Color,
	inactiveBackgroundColor : Color,
	activeColor   : Color,
	inactiveColor : Color,

	outlineColor : Color = Color.Transparent,
	outlineThickness : Dp = 0.dp,

	getState : () -> Boolean,
	onStateChange : (newState: Boolean) -> Unit

) = run {

	Clickable(
		onClick = { onStateChange(!getState()) },
		child = Material(
			cornersRadius = ((16 + 6 * 2 + 1 * 2) / 2).dp,
			getBackgroundColor = {
				if (getState()) {
					activeBackgroundColor
				} else {
					inactiveBackgroundColor
				}
			},
			outlineColor = outlineColor,
			outlineThickness = outlineThickness,
			child = Padding(
				padding = 6.dp,
				child = Row(
					width = ByContent, height = ByContent,
					children = List(
						Hideable(
							isVisible = { getState() == true },
							child = Space(width = 16.dp)
						),
						Material(
							getBackgroundColor = {
								if (getState()) {
									activeColor
								} else {
									inactiveColor
								}
							},
							cornersRadius = 8.dp,
							child = Space(width = 16.dp, height = 16.dp)
						),
						Hideable(
							isVisible = { getState() == false },
							child = Space(width = 16.dp)
						)
					)
				)
			)
		)
	)

}