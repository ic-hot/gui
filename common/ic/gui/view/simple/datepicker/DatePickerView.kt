package ic.gui.view.simple.datepicker


import ic.util.time.Time

import ic.gui.adapters.ViewAdapters
import ic.gui.view.simple.SimpleView


abstract class DatePickerView (

	val style : Style?

) : SimpleView() {


	abstract val value : Time?


	protected abstract fun onValueChanged (value: Time?)

	internal fun callOnValueChanged (value: Time?) {
		onValueChanged(value)
	}


	override val ViewAdapters<*>.adapter get() = datePickerViewAdapter


	sealed class Style {
		object Calendar : Style()
		object Spinners : Style()
	}


}