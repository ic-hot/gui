package ic.gui.view.simple.image


import ic.base.primitives.int64.ext.asInt32

import ic.graphics.color.Color
import ic.graphics.image.Image

import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.adapters.ViewAdapters
import ic.gui.dim.px.ext.inDp
import ic.gui.view.ext.height
import ic.gui.view.ext.width
import ic.gui.view.simple.SimpleView


abstract class ImageView : SimpleView() {


	abstract val image : Image?

	abstract val tint : Color?

	abstract val scaleMode : ScaleMode

	abstract val toUseAntialiasing : Boolean


	override val intrinsicMinWidth : Dp get() = run {
		val image = this.image
		if (image == null) {
			0.dp
		} else {
			if (layoutWidth == ByContent && layoutHeight != ByContent) {
				val imageHeight = image.height
				if (imageHeight == 0L) {
					0.dp
				} else {
					height / imageHeight * image.width
				}
			} else {
				image.width.asInt32.inDp
			}
		}
	}

	override val intrinsicMinHeight : Dp get() = run {
		val image = this.image
		if (image == null) {
			0.dp
		} else {
			if (layoutWidth != ByContent && layoutHeight == ByContent) {
				val imageWidth = image.width
				if (imageWidth == 0L) {
					0.dp
				} else {
					width / imageWidth * image.height
				}
			} else {
				image.height.asInt32.inDp
			}
		}
	}


	override val ViewAdapters<*>.adapter get() = imageViewAdapter


}