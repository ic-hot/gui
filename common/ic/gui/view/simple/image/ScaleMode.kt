package ic.gui.view.simple.image


sealed class ScaleMode {

	object Crop : ScaleMode()

	object Fit : ScaleMode()

	object Stretch : ScaleMode()

}