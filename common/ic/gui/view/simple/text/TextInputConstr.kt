package ic.gui.view.simple.text


inline fun TextInput (

	type : TextInput.Type = TextInput.Type.CommonText,

	isAllCaps : Boolean = false,

	toFocusByDefault : Boolean = false,

	onOkayAction : OnOkayAction = OnOkayAction.HideKeyboard,

	crossinline onTextChange : (String) -> Unit,

) : TextInput {

	return object : TextInput() {
		override val type get() = type
		override val isAllCaps get() = isAllCaps
		override fun onTextChange (newText: String) {
			onTextChange(newText)
		}
		override val toFocusByDefault get() = toFocusByDefault
		override val onOkayAction get() = onOkayAction
	}

}


inline fun TextInput (

	crossinline getType : () -> TextInput.Type?,

	toFocusByDefault : Boolean = false,

	isAllCaps : Boolean = false,

	onOkayAction : OnOkayAction = OnOkayAction.HideKeyboard,

	crossinline onTextChange : (String) -> Unit,

) : TextInput {

	return object : TextInput() {
		override val type get() = getType()
		override val isAllCaps get() = isAllCaps
		override fun onTextChange (newText: String) {
			onTextChange(newText)
		}
		override val toFocusByDefault get() = toFocusByDefault
		override val onOkayAction get() = onOkayAction
	}

}


inline fun TextInput (

	crossinline getType : () -> TextInput.Type,

	isAllCaps : Boolean = false,

	crossinline toFocusByDefault : () -> Boolean,

	crossinline getOnOkayAction : () -> OnOkayAction,

	crossinline onTextChange : (String) -> Unit,

) : TextInput {

	return object : TextInput() {
		override val type get() = getType()
		override val isAllCaps get() = isAllCaps
		override fun onTextChange (newText: String) {
			onTextChange(newText)
		}
		override val toFocusByDefault get() = toFocusByDefault()
		override val onOkayAction get() = getOnOkayAction()
	}

}