package ic.gui.view.simple.text.impl.adapter


import ic.gui.adapters.ViewAdapter
import ic.gui.view.simple.text.TextView


interface TextViewAdapter<Impl: Any> : ViewAdapter<TextView, Impl> {


	fun focus (impl: Impl)

	fun unfocus (impl: Impl)


}