package ic.gui.view.simple.text


import ic.base.primitives.int32.Int32
import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.align.HorizontalAlign
import ic.gui.dim.dp.Dp
import ic.gui.font.Font
import ic.gui.adapters.ViewAdapters
import ic.gui.touch.TouchHandler
import ic.gui.touch.click.ClickHandler
import ic.gui.view.simple.SimpleView
import ic.gui.view.simple.text.impl.adapter.TextViewAdapter
import ic.gui.view.simple.text.span.Span


abstract class TextView : SimpleView() {


	abstract val textHorizontalAlign : HorizontalAlign


	abstract val maxLength : Int32

	abstract val maxLinesCount : Int32


	abstract val toEllipsize : Boolean

	abstract val toUnderscore : Boolean

	abstract val toStrikeThrough : Boolean

	open val toSpellCheck : Boolean get () = true


	abstract val size : Dp

	abstract val font : Font

	abstract val lineSpacingExtra : Dp

	abstract val color : Color


	abstract val text : String

	abstract val spans : List<Span>


	abstract val isSelectable : Boolean

	abstract val input : TextInput?


	protected open fun onOpenText() {}

	final override fun onOpen() {
		onOpenText()
	}


	protected open fun onCloseText() {}

	final override fun onClose() {
		hasFocus = false
	}


	var hasFocus : Boolean = false; private set

	protected open fun onFocusStateChanged() {}

	protected open fun onFocus() {}

	internal fun notifyFocus() {
		hasFocus = true
		onFocus()
		onFocusStateChanged()
	}

	protected open fun onUnfocus() {}

	internal fun notifyUnfocus() {
		if (isOpen) {
			hasFocus = false
			onUnfocus()
			onFocusStateChanged()
		}
	}


	fun focus() {
		adapter.focus(impl)
	}

	fun unfocus() {
		adapter.unfocus(impl)
	}


	private var cachedTouchHandler : TouchHandler? = null

	final override val touchHandler : TouchHandler get() {
		if (cachedTouchHandler == null) {
			cachedTouchHandler = ClickHandler(
				isInTouchableArea = { isInTouchableArea(it) },
				isClickEnabled = { input != null },
				onClick = {
					focus()
				}
			)
		}
		return cachedTouchHandler!!
	}


	override val ViewAdapters<*>.adapter get() = textViewAdapter

	override val adapter get() = super.adapter as TextViewAdapter<Any>


}