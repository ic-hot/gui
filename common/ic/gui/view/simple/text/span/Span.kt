package ic.gui.view.simple.text.span


import ic.base.primitives.int32.Int32

import ic.graphics.color.Color


abstract class Span {

	abstract val startCharIndex : Int32
	abstract val endCharIndex   : Int32

	abstract val color : Color?

}