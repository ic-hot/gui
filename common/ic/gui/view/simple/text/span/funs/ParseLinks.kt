package ic.gui.view.simple.text.span.funs


import ic.base.escape.breakable.Break
import ic.base.loop.breakableRepeat
import ic.base.primitives.int32.Int32
import ic.base.strings.ext.replaceAll
import ic.ifaces.action.Action
import ic.struct.list.List
import ic.struct.list.editable.EditableList

import ic.graphics.color.Color

import ic.gui.view.simple.text.span.Link


fun parseLinks (

	markedString : String,

	linkStartMark : String,
	linkEndMark : String,

	linkColor : Color,

	linksActions : List<Action>

) : Pair<String, List<Link>> {

	val links = EditableList<Link>()

	val visibleString = markedString.replaceAll(linkStartMark to "", linkEndMark to "")

	var charIndex = 0
	var removedTagsLength : Int32 = 0

	breakableRepeat(linksActions.length) { linkIndex ->

		val rawStart = markedString.indexOf(linkStartMark, startIndex = charIndex)

		if (rawStart < 0) Break()

		val start = rawStart - removedTagsLength

		removedTagsLength += linkStartMark.length

		charIndex = rawStart + linkStartMark.length

		val rawEnd = markedString.indexOf(linkEndMark, startIndex = charIndex)

		val end = rawEnd - removedTagsLength

		removedTagsLength += linkEndMark.length

		charIndex = rawEnd + linkEndMark.length

		links.add(
			Link(
				startCharIndex = start, endCharIndex = end,
				color = linkColor,
				onClick = linksActions[linkIndex]::run
			)
		)

	}

	return Pair(visibleString, links)

}