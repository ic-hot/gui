package ic.gui.view.simple.text.span


abstract class Link : Span() {

	abstract fun onClick()

}