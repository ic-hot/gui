package ic.gui.view.simple.text.span


import ic.base.primitives.int32.Int32

import ic.graphics.color.Color


inline fun Link (

	startCharIndex : Int32,
	endCharIndex   : Int32,

	color : Color?,

	crossinline onClick : () -> Unit

) : Link {

	return object : Link() {

		override val color get() = color

		override val startCharIndex get() = startCharIndex
		override val endCharIndex   get() = endCharIndex

		override fun onClick() {
			onClick()
		}

	}

}