package ic.gui.view.simple.text.span


import ic.base.primitives.int32.Int32

import ic.graphics.color.Color


@Suppress("NOTHING_TO_INLINE")
inline fun Span (

	startCharIndex : Int32,
	endCharIndex   : Int32,

	color : Color

) : Span {

	return object : Span() {

		override val startCharIndex get() = startCharIndex
		override val endCharIndex   get() = endCharIndex

		override val color get() = color

	}

}