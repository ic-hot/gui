package ic.gui.view.simple.text


sealed class OnOkayAction {

	abstract fun onOkay()

	object HideKeyboard : OnOkayAction() {
		override fun onOkay() {}
	}

	abstract class Send : OnOkayAction()

	abstract class Done : OnOkayAction()

	companion object {

		inline fun Send (
			crossinline onOkay : () -> Unit
		) : Send {
			return object : Send() {
				override fun onOkay() = onOkay()
			}
		}

		inline fun Done (
			crossinline onOkay : () -> Unit
		) : Done {
			return object : Done() {
				override fun onOkay() = onOkay()
			}
		}

	}

}