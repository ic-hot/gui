package ic.gui.view.simple.text


abstract class TextInput {

	abstract val type : Type?

	abstract val isAllCaps : Boolean

	abstract fun onTextChange (newText: String)

	abstract val onOkayAction : OnOkayAction

	abstract val toFocusByDefault : Boolean

	sealed class Type {
		object CommonText  : Type()
		object Password    : Type()
		object Integer     : Type()
		object Decimal     : Type()
		object PhoneNumber : Type()
	}

}