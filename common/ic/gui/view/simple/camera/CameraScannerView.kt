package ic.gui.view.simple.camera


import ic.gui.dim.dp.ext.dp
import ic.gui.adapters.ViewAdapters
import ic.gui.view.simple.SimpleView


abstract class CameraScannerView : SimpleView() {


	protected abstract fun onScanned (code: String)

	internal fun callOnScanned (code: String) {
		onScanned(code = code)
	}


	override val intrinsicMinWidth  get() = 0.dp
	override val intrinsicMinHeight get() = 0.dp


	override val ViewAdapters<*>.adapter get() = cameraScannerViewAdapter


}