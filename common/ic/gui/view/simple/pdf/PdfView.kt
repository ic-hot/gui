package ic.gui.view.simple.pdf


import ic.stream.sequence.ByteSequence

import ic.gui.adapters.ViewAdapters
import ic.gui.dim.dp.ByContainer
import ic.gui.view.simple.SimpleView


abstract class PdfView : SimpleView() {


	override val layoutWidth  get() = ByContainer
	override val layoutHeight get() = ByContainer


	abstract val pdf : ByteSequence?


	override val ViewAdapters<*>.adapter get() = pdfViewAdapter


}