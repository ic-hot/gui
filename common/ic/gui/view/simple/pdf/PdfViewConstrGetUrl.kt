package ic.gui.view.simple.pdf


import ic.design.task.Task
import ic.design.task.scope.ext.doInUiThread
import ic.network.http.sendHttpRequest
import ic.parallel.funs.doInBackgroundAsTask
import ic.stream.sequence.ByteSequence
import ic.util.log.LogLevel
import ic.util.log.logW


inline fun PdfView (

	crossinline getPdfUrl : () -> String?

) : PdfView {

	return object : PdfView() {

		override var pdf : ByteSequence? = null

		var oldPdfUrl : String? = null

		var loadTask : Task? = null

		override fun onUpdate() {
			val pdfUrl = getPdfUrl()
			if (pdfUrl != oldPdfUrl) {
				oldPdfUrl = pdfUrl
				loadTask?.cancel()
				loadTask = null
				pdf = null
				if (pdfUrl != null) {
					loadTask = doInBackgroundAsTask {
						try {
							val loaded = sendHttpRequest(
								urlString = pdfUrl,
								successLogLevel = LogLevel.None
							).body
							doInUiThread {
								loadTask = null
								pdf = loaded
								updateAllViews()
							}
						} catch (e: Exception) {
							logW("Uncaught") { e }
							doInUiThread {
								loadTask = null
							}
						}
					}
				}
			}
		}

		override fun onClose() {
			loadTask?.cancel()
			loadTask = null
		}

	}

}