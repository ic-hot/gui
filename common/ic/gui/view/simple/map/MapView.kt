package ic.gui.view.simple.map


import ic.base.primitives.float32.Float32
import ic.util.geo.Location

import ic.gui.view.simple.SimpleView


abstract class MapView : SimpleView() {


	abstract val zoom : Float32

	abstract val position : Location


}