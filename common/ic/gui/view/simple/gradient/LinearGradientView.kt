package ic.gui.view.simple.gradient


import ic.base.primitives.float32.Float32

import ic.graphics.color.Color

import ic.gui.dim.dp.ext.dp
import ic.gui.adapters.ViewAdapters
import ic.gui.view.simple.SimpleView


abstract class LinearGradientView : SimpleView() {


	abstract val startColor : Color

	abstract val endColor : Color


	abstract val directionX : Float32
	abstract val directionY : Float32


	override val intrinsicMinWidth  get() = 0.dp
	override val intrinsicMinHeight get() = 0.dp


	final override val ViewAdapters<*>.adapter get() = linearGradientViewAdapter


}