package ic.gui.view.simple.canvas


import ic.graphics.canvas.Canvas

import ic.gui.dim.dp.ext.dp
import ic.gui.adapters.ViewAdapters
import ic.gui.view.simple.SimpleView


abstract class CanvasView : SimpleView() {


	abstract val toRedrawContinuously : Boolean


	protected abstract fun Canvas.onDraw()

	internal fun callOnDraw (canvas: Canvas) {
		canvas.onDraw()
	}


	override val intrinsicMinWidth  get() = 0.dp
	override val intrinsicMinHeight get() = 0.dp


	final override val ViewAdapters<*>.adapter get() = canvasViewAdapter


}