package ic.gui.view.simple.input


import ic.base.primitives.int32.Int32
import ic.base.strings.ext.isEmpty
import ic.base.strings.ext.startsWith
import ic.struct.list.empty.EmptyList

import ic.graphics.color.Color

import ic.gui.align.Left
import ic.gui.dim.dp.ext.dp
import ic.gui.font.Font
import ic.gui.view.simple.text.OnOkayAction
import ic.gui.view.simple.text.TextInput
import ic.gui.view.simple.text.TextView


abstract class InputView : TextView() {


	final override val opacity get() = 0F

	final override val isSelectable get() = false

	final override val textHorizontalAlign get() = Left

	final override val maxLength get() = Int32.MAX_VALUE

	final override val maxLinesCount get() = Int32.MAX_VALUE

	final override val toEllipsize get() = false

	final override val toUnderscore get() = false

	final override val toStrikeThrough get() = false

	final override val size get() = 0.dp

	final override val font : Font get() = Font.Default

	final override val lineSpacingExtra get() = 0.dp

	final override val color get() = Color.Transparent

	final override val text get() = " "

	final override val spans get() = EmptyList


	protected abstract val inputType : TextInput.Type

	protected abstract val toFocusByDefault : Boolean

	protected abstract val onOkayAction : OnOkayAction

	protected abstract fun onInputEvent (inputEvent: InputEvent)


	override val input = TextInput(
		getType = { inputType },
		toFocusByDefault = { toFocusByDefault },
		getOnOkayAction = { onOkayAction },
		onTextChange = { newValue ->
			when {
				newValue.isEmpty -> {
					onInputEvent(InputEvent.Backspace)
				}
				newValue startsWith " " -> { // TODO
					newValue.forEachIndexed { index, c ->
						if (index > 0) {
							onInputEvent(
								InputEvent.Character(c)
							)
						}
					}
				}
				else -> {
					newValue.forEach { c ->
						onInputEvent(
							InputEvent.Character(c)
						)
					}
				}
			}
		}
	)


}