package ic.gui.view.simple.input


sealed class InputEvent {


	data class Character (

		val character : ic.base.primitives.character.Character

	) : InputEvent()


	object Backspace : InputEvent()


}