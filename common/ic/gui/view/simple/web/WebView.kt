package ic.gui.view.simple.web


import ic.base.escape.skip.Skippable

import ic.gui.adapters.ViewAdapters
import ic.gui.view.simple.SimpleView


abstract class WebView : SimpleView() {


	abstract val innerStyleCss : String

	abstract val pageHtml : String?

	abstract val pageUrl : String?

	abstract val toEnableJavaScript : Boolean

	abstract val toUseMobileSiteVersion : Boolean

	abstract val javascriptToRun : String?

	abstract val toOpenLinksInBrowser : Boolean


	@Skippable
	protected abstract fun onRedirectOrSkip (url: String)

	@Skippable
	internal fun callOnRedirectOrSkip (url: String) {
		onRedirectOrSkip(url = url)
	}


	override val ViewAdapters<*>.adapter get() = webViewAdapter


}