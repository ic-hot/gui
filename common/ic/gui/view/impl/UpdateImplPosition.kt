package ic.gui.view.impl


import ic.util.log.logD

import ic.gui.view.View


internal fun View.updateImplPosition() {

	val localX = this.localX
	val localY = this.localY

	val isNeeded = (
		if (implLocalX == localX && implLocalY == localY) {
			false
		} else {
			true
		}
	)

	if (isNeeded) {

		if (debugName != null) {
			logD("gui") { "$debugName updateImplPosition localX: $localX, localY: $localY" }
		}

		implLocalX = localX
		implLocalY = localY

		thisAdapter.run { updateImplLocalPosition(impl, localX, localY) }

	}

}