package ic.gui.view.impl


import ic.util.log.logD

import ic.gui.view.View


internal fun View.updateSelfOnly() {

	if (debugName != null) {
		logD("gui") {
			"$debugName updateSelfOnly " +
			"updatedVersion: $updatedVersion, " +
			"updateVersion: $updateVersion"
		}
	}

	val isNeeded = (
		if (updatedVersion == 0L) {
			true
		} else {
			if (updatedVersion == updateVersion) {
				false
			} else {
				true
			}
		}
	)

	if (isNeeded) {

		updatedVersion = updateVersion

		callOnUpdate()

	}

}