package ic.gui.view.impl


import ic.base.assert.assert
import ic.util.log.logD

import ic.gui.view.View


internal fun View.updateImplSelf() {

	assert { updatedVersion > 0 }

	if (debugName != null) {
		logD("gui") {
			"$debugName updateImplSelf " +
			"updatedVersion: $updatedVersion, " +
			"implUpdatedVersion: $implUpdatedVersion"
		}
	}

	if (implUpdatedVersion != updatedVersion) {

		implUpdatedVersion = updatedVersion

		thisAdapter.run { updateImplSelf(impl) }

	}

}