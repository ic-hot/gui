package ic.gui.view.impl


import ic.gui.adapters.ViewAdapter
import ic.gui.view.View


@Suppress("UNCHECKED_CAST")
internal inline val View.thisAdapter get() = adapter as ViewAdapter<View, Any>