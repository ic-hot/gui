package ic.gui.view.impl


import ic.util.log.logD

import ic.gui.view.View
import ic.gui.view.ext.height
import ic.gui.view.ext.width


internal fun View.updateImplSize() {

	val width  = this.width
	val height = this.height

	val isNeeded = (
		if (implWidth == width && implHeight == height) {
			false
		} else {
			true
		}
	)

	if (isNeeded) {

		implWidth  = width
		implHeight = height

		if (debugName != null) {
			logD("gui") { "$debugName updateImplSize width: $width, height: $height" }
		}

		thisAdapter.updateImplSize(this, impl, width, height)

	}

}