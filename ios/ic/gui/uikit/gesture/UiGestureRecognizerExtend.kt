package ic.gui.uikit.gesture


internal val UiGestureRecognizer.extend get() = extendUiGestureRecognizer(this)