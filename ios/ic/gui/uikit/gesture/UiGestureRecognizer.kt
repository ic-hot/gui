package ic.gui.uikit.gesture


import platform.UIKit.UIEvent


interface UiGestureRecognizer {


	fun touchesBegan (touches: Set<*>, withEvent: UIEvent?)

	fun touchesMoved (touches: Set<*>, withEvent: UIEvent?)

	fun touchesEnded (touches: Set<*>, withEvent: UIEvent?)

	fun touchesCancelled (touches: Set<*>, withEvent: UIEvent?)

	fun reset() {}


}