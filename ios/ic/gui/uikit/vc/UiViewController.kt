package ic.gui.uikit.vc


import platform.Foundation.NSNotification
import platform.UIKit.UIInterfaceOrientationMask
import platform.UIKit.UIViewController


interface UiViewController {

	fun loadView (uiViewController: UIViewController)

	fun keyboardWillShow (notification: NSNotification)
	fun keyboardWillHide (notification: NSNotification)

	val supportedInterfaceOrientations : UIInterfaceOrientationMask

	fun orientationDidChange (notification: NSNotification)

}