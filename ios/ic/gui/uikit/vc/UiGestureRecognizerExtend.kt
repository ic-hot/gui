package ic.gui.uikit.vc


internal val UiViewController.extend get() = extendUiViewController(this)