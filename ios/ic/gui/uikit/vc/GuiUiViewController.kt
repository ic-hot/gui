package ic.gui.uikit.vc


import ic.base.primitives.float64.Float64
import ic.gui.view.View
import ic.gui.dim.dp.ext.dp
import ic.gui.window.Window
import ic.gui.window.context.UiWindowContext
import ic.gui.window.orient.ScreenOrientationConfig

import kotlinx.cinterop.useContents
import kotlinx.cinterop.ExperimentalForeignApi

import platform.Foundation.NSNotification
import platform.Foundation.NSValue
import platform.UIKit.CGRectValue
import platform.UIKit.UIInterfaceOrientationMaskAll
import platform.UIKit.UIInterfaceOrientationMaskLandscape
import platform.UIKit.UIInterfaceOrientationMaskPortrait
import platform.UIKit.UIKeyboardFrameEndUserInfoKey
import platform.UIKit.UIView
import platform.UIKit.UIViewController
import platform.UIKit.UIViewControllerTransitionCoordinatorProtocol


@OptIn(ExperimentalForeignApi::class)
internal class GuiUiViewController (

	private val getIcView : () -> View,

	private val getIcWindow : () -> Window,

	private val getWindowContext : () -> UiWindowContext

) : UiViewController {


	override fun loadView (uiViewController: UIViewController) {
		uiViewController.view = getIcView().impl as UIView
	}


	override val supportedInterfaceOrientations get() = when (getIcWindow().screenOrientationConfig) {
		ScreenOrientationConfig.All       -> UIInterfaceOrientationMaskAll
		ScreenOrientationConfig.Landscape -> UIInterfaceOrientationMaskLandscape
		ScreenOrientationConfig.Portrait  -> UIInterfaceOrientationMaskPortrait
	}


	override fun keyboardWillShow (notification: NSNotification) {
		val userInfo = notification.userInfo ?: return
		val keyboardHeightNsValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue ?: return
		val keyboardHeight = keyboardHeightNsValue.CGRectValue.useContents { size.height }
		getWindowContext().notifyKeyboardShown(keyboardHeight = keyboardHeight.dp)
	}

	override fun keyboardWillHide (notification: NSNotification) {
		getWindowContext().notifyKeyboardHidden()
	}

	override fun orientationDidChange (notification: NSNotification) {
		getWindowContext().notifyOrientationChanged()
	}


}