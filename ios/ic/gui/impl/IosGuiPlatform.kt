package ic.gui.impl


import ic.base.primitives.float32.Float32
import ic.stream.output.ByteOutput

import ic.gui.app.context.IosGuiAppContext
import ic.storage.res.impl.IosGuiResources


internal object IosGuiPlatform : GuiPlatform {

	override val resources get() = IosGuiResources

	override val guiAppContext get() = IosGuiAppContext

	override fun getScreenDensityFactor() : Float32 {
		return 1F
	}

	override fun openBroadcastOutput(): ByteOutput {
		TODO("Not yet implemented")
	}

}