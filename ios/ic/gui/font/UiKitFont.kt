package ic.gui.font


import platform.UIKit.UIFont

import ic.gui.dim.dp.Dp


interface UiKitFont : Font {

	fun asUiFont (size: Dp) : UIFont

}