package ic.gui.font


import platform.UIKit.UIFont

import ic.base.primitives.float32.ext.asFloat64
import ic.util.log.logE

import ic.gui.dim.dp.Dp


data class UiKitFontFromResource (

	val fontName : String

) : UiKitFont {

	override fun asUiFont (size: Dp) : UIFont {
		UIFont.familyNames.forEach { familyName ->
			familyName as String
			UIFont.fontNamesForFamilyName(familyName).forEach { iosFontName ->
				iosFontName as String
				if (iosFontName == fontName) {
					return UIFont.fontWithName(
						fontName = iosFontName,
						size = size.asFloat64
					)!!
				}
			}
		}
		UIFont.familyNames.forEach { familyName ->
			familyName as String
			UIFont.fontNamesForFamilyName(familyName).forEach { iosFontName ->
				iosFontName as String
				logE("IosFont") { "familyName: $familyName, fontName: $iosFontName" }
			}
		}
		throw RuntimeException("fontName: $fontName")
	}

}