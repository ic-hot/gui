package ic.gui.font


import platform.UIKit.UIFont

import ic.base.primitives.float32.ext.asFloat64

import ic.gui.dim.dp.Dp


object DefaultUiKitFont : UiKitFont {

	override fun asUiFont (size: Dp) : UIFont {
		return UIFont.systemFontOfSize(
			fontSize = size.asFloat64
		)
	}

}