package ic.gui.window.context


import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.useContents

import platform.UIKit.UIImage
import platform.UIKit.UIImagePickerControllerDelegateProtocol
import platform.UIKit.UINavigationControllerDelegateProtocol
import platform.darwin.NSObject
import platform.UIKit.UIImagePickerController
import platform.UIKit.UIWindow
import platform.UIKit.UIViewController
import platform.UserNotifications.UNAuthorizationOptionAlert
import platform.UserNotifications.UNAuthorizationOptionBadge
import platform.UserNotifications.UNAuthorizationOptionSound
import platform.UserNotifications.UNUserNotificationCenter

import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.stream.buffer.ByteBuffer
import ic.struct.collection.ext.count.count
import ic.struct.collection.ext.reduce.any
import ic.struct.collection.Collection
import ic.stream.output.ByteOutput

import ic.graphics.image.ImageFromUiImage

import ic.gui.app.context.impl.globalWindowSerializer

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.permissions.Permission
import ic.gui.root.context.UiGuiRootContext
import ic.gui.scope.ViewScope.IfAlreadyUpdating.*
import ic.gui.uikit.app.foregroundIcWindow
import ic.gui.uikit.app.isInForeground
import ic.gui.view.context.ViewContext
import ic.gui.view.context.UiViewContext
import ic.gui.window.Window
import ic.gui.window.context.impl.createUiWindowExternalInterface


@OptIn(ExperimentalForeignApi::class)
internal class UiWindowContext (

	internal val uiWindow : UIWindow,

	override val icWindow : Window,

	internal val parentIcWindow : Window?,

	internal val uiViewController : UIViewController

) : WindowContext, UiGuiRootContext {

	private inline val thisUiWindowContext get() = this

	override val viewContext : ViewContext = UiViewContext(
		getUiViewController = { uiViewController },
		getRootMaxWidth  = { windowWidth  },
		getRootMaxHeight = { windowHeight }
	)

	override val topDecorHeight get() = uiWindow.safeAreaInsets.useContents { top }.dp

	override val bottomDecorHeight get() = (
		if (isKeyboardShown) {
			0.dp
		} else {
			uiWindow.safeAreaInsets.useContents { bottom }.dp
		}
	)

	private var keyboardHeight : Dp = 0.dp

	override val windowWidth get() = uiWindow.frame.useContents { size.width }.dp
	override val windowHeight get() = uiWindow.frame.useContents { size.height }.dp - keyboardHeight

	override val isKeyboardShown get() = keyboardHeight > 0

	fun notifyKeyboardShown (keyboardHeight: Dp) {
		this.keyboardHeight = keyboardHeight
		viewContext.updateAllViews(ifAlreadyUpdating = UpdateAgain)
	}

	fun notifyKeyboardHidden() {
		this.keyboardHeight = 0.dp
		viewContext.updateAllViews(ifAlreadyUpdating = UpdateAgain)
	}

	fun notifyOrientationChanged () {
		viewContext.updateAllViews(ifAlreadyUpdating = UpdateAgain)
	}

	override fun initResultOutput() : ByteOutput {
		return ByteBuffer(
			onClose = {
				val result = this
				val output = ByteBuffer()
				output.runAndCloseOrCancel {
					globalWindowSerializer.run {
						writeWindow(window = icWindow)
					}
					write(result)
				}
				thisUiWindowContext.close()
				parentIcWindow!!.notifyWindowResult(output.newIterator())
			}
		)
	}

	override fun close() {
		keyboardHeight = 0.dp
		uiWindow.hidden = true
		foregroundIcWindow = parentIcWindow
		if (isInForeground) {
			icWindow.notifyPause()
			parentIcWindow?.notifyResume()
		}
		icWindow.notifyClose(Window.CloseReason.ByCode)
	}

	override fun openImageSelector (titleText: String) {
		val uiImagePickerController = UIImagePickerController()
		uiImagePickerController.setDelegate(
			object : NSObject(), UINavigationControllerDelegateProtocol, UIImagePickerControllerDelegateProtocol {
				override fun imagePickerController (
					picker : UIImagePickerController,
					didFinishPickingImage : UIImage,
					editingInfo : Map<Any?, *>?
				) {
					icWindow.notifyImageSelected(
						ImageFromUiImage(didFinishPickingImage)
					)
					uiImagePickerController.dismissViewControllerAnimated(true) {}
				}
			}
		)
		uiViewController.presentViewController(uiImagePickerController, animated = true) {}
	}

	override fun openFileSelector() {
		TODO("Not yet implemented")
	}

	override fun requestPermission (permissions: Collection<Permission>) {
		if (permissions.count == 1L && permissions.any == Permission.Notifications) {
			UNUserNotificationCenter
			.currentNotificationCenter()
			.requestAuthorizationWithOptions(
				UNAuthorizationOptionAlert or
				UNAuthorizationOptionSound or
				UNAuthorizationOptionBadge
			) { isSuccess, _ ->
				if (isSuccess) {
					icWindow.notifyPermissionGranted()
				} else {
					icWindow.notifyPermissionDenied()
				}
			}
		} else {
			throw NotImplementedError()
		}
	}

	private val externalInterface = createUiWindowExternalInterface()

	init {
		externalInterface.init(icWindow = icWindow, uiWindow = uiWindow)
	}

	override fun externalCall (message: Any?): Any? {
		return externalInterface.notifyCall(message)
	}

}