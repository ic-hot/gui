package ic.gui.app.external


import ic.app.runtimeApp
import ic.base.annotations.UsedExternally

import ic.gui.app.GuiApp

import platform.Foundation.NSData
import platform.UIKit.UIApplicationLaunchOptionsKey


abstract class UiApplicationExternalInterface {


	@UsedExternally
	open fun onLaunch (options: Map<UIApplicationLaunchOptionsKey, Any>?) {}

	@UsedExternally
	open fun onRegisterForRemoteNotifications (token: NSData) {}


	protected abstract fun onCall (message: Any?) : Any?

	fun notifyCall (message: Any?) : Any? {
		return onCall(message)
	}


	@UsedExternally
	fun call (message: Any?) : Any? {
		val icGuiApp = runtimeApp as GuiApp
		return icGuiApp.notifyExternalCall(message)
	}


}