package ic.gui.app.external


import platform.UIKit.UIView
import platform.UIKit.UIWindow

import ic.base.annotations.UsedExternally

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.Dp
import ic.gui.view.View
import ic.gui.window.Window


abstract class UiWindowExternalInterface {


	private lateinit var icWindow : Window

	protected lateinit var uiWindow : UIWindow

	internal fun init (
		icWindow : Window,
		uiWindow : UIWindow
	) {
		this.icWindow = icWindow
		this.uiWindow = uiWindow
	}


	fun call (message: Any?) : Any? {
		return icWindow.notifyExternalCall(message)
	}


	protected abstract fun onCall (message: Any?) : Any?

	fun notifyCall (message: Any?) : Any? {
		return onCall(message)
	}


	@UsedExternally
	fun IosView (
		uiView : UIView,
		width : Dp, height : Dp
	) : View {
		return ic.gui.view.ios.IosView(
			uiView = uiView,
			width = width, height = height
		)
	}

	@UsedExternally
	fun IosViewByContainer (
		uiView : UIView
	) : View {
		return ic.gui.view.ios.IosView(
			uiView = uiView,
			width = ByContainer, height = ByContainer
		)
	}


}