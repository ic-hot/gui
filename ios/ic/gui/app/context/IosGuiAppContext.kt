package ic.gui.app.context


import ic.gui.root.context.UiGuiRootContext
import ic.gui.window.context.impl.uiApplicationExternalInterface


object IosGuiAppContext : GuiAppContext, UiGuiRootContext {

    override val isOpen get() = true

    override val icWindow get() = null

    override fun externalCall (message: Any?) : Any? {
        return uiApplicationExternalInterface.notifyCall(message)
    }

}