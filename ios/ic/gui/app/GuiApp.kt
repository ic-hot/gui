package ic.gui.app


import platform.Foundation.NSURL
import platform.Foundation.NSUserActivity
import platform.Foundation.NSUserActivityTypeBrowsingWeb
import platform.UIKit.UIApplicationLaunchOptionsKey
import platform.UIKit.UIGestureRecognizer
import platform.UIKit.UIViewController
import platform.UIKit.UIApplicationLaunchOptionsURLKey
import platform.UIKit.UIApplication
import platform.UIKit.UIApplicationOpenURLOptionsKey

import ic.app.impl.tierDefinedExternally
import ic.app.setRuntimeApp
import ic.app.tier.Tier
import ic.app.tier.tierMayChange
import ic.base.annotations.UsedExternally
import ic.util.url.Url

import ic.gui.uikit.gesture.UiGestureRecognizer
import ic.gui.app.external.UiApplicationExternalInterface
import ic.gui.app.external.UiWindowExternalInterface
import ic.gui.uikit.app.foregroundIcWindow
import ic.gui.uikit.app.isInForeground
import ic.gui.uikit.url.urlBeforeWindow
import ic.gui.uikit.vc.UiViewController


actual abstract class GuiApp : AGuiApp() {


	@UsedExternally
	fun launch (
		isDebug : Boolean,
		launchOptions : Map<UIApplicationLaunchOptionsKey, Any>?,
		uiApplicationExternalInterface : UiApplicationExternalInterface,
		createUiWindowExternalInterface : () -> UiWindowExternalInterface,
		extendUiGestureRecognizer : (UiGestureRecognizer) -> UIGestureRecognizer,
		extendUiViewController : (UiViewController) -> UIViewController
	) {

		tierDefinedExternally = if (isDebug) Tier.Debug else Tier.Production
		tierMayChange

		setRuntimeApp(this)

		val nsUrl = launchOptions?.get(UIApplicationLaunchOptionsURLKey) as? NSURL
		val url = nsUrl?.let { Url.parseOrNull(it.absoluteString) }
		url?.let { urlBeforeWindow = it }

		ic.gui.window.context.impl.createUiWindowExternalInterface = createUiWindowExternalInterface
		ic.gui.window.context.impl.uiApplicationExternalInterface = uiApplicationExternalInterface
		ic.gui.uikit.gesture.extendUiGestureRecognizer = extendUiGestureRecognizer
		ic.gui.uikit.vc.extendUiViewController = extendUiViewController

		uiApplicationExternalInterface.onLaunch(launchOptions)

		run()

	}


	@UsedExternally
	fun applicationWillEnterForeground (uiApplication: UIApplication) {
		if (!isInForeground) {
			isInForeground = true
			foregroundIcWindow?.notifyResume()
		}
	}

	@UsedExternally
	fun applicationDidEnterBackground (uiApplication: UIApplication) {
		if (isInForeground) {
			isInForeground = false
			foregroundIcWindow?.notifyPause()
		}
	}

	@UsedExternally
	fun applicationContinueUserActivity (uiApplication: UIApplication, userActivity: NSUserActivity) : Boolean {
		if (userActivity.activityType == NSUserActivityTypeBrowsingWeb) {
			val nsUrl = userActivity.webpageURL ?: return false
			val icUrl = Url.parseOrNull(nsUrl.absoluteString) ?: return false
			foregroundIcWindow?.notifyUrl(icUrl)
			return true
		}
		else return false
	}

	@UsedExternally
	fun applicationOpenUrl (uiApplication: UIApplication, nsUrl: NSURL, options: Map<UIApplicationOpenURLOptionsKey, Any>?) : Boolean {
		val icUrl = Url.parseOrNull(nsUrl.absoluteString) ?: return false
		foregroundIcWindow?.notifyUrl(icUrl)
		return true
	}


}