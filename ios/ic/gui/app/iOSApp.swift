import UIKit

import app

import UIKit.UIGestureRecognizerSubclass

let guiApp = Main()

let mainUiApplicationExternalInterface = MainUiApplicationExternalInterface()

@UIApplicationMain
class MainApplicationDelegate : NSObject, UIApplicationDelegate {

    func application (
        _ application : UIApplication,
        didFinishLaunchingWithOptions launchOptions : [UIApplication.LaunchOptionsKey : Any]? = nil
    ) -> Bool {
        #if DEBUG
            let isDebug = true
        #else
            let isDebug = false
        #endif
        guiApp.launch(
            isDebug : isDebug,
            launchOptions : launchOptions,
            uiApplicationExternalInterface : mainUiApplicationExternalInterface,
            createUiWindowExternalInterface : {
                return MainUiWindowExternalInterface()
            },
            extendUiGestureRecognizer : { iface in
                return CustomUiGestureRecognizer(iface: iface)
            },
            extendUiViewController : { iface in
                return CustomUiViewController(iface: iface)
            }
        )
        return true
    }

    func applicationWillEnterForeground (_ application: UIApplication) {
        /*
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        center.removeAllPendingNotificationRequests()
        */
        guiApp.applicationWillEnterForeground(uiApplication: application)
    }

    func applicationDidEnterBackground (_ application: UIApplication) {
        guiApp.applicationDidEnterBackground(uiApplication: application)
    }

    func application (
        _ application : UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken : Data
    ) {
        mainUiApplicationExternalInterface.onRegisterForRemoteNotifications(token: deviceToken)
    }

    func application (
        _ application: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {
        return guiApp.applicationOpenUrl(uiApplication: application, nsUrl: url, options: options)
    }

    func application (
        _ application: UIApplication,
        continue userActivity: NSUserActivity,
        restorationHandler: @escaping ([any UIUserActivityRestoring]?) -> Void
    ) -> Bool {
        return guiApp.applicationContinueUserActivity(uiApplication: application, userActivity: userActivity)
    }

}


class CustomUiGestureRecognizer : UIGestureRecognizer {

    private let iface : UiGestureRecognizer

    init (iface: UiGestureRecognizer) {
        self.iface = iface
        super.init(target: nil, action: nil)
    }

    override func touchesBegan (_ touches: Set<UITouch>, with event: UIEvent) {
        iface.touchesBegan(touches: touches, withEvent: event)
    }

    override func touchesMoved (_ touches: Set<UITouch>, with event: UIEvent) {
        iface.touchesMoved(touches: touches, withEvent: event)
    }

    override func touchesCancelled (_ touches: Set<UITouch>, with event: UIEvent) {
        iface.touchesCancelled(touches: touches, withEvent: event)
    }

    override func touchesEnded (_ touches: Set<UITouch>, with event: UIEvent) {
        iface.touchesEnded(touches: touches, withEvent: event)
    }

    override func reset() {
        iface.reset()
    }

}


class CustomUiViewController : UIViewController {

    private let iface : UiViewController

    init (iface: UiViewController) {
        self.iface = iface
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

	override func loadView () {
        iface.loadView(uiViewController: self)
    }

    override func viewWillAppear (_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(notification:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(notification:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(orientationDidChange),
            name: UIDevice.orientationDidChangeNotification,
            object: nil
        )
    }

    override func viewDidDisappear (_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        NotificationCenter.default.removeObserver(
            self,
            name: UIDevice.orientationDidChangeNotification,
            object: nil
        )
    }

    @objc func keyboardWillShow (notification: Notification) {
        iface.keyboardWillShow(notification: notification)
    }

    @objc func keyboardWillHide (notification: Notification) {
        iface.keyboardWillHide(notification: notification)
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask(rawValue: UInt(iface.supportedInterfaceOrientations))
    }

    @objc func orientationDidChange (_ notification: Notification) {
        iface.orientationDidChange(notification: notification)
    }

}