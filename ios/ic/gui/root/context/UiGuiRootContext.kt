package ic.gui.root.context


import platform.UIKit.UIWindow

import ic.stream.buffer.ByteBuffer
import ic.stream.output.ByteOutput

import ic.gui.app.context.impl.globalWindowSerializer
import ic.gui.uikit.app.foregroundIcWindow
import ic.gui.uikit.app.isInForeground
import ic.gui.uikit.url.urlBeforeWindow
import ic.gui.uikit.vc.GuiUiViewController
import ic.gui.uikit.vc.extend
import ic.gui.view.View
import ic.gui.window.Window
import ic.gui.window.context.UiWindowContext
import kotlinx.cinterop.ExperimentalForeignApi
import platform.UIKit.UIScreen


@OptIn(ExperimentalForeignApi::class)
interface UiGuiRootContext : GuiRootContext {


	val icWindow : Window?


	override fun initOpenWindowOutput() : ByteOutput {
		return ByteBuffer(
			onClose = {
				val thisIcWindow = icWindow
				val windowTypeAndStateInput = newIterator()
				val newIcWindow = globalWindowSerializer.run {
					windowTypeAndStateInput.readWindow()
				}
				val uiWindow = UIWindow(
					frame = UIScreen.mainScreen.bounds
				)
				lateinit var windowContext : UiWindowContext
				lateinit var icView : View
				val uiViewController = GuiUiViewController(
					getWindowContext = { windowContext },
					getIcView = { icView },
					getIcWindow = { newIcWindow }
				).extend
				windowContext = UiWindowContext(
					uiWindow = uiWindow,
					icWindow = newIcWindow,
					parentIcWindow = thisIcWindow,
					uiViewController = uiViewController
				)
				icView = newIcWindow.notifyOpen(
					context = windowContext,
					stateInput = windowTypeAndStateInput
				)
				uiWindow.setRootViewController(uiViewController)
				if (urlBeforeWindow != null) {
					newIcWindow.notifyUrl(urlBeforeWindow!!)
					urlBeforeWindow = null
				}
				uiWindow.makeKeyAndVisible()
				windowContext.viewContext.updateAllViews()
				foregroundIcWindow = newIcWindow
				if (isInForeground) {
					thisIcWindow?.notifyPause()
					newIcWindow.notifyResume()
				}
			}
		)
	}


}
