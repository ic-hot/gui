package ic.gui.view.ios


import platform.UIKit.UIView

import ic.base.primitives.float32.Float32
import ic.gui.align.Center

import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp


internal inline fun IosView (

	uiView : UIView,

	width : Dp, height : Dp,
	weight : Float32 = 1F,
	horizontalAlign : HorizontalAlign = Center,
	verticalAlign   : VerticalAlign   = Center,

) : IosView {

	return object : IosView(uiView = uiView) {

		override val layoutWidth  get() = width
		override val layoutHeight get() = height

		override val layoutWeight get() = weight

		override val layoutHorizontalAlign get() = horizontalAlign
		override val layoutVerticalAlign   get() = verticalAlign

	}

}