package ic.gui.view.ios


import platform.UIKit.UIView

import ic.gui.adapters.IosViewAdapters
import ic.gui.adapters.ViewAdapter
import ic.gui.adapters.ViewAdapters
import ic.gui.view.simple.SimpleView


internal abstract class IosView (

	val uiView : UIView

) : SimpleView() {

	override val ViewAdapters<*>.adapter : ViewAdapter<*, *> get() {
		this as IosViewAdapters
		return iosViewAdapter
	}

}