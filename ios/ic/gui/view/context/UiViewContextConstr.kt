package ic.gui.view.context


import kotlinx.cinterop.ExperimentalForeignApi

import platform.UIKit.UIViewController

import ic.gui.dim.dp.Dp


@ExperimentalForeignApi
internal inline fun UiViewContext (

    crossinline getUiViewController : () -> UIViewController?,

    crossinline getRootMaxWidth  : () -> Dp,
    crossinline getRootMaxHeight : () -> Dp,

    crossinline afterUpdate : () -> Unit = {}

) : UiViewContext {

    return object : UiViewContext() {

        override val uiViewController get() = getUiViewController()

        override val rootMaxWidth  get() = getRootMaxWidth()
        override val rootMaxHeight get() = getRootMaxHeight()

        override fun afterUpdate() {
            afterUpdate()
        }

    }

}