package ic.gui.view.context


import kotlinx.cinterop.ExperimentalForeignApi

import platform.Foundation.NSURL
import platform.QuickLook.QLPreviewController
import platform.UIKit.UIApplication
import platform.UIKit.UIViewController
import platform.UIKit.UIView
import platform.UIKit.endEditing
import platform.QuickLook.QLPreviewControllerDataSourceProtocol
import platform.QuickLook.QLPreviewItemProtocol
import platform.darwin.NSInteger
import platform.darwin.NSObject

import ic.base.primitives.float32.Float32
import ic.ifaces.cancelable.Cancelable
import ic.design.task.scope.ext.doInUiThread
import ic.parallel.funs.doInBackgroundAsTask
import ic.util.geo.Location
import ic.util.log.logW

import ic.graphics.image.Image
import ic.graphics.image.ext.asUiImage
import ic.graphics.image.fromurl.getImageFromUrlCacheFirst
import ic.graphics.uikit.image.ext.asNsUrl

import ic.gui.adapters.IosViewAdapters
import ic.util.log.logD


@ExperimentalForeignApi
internal abstract class UiViewContext : ViewContext() {

    protected abstract val uiViewController : UIViewController?

    override val adapters get() = IosViewAdapters

    override fun showKeyboard() {}

    override fun hideKeyboard() {
        val rootUiView = uiViewController?.view ?: return
        UIView.setAnimationsEnabled(true)
        rootUiView.endEditing(true)
        UIView.setAnimationsEnabled(false)
    }

    override fun copyToClipboard(text: String) {
        TODO("Not yet implemented")
    }

    override fun getStringFromClipboard(): String? {
        TODO("Not yet implemented")
    }

    override fun goToMapsNavigator(destination: Location) {
        TODO("Not yet implemented")
    }

    override fun openUrl (url: String) {
        logD("UiViewContext") { "openUrl $url" }
        val nsUrl = NSURL(string = url)
        UIApplication.sharedApplication.openURL(nsUrl, mapOf<Any?, Any?>()) { isSuccess ->
            logD("UiViewContext") { "openUrl $url isSuccess: $isSuccess" }
        }
    }

    override fun openImageViewer (image: Image) {
        val imageUrl = image.asUiImage.asNsUrl
        val qlPreviewController = QLPreviewController()
        qlPreviewController.dataSource = object : NSObject(), QLPreviewControllerDataSourceProtocol {
            override fun numberOfPreviewItemsInPreviewController (controller: QLPreviewController) : NSInteger {
                return 1
            }
            override fun previewController (controller: QLPreviewController, previewItemAtIndex: NSInteger) : QLPreviewItemProtocol {
                return object : NSObject(), QLPreviewItemProtocol {
                    override fun previewItemURL() : NSURL {
                        return imageUrl
                    }
                }
            }
        }
        uiViewController?.presentViewController(qlPreviewController, animated = true) {}
    }

    private var loadImageTask : Cancelable? = null

    override fun openImageViewer (imageUrl: String) {
        if (loadImageTask != null) return
        loadImageTask = doInBackgroundAsTask {
            try {
            	val image = getImageFromUrlCacheFirst(imageUrl)
                doInUiThread {
                    loadImageTask = null
                    openImageViewer(image)
                }
            } catch (e: Exception) {
                logW("Uncaught") { e }
                doInUiThread {
                    loadImageTask = null
                }
            }
        }
    }

    override fun setAppLanguage(languageCode: String?) {
        TODO("Not yet implemented")
    }

    override fun setScreenBrightness(brightness: Float32) {
        TODO("Not yet implemented")
    }

}