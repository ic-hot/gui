package ic.gui.adapters


import kotlinx.cinterop.ExperimentalForeignApi

import platform.UIKit.UIView

import ic.gui.view.simple.camera.CameraScannerView
import ic.gui.view.simple.canvas.CanvasView
import ic.gui.view.simple.datepicker.DatePickerView


@OptIn(ExperimentalForeignApi::class)
internal object IosViewAdapters : ViewAdapters<UIView> {

    override val cameraScannerViewAdapter: ViewAdapter<CameraScannerView, out UIView>
        get() = TODO("Not yet implemented")
    override val canvasViewAdapter: ViewAdapter<CanvasView, out UIView>
        get() = TODO("Not yet implemented")

    override val cardViewAdapter      get() = IosCardViewAdapter
    override val containerViewAdapter get() = IosContainerViewAdapter

    override val datePickerViewAdapter: ViewAdapter<DatePickerView, out UIView>
        get() = TODO("Not yet implemented")

    override val imageViewAdapter get() = IosImageViewAdapter

    override val pdfViewAdapter get() = IosPdfViewAdapter

    override val linearGradientViewAdapter get() = IosLinearGradientViewAdapter

    override val textViewAdapter get() = IosTextViewAdapter

    override val webViewAdapter get() = IosWebViewAdapter

    val iosViewAdapter get() = IosIosViewAdapter

}