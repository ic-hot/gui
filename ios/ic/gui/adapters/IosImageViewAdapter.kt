package ic.gui.adapters


import platform.UIKit.UIImage
import platform.UIKit.UIImageRenderingMode.UIImageRenderingModeAlwaysTemplate
import platform.UIKit.UIImageView
import platform.UIKit.UIViewContentMode

import ic.struct.map.editable.EditableMap

import ic.graphics.color.ext.asUiColor
import ic.graphics.image.ext.asUiImage

import ic.gui.view.simple.image.ImageView
import ic.gui.view.simple.image.ScaleMode


internal object IosImageViewAdapter : IosViewAdapter<ImageView, UIImageView>() {

    override fun initUiView (icView: ImageView) : UIImageView {
        return UIImageView()
    }

    private val uiImagesWithAlwaysTemplate = EditableMap<UIImage, UIImage>()

    override fun ImageView.updateUiViewSelf (uiView: UIImageView) {
        uiView.contentMode = when (scaleMode) {
            ScaleMode.Crop    -> UIViewContentMode.UIViewContentModeScaleAspectFill
            ScaleMode.Fit     -> UIViewContentMode.UIViewContentModeScaleAspectFit
            ScaleMode.Stretch -> UIViewContentMode.UIViewContentModeScaleToFill
        }
        val tint = this.tint
        if (tint == null) {
            uiView.image = image?.asUiImage
        } else {
            uiView.image = image?.asUiImage?.let { uiImage ->
                uiImagesWithAlwaysTemplate[uiImage]
                ?: uiImage.imageWithRenderingMode(UIImageRenderingModeAlwaysTemplate)
                .also { uiImagesWithAlwaysTemplate[uiImage] = it }
            }
            uiView.tintColor = tint.asUiColor
        }
    }

}