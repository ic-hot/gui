package ic.gui.adapters


import platform.UIKit.UIView

import ic.gui.view.ios.IosView


internal object IosIosViewAdapter : IosViewAdapter<IosView, UIView>() {

	override fun initUiView (icView: IosView): UIView {
		return icView.uiView
	}

	override fun IosView.updateUiViewSelf (uiView: UIView) {}

}