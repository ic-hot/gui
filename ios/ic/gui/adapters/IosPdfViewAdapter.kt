package ic.gui.adapters


import kotlinx.cinterop.BetaInteropApi
import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.memScoped

import platform.CoreGraphics.CGRectMake
import platform.PDFKit.PDFDocument

import ic.base.arrays.ext.byteArrayToNsData
import ic.stream.sequence.ByteSequence
import ic.stream.sequence.ext.toByteArray

import ic.gui.view.simple.pdf.PdfView


@BetaInteropApi
@ExperimentalForeignApi
internal object IosPdfViewAdapter : IosViewAdapter<PdfView, IosPdfViewAdapter.Impl>() {

	class Impl : platform.PDFKit.PDFView(
		frame = CGRectMake(0.0, 0.0, 0.0, 0.0)
	) {
		var pdf : ByteSequence? = null
		init {
			autoScales = true
		}
	}

	override fun initUiView (icView: PdfView) : Impl {
		return Impl()
	}

	override fun PdfView.updateUiViewSelf (uiView: Impl) {
		val pdf = this.pdf
		if (pdf != uiView.pdf) {
			uiView.pdf = pdf
			if (pdf == null) {
				uiView.document = null
			} else {
				val byteArray = pdf.toByteArray()
				val document = memScoped {
					PDFDocument(
						data = byteArrayToNsData(byteArray)
					)
				}
				uiView.document = document
			}
		}
	}

}