package ic.gui.adapters


import kotlinx.cinterop.copy
import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.useContents

import platform.UIKit.UIView
import platform.CoreGraphics.CGSizeMake

import ic.base.primitives.float32.ext.asFloat64
import ic.base.primitives.float64.Float64
import ic.base.primitives.int32.ext.asFloat64

import ic.graphics.color.ext.asUiColor

import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.View
import ic.gui.view.ext.width
import platform.CoreGraphics.CGAffineTransformMakeScale


@OptIn(ExperimentalForeignApi::class)
abstract class IosViewAdapter
    <V: View, UiView: UIView>
    : ViewAdapter<V, UiView>
{

    protected abstract fun initUiView (icView: V) : UiView

    final override fun V.initImpl() : UiView {
        return initUiView(this)
    }

    override fun V.updateImplLocalPosition (impl: UiView, localX: Dp, localY: Dp) {
        impl.setFrame(
            impl.frame.copy {
                origin.x = localX.asFloat64
                origin.y = localY.asFloat64
            }
        )
    }

    override fun updateImplSize (icView: V, impl: UiView, width: Dp, height: Dp) {
        impl.setFrame(
            impl.frame.copy {
                size.width  = width.asFloat64
                size.height = height.asFloat64
            }
        )
    }

    protected abstract fun V.updateUiViewSelf (uiView: UiView)

    private val defaultScale = CGAffineTransformMakeScale(sx = 1.0, sy = 1.0)

    final override fun V.updateImplSelf (impl: UiView) {
        updateUiViewSelf(impl)
        impl.alpha = opacity.asFloat64
        impl.transform = (
            if (scaleX == 1F && scaleY == 1F) {
                defaultScale
            } else {
                CGAffineTransformMakeScale(sx = scaleX.asFloat64, sy = scaleY.asFloat64)
            }
        )
        impl.layer.zPosition = this.z.asFloat64
        val debugColor = this.debugColor
        if (debugColor != null) {
            impl.backgroundColor = debugColor.asUiColor
        }
    }

    override fun getIntrinsicMinWidth (view: V, impl: UiView) : Dp {
        val measuredWidth = impl.sizeThatFits(
            CGSizeMake(Float64.MAX_VALUE, Float64.MAX_VALUE)
        ).useContents { width }
        if (measuredWidth == Float64.POSITIVE_INFINITY) {
            return 0.dp
        } else {
            return measuredWidth.dp
        }
    }

    override fun getIntrinsicMinHeight (view: V, impl: UiView) : Dp {
        val measuredHeight = impl.sizeThatFits(
            CGSizeMake(view.width.asFloat64, Float64.MAX_VALUE)
        ).useContents { height }
        if (measuredHeight == Float64.POSITIVE_INFINITY) {
            return 0.dp
        } else {
            return measuredHeight.dp
        }
    }

}