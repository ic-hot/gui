package ic.gui.adapters


import kotlinx.cinterop.ExperimentalForeignApi

import platform.CoreFoundation.CFRetain
import platform.CoreGraphics.CGPointMake
import platform.CoreGraphics.CGRectMake
import platform.Foundation.CFBridgingRelease
import platform.UIKit.UIView
import platform.QuartzCore.CAGradientLayer

import ic.base.primitives.float32.ext.asFloat64
import ic.graphics.color.ext.alpha

import ic.graphics.color.ext.asUiColor
import ic.graphics.color.ext.copy

import ic.gui.dim.dp.Dp
import ic.gui.view.simple.gradient.LinearGradientView


@OptIn(ExperimentalForeignApi::class)
internal object IosLinearGradientViewAdapter
    : IosViewAdapter<LinearGradientView, IosLinearGradientViewAdapter.Impl>()
{

    class Impl : UIView(
        frame = CGRectMake(.0, .0, .0, .0)
    ) {
        val gradientLayer = CAGradientLayer()
        init {
            layer.addSublayer(gradientLayer)
        }
    }

    override fun initUiView (icView: LinearGradientView) : Impl {
        return Impl()
    }

    override fun updateImplSize (icView: LinearGradientView, impl: Impl, width: Dp, height: Dp) {
        super.updateImplSize(icView, impl, width, height)
        impl.gradientLayer.setFrame(
            CGRectMake(0.0, 0.0, width.asFloat64, height.asFloat64)
        )
    }

    override fun LinearGradientView.updateUiViewSelf (uiView: Impl) {
        var startColor = this.startColor
        var endColor = this.endColor
        when {
            endColor.alpha == 0F -> {
                endColor = startColor.copy(alpha = 0F)
            }
            startColor.alpha == 0F -> {
                startColor = endColor.copy(alpha = 0F)
            }
        }
        uiView.gradientLayer.colors = listOf(
            CFBridgingRelease(CFRetain(startColor.asUiColor.CGColor)),
            CFBridgingRelease(CFRetain(endColor.asUiColor.CGColor))
        )
        var directionX = this.directionX
        var directionY = this.directionY
        when {
            directionX < 0 -> {
                uiView.gradientLayer.startPoint = CGPointMake(1.0, 0.0)
                uiView.gradientLayer.endPoint = CGPointMake(0.0, 0.0)
            }
            directionY < 0 -> {
                uiView.gradientLayer.startPoint = CGPointMake(0.0, 1.0)
                uiView.gradientLayer.endPoint = CGPointMake(0.0, 0.0)
            }
            else -> {
                uiView.gradientLayer.startPoint = CGPointMake(0.0, 0.0)
                uiView.gradientLayer.endPoint = CGPointMake(directionX.asFloat64, directionY.asFloat64)
            }
        }
    }

}