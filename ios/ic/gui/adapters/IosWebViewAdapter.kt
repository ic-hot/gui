package ic.gui.adapters


import kotlinx.cinterop.ExperimentalForeignApi

import platform.CoreGraphics.CGRectMake
import platform.WebKit.WKContentMode
import platform.WebKit.WKNavigation
import platform.WebKit.WKNavigationDelegateProtocol
import platform.WebKit.WKWebView
import platform.WebKit.WKWebViewConfiguration
import platform.darwin.NSObject

import ic.base.primitives.float64.Float64
import ic.base.primitives.float64.ext.asFloat32

import ic.graphics.color.Color
import ic.graphics.color.ext.asUiColor

import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.view.simple.web.WebView


@ExperimentalForeignApi
internal object IosWebViewAdapter : IosViewAdapter<WebView, IosWebViewAdapter.Impl>() {

    class Impl : WKWebView(
        frame = CGRectMake(0.0, 0.0, 0.0, 0.0),
        configuration = WKWebViewConfiguration()
    ) {
        var pageHtml : String? = null
        var measuredHeight : Dp = 0.dp
        init {
        	hidden = true
            backgroundColor = Color.White.asUiColor
        }
    }

    override fun initUiView (icView: WebView) : Impl {
        val uiView = Impl()
        uiView.scrollView.bounces = false
        uiView.configuration.defaultWebpagePreferences.preferredContentMode = WKContentMode.WKContentModeMobile
        uiView.navigationDelegate = object : NSObject(), WKNavigationDelegateProtocol {
            @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
            override fun webView (webView: WKWebView, didFinishNavigation: WKNavigation?) {
                uiView.evaluateJavaScript("document.body.scrollHeight") { measuredHeight, _ ->
                    if (measuredHeight is Float64) {
                        uiView.measuredHeight = measuredHeight.asFloat32
                        icView.updateAllViews()
                        uiView.hidden = false
                    }
                }
            }
        }
        return uiView
    }

    override fun WebView.updateUiViewSelf (uiView: Impl) {
        val pageHtml = this.pageHtml
        if (this.layoutHeight == ByContent) {
            uiView.userInteractionEnabled = false
            uiView.scrollView.scrollEnabled = false
        } else {
            uiView.userInteractionEnabled = true
            uiView.scrollView.scrollEnabled = true
        }
        if (uiView.pageHtml != pageHtml) {
            uiView.hidden = true
            uiView.pageHtml = pageHtml
            if (pageHtml != null) {
                uiView.loadHTMLString(
                    string = (
                        "<head><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></head>" +
                        pageHtml
                    ),
                    baseURL = null
                )
            }
        }
    }

    override fun getIntrinsicMinWidth (view: WebView, impl: Impl): Dp {
        return 0.dp
    }

    override fun getIntrinsicMinHeight (view: WebView, impl: Impl) : Dp {
        return impl.measuredHeight
    }

}
