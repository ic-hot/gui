package ic.gui.adapters


import kotlinx.cinterop.copy
import kotlinx.cinterop.CValue
import kotlinx.cinterop.ExperimentalForeignApi

import platform.CoreGraphics.CGRectMake
import platform.CoreGraphics.CGSizeMake
import platform.CoreGraphics.CGPoint
import platform.UIKit.UIEvent
import platform.UIKit.UIView
import platform.UIKit.UIColor
import platform.QuartzCore.kCALayerMaxXMaxYCorner
import platform.QuartzCore.kCALayerMaxXMinYCorner
import platform.QuartzCore.kCALayerMinXMaxYCorner
import platform.QuartzCore.kCALayerMinXMinYCorner
import platform.UIKit.UIStackView
import platform.UIKit.UIBlurEffect.Companion.effectWithStyle
import platform.UIKit.UIBlurEffectStyle
import platform.UIKit.UIVisualEffectView

import ic.base.primitives.float32.ext.asFloat64
import ic.math.funs.max

import ic.graphics.color.ext.asUiColor

import ic.gui.dim.dp.Dp
import ic.gui.view.card.CardView


@OptIn(ExperimentalForeignApi::class)
internal object IosCardViewAdapter
    : IosAContainerViewAdapter<CardView, UIView>()
{

    class OuterUiView (
        private val icView: CardView
    ) : UIStackView(frame = CGRectMake(.0, .0, .0, .0)) {
        override fun hitTest (point: CValue<CGPoint>, withEvent: UIEvent?) : UIView? {
            return implHitTest(
                icView = icView, uiView = this,
                superHitTest = { super.hitTest(point, withEvent) }
            )
        }
        init {
            this.clipsToBounds = false
            this.layer.masksToBounds = false
            this.layer.shadowColor = UIColor.blackColor.CGColor
        }
        val innerUiView = UIStackView(frame = CGRectMake(.0, .0, .0, .0))
        init {
            innerUiView.clipsToBounds = true
        	addSubview(innerUiView)
        }
    }

    override fun initOuterUiView (icView: CardView) : UIView {
        if (icView.subview == null) {
            if (icView.blurRadius > 0) {
                return UIVisualEffectView(
                    effect = effectWithStyle(
                        style = UIBlurEffectStyle.UIBlurEffectStyleRegular
                    )
                )
            } else {
                return UIView()
            }
        } else {
            return OuterUiView(icView)
        }
    }

    override fun getInnerUiView (outerUiView: UIView) : UIStackView? {
        return (outerUiView as? OuterUiView)?.innerUiView
    }

    override fun updateImplSize (icView: CardView, impl: UIView, width: Dp, height: Dp) {
        super.updateImplSize(icView, impl, width, height)
        if (impl !is OuterUiView) return
        val innerUiView = impl.innerUiView
        val inset = icView.inset
        val innerViewWidth  = width  - inset * 2
        val innerViewHeight = height - inset * 2
        innerUiView.setFrame(
            innerUiView.frame.copy {
                origin.x = inset.asFloat64
                origin.y = inset.asFloat64
                size.width  = innerViewWidth.asFloat64
                size.height = innerViewHeight.asFloat64
            }
        )
    }

    override fun CardView.updateOuterUiView (outerUiView: UIView) {

        if (outerUiView is OuterUiView) {

            outerUiView.layer.cornerRadius = max(
                topLeftCornerRadius, topRightCornerRadius,
                bottomLeftCornerRadius, bottomRightCornerRadius
            ).asFloat64

            outerUiView.layer.borderWidth = outlineThickness.asFloat64
            outerUiView.layer.borderColor = outlineColor.asUiColor.CGColor

            if (elevation > 0) {
                outerUiView.layer.shadowOpacity = this.shadowOpacity * 0.375F
            } else {
                outerUiView.layer.shadowOpacity = 0F
            }

            outerUiView.layer.shadowOffset = CGSizeMake(0.0, 0.0)

            outerUiView.layer.shadowRadius = elevation.asFloat64

            val innerUiView = outerUiView.innerUiView

            innerUiView.backgroundColor = this.backgroundColor.asUiColor

            innerUiView.layer.cornerRadius = max(
                max(
                    topLeftCornerRadius, topRightCornerRadius,
                    bottomLeftCornerRadius, bottomRightCornerRadius
                ) - inset,
                0F
            ).asFloat64

            innerUiView.layer.maskedCorners = (
                (if (topLeftCornerRadius > 0) kCALayerMinXMinYCorner else 0.toULong()) +
                (if (topRightCornerRadius > 0) kCALayerMaxXMinYCorner else 0.toULong()) +
                (if (bottomLeftCornerRadius > 0) kCALayerMinXMaxYCorner else 0.toULong()) +
                (if (bottomRightCornerRadius > 0) kCALayerMaxXMaxYCorner else 0.toULong())
            )

        } else {

            outerUiView.backgroundColor = backgroundColor.asUiColor

        }

    }

}