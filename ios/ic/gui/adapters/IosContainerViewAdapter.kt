package ic.gui.adapters


import kotlinx.cinterop.CValue
import kotlinx.cinterop.ExperimentalForeignApi

import platform.CoreGraphics.CGPoint
import platform.CoreGraphics.CGRectMake
import platform.UIKit.UIEvent
import platform.UIKit.UIStackView
import platform.UIKit.UIView

import ic.gui.view.container.ContainerView


@ExperimentalForeignApi
internal object IosContainerViewAdapter
    : IosAContainerViewAdapter<ContainerView, IosContainerViewAdapter.OuterUiView>()
{

    class OuterUiView (
        val icView : ContainerView
    ) : UIStackView(frame = CGRectMake(.0, .0, .0, .0)) {
        init {
            clipsToBounds = false
            layer.masksToBounds = false
        }
        override fun hitTest (point: CValue<CGPoint>, withEvent: UIEvent?) : UIView? {
            return implHitTest(
                icView = icView, uiView = this,
                superHitTest = { super.hitTest(point, withEvent) }
            )
        }
    }

    override fun initOuterUiView (icView: ContainerView) : OuterUiView {
        return OuterUiView(icView)
    }

    override fun getInnerUiView (outerUiView: OuterUiView) : UIStackView {
        return outerUiView
    }

    override fun ContainerView.updateOuterUiView (outerUiView: OuterUiView) {}

}