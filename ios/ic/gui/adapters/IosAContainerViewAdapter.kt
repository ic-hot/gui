package ic.gui.adapters


import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.useContents

import platform.UIKit.UIEvent
import platform.UIKit.UIStackView
import platform.UIKit.UITouch
import platform.UIKit.UIView

import ic.base.kcollections.getAny
import ic.base.primitives.float64.Float64
import ic.struct.list.ext.reduce.find.none
import ic.struct.list.ext.foreach.forEach

import ic.gui.uikit.gesture.UiGestureRecognizer
import ic.gui.uikit.gesture.extend
import ic.gui.dim.dp.ext.dp
import ic.gui.touch.TouchEvent
import ic.gui.view.container.ContainerView
import ic.gui.view.container.empty.EmptyView
import ic.gui.view.context.ViewContext
import ic.gui.view.ext.updateImpl


@OptIn(ExperimentalForeignApi::class)
internal abstract class IosAContainerViewAdapter<V: ContainerView, OuterUiView: UIView>
    : IosViewAdapter<V, OuterUiView>()
{

	companion object {

		protected inline fun implHitTest (
			icView : ContainerView,
			uiView : UIView,
			superHitTest : () -> UIView?
		) : UIView? {
			val responder = superHitTest()
			if (icView.container is ViewContext) {
				return responder
			} else {
				if (responder == uiView) {
					return null
				} else {
					return responder
				}
			}
		}

	}


	protected abstract fun initOuterUiView (icView: V) : OuterUiView

	final override fun initUiView (icView: V) : OuterUiView {
		val uiView = initOuterUiView(icView)
		if (icView.container is ViewContext) {
			val gestureRecognizer = object : UiGestureRecognizer {
				override fun touchesBegan (touches: Set<*>, withEvent: UIEvent?) {
					if (touches.size == 1) {
						val touch = touches.getAny() as UITouch
						var x : Float64 = Float64.NaN
						var y : Float64 = Float64.NaN
						touch.locationInView(uiView).useContents { x = this.x; y = this.y }
						icView.touchHandler.handle(
							TouchEvent.Down(
								touchId = 1,
								globalX = x.dp, globalY = y.dp
							)
						)
					}
				}
				override fun touchesMoved (touches: Set<*>, withEvent: UIEvent?) {
					if (touches.size == 1) {
						val touch = touches.getAny() as UITouch
						var x : Float64 = Float64.NaN
						var y : Float64 = Float64.NaN
						touch.locationInView(uiView).useContents { x = this.x; y = this.y }
						icView.touchHandler.handle(
							TouchEvent.Move(
								touchId = 1,
								globalX = x.dp, globalY = y.dp
							)
						)
					}
				}
				override fun touchesEnded (touches: Set<*>, withEvent: UIEvent?) {
					if (touches.size == 1) {
						icView.touchHandler.handle(
							TouchEvent.Up(touchId = 1)
						)
					}
				}
				override fun touchesCancelled (touches: Set<*>, withEvent: UIEvent?) {
					if (touches.size == 1) {
						icView.touchHandler.handle(
							TouchEvent.Cancel(touchId = 1)
						)
					}
				}
			}.extend
			gestureRecognizer.cancelsTouchesInView = false
			gestureRecognizer.delaysTouchesBegan = false
			gestureRecognizer.delaysTouchesEnded = false
			uiView.addGestureRecognizer(gestureRecognizer)
		}
		return uiView
	}

	protected abstract fun getInnerUiView (outerUiView: OuterUiView) : UIStackView?

	protected abstract fun V.updateOuterUiView (outerUiView: OuterUiView)

	final override fun V.updateUiViewSelf (uiView: OuterUiView) {
		updateOuterUiView(uiView)
		val innerUiView = getInnerUiView(uiView) ?: return
		val implSubviews = innerUiView.subviews
		implSubviews.forEach { implSubview ->
			implSubview as UIView
			if (preparedSubviews.none { it.impl == implSubview }) {
				implSubview.removeFromSuperview()
			}
		}
		preparedSubviews.forEach { child ->
			if (child !is EmptyView || child.debugColor != null) {
				val childImpl = child.impl as UIView
				if (implSubviews.none { it == childImpl }) {
					innerUiView.addSubview(childImpl)
				}
				child.updateImpl()
			}
		}
	}

}