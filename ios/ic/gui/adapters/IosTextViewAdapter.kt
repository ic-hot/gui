package ic.gui.adapters


import kotlinx.cinterop.CValue
import kotlinx.cinterop.ExperimentalForeignApi

import platform.CoreGraphics.CGRectMake
import platform.Foundation.NSMutableAttributedString
import platform.Foundation.create
import platform.UIKit.NSTextAlignmentCenter
import platform.UIKit.NSTextAlignmentLeft
import platform.UIKit.NSTextAlignmentRight
import platform.UIKit.UIColor
import platform.UIKit.UIEdgeInsetsMake
import platform.UIKit.UITextView
import platform.UIKit.UITextViewDelegateProtocol
import platform.darwin.NSObject
import platform.Foundation.NSMakeRange
import platform.Foundation.NSRange
import platform.Foundation.NSURL
import platform.Foundation.addAttribute
import platform.UIKit.NSForegroundColorAttributeName
import platform.UIKit.NSLineBreakByTruncatingTail
import platform.UIKit.NSLinkAttributeName
import platform.UIKit.UIKeyboardTypeDecimalPad
import platform.UIKit.UIKeyboardTypeDefault
import platform.UIKit.UIKeyboardTypeNumberPad
import platform.UIKit.UIKeyboardTypePhonePad
import platform.UIKit.UIReturnKeyType
import platform.UIKit.UITextItemInteraction
import platform.UIKit.UIView

import ic.base.assert.assert
import ic.base.primitives.int32.ext.asUInt64
import ic.base.strings.ext.getSubstring
import ic.base.strings.ext.parse.parseInt64
import ic.base.strings.ext.startsWith
import ic.design.task.scope.ext.doInUiThread
import ic.struct.list.List
import ic.struct.list.ext.foreach.forEachIndexed
import ic.struct.list.ext.length.isEmpty
import ic.struct.list.ext.reduce.find.atLeastOne

import ic.graphics.color.Color
import ic.graphics.color.ext.asUiColor

import ic.gui.dim.dp.Dp
import ic.gui.font.Font
import ic.gui.font.UiKitFont
import ic.gui.view.simple.text.OnOkayAction
import ic.gui.view.simple.text.TextInput
import ic.gui.view.simple.text.TextView
import ic.gui.view.simple.text.impl.adapter.TextViewAdapter
import ic.gui.view.simple.text.span.Link
import ic.gui.view.simple.text.span.Span


@OptIn(ExperimentalForeignApi::class)
internal object IosTextViewAdapter
    : IosViewAdapter<TextView, IosTextViewAdapter.Impl>(), TextViewAdapter<IosTextViewAdapter.Impl>
{

    class Impl (
        val icView : TextView
    ) : UITextView(
        frame = CGRectMake(0.0, 0.0, 0.0, 0.0),
        textContainer = null
    ) {
        var appliedText : String? = null
        var appliedSpans : List<Span>? = null
        var appliedFont : Font? = null
        var appliedSize : Dp = Dp.NaN
        var appliedColor : Color? = null
        var isFocusTaskLaunched : Boolean = false
        var delegateIfInput : UITextViewDelegateProtocol? = null
        var delegateIfNonInput : UITextViewDelegateProtocol? = null
    }

    override fun initUiView (icView: TextView) : Impl {
        val uiView = Impl(icView = icView)
        uiView.scrollEnabled = false
        uiView.backgroundColor = UIColor.clearColor
        return uiView
    }

    override fun TextView.updateUiViewSelf (uiView: Impl) {
        val icView = this
        val text = this.text
        val spans = this.spans
        if (text != uiView.appliedText || spans != uiView.appliedSpans) {
            uiView.appliedText = text
            uiView.appliedSpans = spans
            if (spans.isEmpty) {
                uiView.text = text
            } else {
                val attributedText = NSMutableAttributedString.create(text)
                spans.forEachIndexed { index, span ->
                    val nsRange = NSMakeRange(
                        loc = span.startCharIndex.asUInt64,
                        len = (span.endCharIndex - span.startCharIndex).asUInt64
                    )
                    val spanColor = span.color
                    if (spanColor != null) {
                        attributedText.addAttribute(
                            name = NSForegroundColorAttributeName,
                            value = spanColor.asUiColor,
                            range = nsRange
                        )
                    }
                    when (span) {
                        is Link -> {
                            attributedText.addAttribute(
                                name = NSLinkAttributeName,
                                value = "https://link.to/$index",
                                range = nsRange
                            )
                        }
                        else -> {}
                    }
                }
                uiView.attributedText = attributedText
            }
        }
        val font = this.font
        val size = this.size
        if (font != uiView.appliedFont || size != uiView.appliedSize) {
            uiView.appliedFont = font
            uiView.appliedSize = size
            uiView.font = (font as UiKitFont).asUiFont(size = size)
            val lineHorizontalPadding = uiView.textContainer.lineFragmentPadding
            uiView.textContainerInset = UIEdgeInsetsMake(0.0, -lineHorizontalPadding, 0.0, -lineHorizontalPadding)
        }
        val color = this.color
        if (color != uiView.appliedColor) {
            uiView.appliedColor = color
            uiView.textColor = color.asUiColor
        }
        val textHorizontalAlign = this.textHorizontalAlign
        uiView.textAlignment = when {
            textHorizontalAlign < .25F -> NSTextAlignmentLeft
            textHorizontalAlign > .75F -> NSTextAlignmentRight
            else                       -> NSTextAlignmentCenter
        }
        uiView.textContainer.maximumNumberOfLines = icView.maxLinesCount.asUInt64
        if (this.toEllipsize) {
            uiView.textContainer.lineBreakMode = NSLineBreakByTruncatingTail
        }
        val input = this.input
        if (input == null) {
            uiView.editable = false
            if (spans.atLeastOne { it is Link }) {
                uiView.userInteractionEnabled = true
            } else {
                uiView.userInteractionEnabled = false
            }
            if (uiView.delegateIfNonInput == null) {
                uiView.delegateIfNonInput = object : NSObject(), UITextViewDelegateProtocol {
                    override fun textView(
                        textView : UITextView, shouldInteractWithURL : NSURL,
                        inRange : CValue<NSRange>, interaction : UITextItemInteraction
                    ) : Boolean {
                        val url = shouldInteractWithURL.toString()
                        assert { url startsWith "https://link.to/" }
                        val spanIndex = (
                            url.getSubstring("https://link.to/".length, url.length).parseInt64()
                        )
                        val link = icView.spans[spanIndex] as Link
                        link.onClick()
                        return false
                    }
                }
            }
            uiView.setDelegate(uiView.delegateIfNonInput)
        } else {
            uiView.editable = true
            uiView.userInteractionEnabled = true
            if (input.toFocusByDefault) {
                if (!uiView.isFocusTaskLaunched) {
                    uiView.isFocusTaskLaunched = true
                    doInUiThread {
                        UIView.setAnimationsEnabled(true)
                        uiView.becomeFirstResponder()
                        UIView.setAnimationsEnabled(false)
                    }
                }
            }
            uiView.keyboardType = when (input.type) {
                TextInput.Type.Integer     -> UIKeyboardTypeNumberPad
                TextInput.Type.Decimal     -> UIKeyboardTypeDecimalPad
                TextInput.Type.PhoneNumber -> UIKeyboardTypePhonePad
                else                       -> UIKeyboardTypeDefault
            }
            uiView.returnKeyType = when (input.onOkayAction) {
                OnOkayAction.HideKeyboard -> UIReturnKeyType.UIReturnKeyDefault
                is OnOkayAction.Done -> UIReturnKeyType.UIReturnKeyDone
                is OnOkayAction.Send -> UIReturnKeyType.UIReturnKeySend
            }
            if (uiView.delegateIfInput == null) {
                uiView.delegateIfInput = object : NSObject(), UITextViewDelegateProtocol {
                    override fun textViewDidBeginEditing (textView: UITextView) {
                        icView.notifyFocus()
                    }
                    override fun textView(
                        textView : UITextView,
                        shouldChangeTextInRange : CValue<NSRange>,
                        replacementText : String
                    ) : Boolean {
                        if (replacementText == "\n") {
                            if (input.onOkayAction is OnOkayAction.HideKeyboard) {
                                return true
                            } else {
                                input.onOkayAction.onOkay()
                                return false
                            }
                        } else {
                            return true
                        }
                    }
                    override fun textViewDidChange (textView: UITextView) {
                        val newText = textView.text
                        uiView.appliedText = newText
                        icView.input?.onTextChange(newText)
                    }
                    override fun textViewDidEndEditing (textView: UITextView) {
                        icView.notifyUnfocus()
                    }
                }
            }
            uiView.setDelegate(uiView.delegateIfInput)
        }
    }

    override fun focus (impl: Impl) {
        impl.becomeFirstResponder()
    }

    override fun unfocus (impl: Impl) {

    }

}
