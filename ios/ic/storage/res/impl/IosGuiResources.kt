package ic.storage.res.impl


import ic.gui.font.DefaultBoldUiKitFont
import ic.gui.font.DefaultUiKitFont
import ic.gui.font.UiKitFontFromResource


object IosGuiResources : GuiResources {

	override fun getDefaultFont() = DefaultUiKitFont

	override fun getDefaultBoldFont() = DefaultBoldUiKitFont

	override fun getDefaultMonospaceFont() = throw NotImplementedError()

	override fun getFontOrNull (path: String, fontName: String) = UiKitFontFromResource(fontName = fontName)

}